local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load(--This config assumes the passenger train is currently on the westbound platform at my base
	"#railwayDispatch.lua",
	{
		modemSide = "back",
		railwayServerComputerID = 6,
		routesFromHere = {
			["bc"] = {
				["path"] = {
					"main_signal_+1_W_station",
					"main_signal_-1_W",
					"main_signal_-2_W",
					"bbb_signal_-1_N",
					"bbb_signal_-2_N",
					"bbb_signal_-3_N",
					"bbb_signal_-4_N",
					"bbb_signal_-3_S",
					"bbb_signal_-2_S",
					"bbb_signal_-1_S",
					"main_signal_-2_E",
					"main_signal_-1_E",
					"main_signal_+1_E",
					"main_signal_+2_E",
					"main_signal_+2.5_E",
					"main_signal_+3_E",
					"im_signal_+1_S",
					"im_signal_+2_S",
					"im_signal_+3_S",
					"im_signal_+4_S",
					"im_signal_+3_N",
					"im_signal_+2_N",
					"im_signal_+1_N",
					"main_signal_+3_W",
					"main_signal_+2.5_W",
					"main_signal_+2_W_station",
				},
				["looping"] = true
			},
			["b"] = {
				["path"] = {
					"main_signal_+1_W_station",
					"main_signal_-1_W",
					"main_signal_-2_W",
					"bbb_signal_-1_N",
					"bbb_signal_-2_N",
					"bbb_signal_-3_N",
					"bbb_signal_-4_N",
					"bbb_signal_-3_S",
					"bbb_signal_-2_S",
					"bbb_signal_-1_S",
					"main_signal_-2_E",
					"main_signal_-1_E",
					"main_signal_+1_E",
					"main_signal_+2_E",
					"main_signal_+2.5_E",
					"main_signal_+3_E",
					"main_signal_+3_W",
					"main_signal_+2.5_W",
					"main_signal_+2_W_station"
				},
				["looping"] = true
			},
			["c"] = {
				["path"] = {
					"main_signal_+1_W_station",
					"main_signal_+1_E",
					"main_signal_+2_E",
					"main_signal_+2.5_E",
					"main_signal_+3_E",
					"im_signal_+1_S",
					"im_signal_+2_S",
					"im_signal_+3_S",
					"im_signal_+4_S",
					"im_signal_+3_N",
					"im_signal_+2_N",
					"im_signal_+1_N",
					"main_signal_+3_W",
					"main_signal_+2.5_W",
					"main_signal_+2_W_station"
				},
				["looping"] = true
			}
		}
	}
)

rednet.open(config.modemSide)

local function userChoice()
	print("--- Select one of the following ---")
	for routeName, route in pairs(config.routesFromHere) do
		print("> " .. routeName)
	end
	
	local selected = read()
	
	if config.routesFromHere[selected] == nil then
		print("Unknown route name")
		return
	end
	
	rednet.send(
		config.railwayServerComputerID,
		{
			["type"] = "railwayDispatchSetTrainRoute",
			["trainID"] = "passenger1",
			["path"] = config.routesFromHere[selected].path,
			["looping"] = config.routesFromHere[selected].looping
		},
		"railwayDispatch"
	)
	
	print("Route set")
end

while true do
	userChoice()
	sleep(0.5)
end