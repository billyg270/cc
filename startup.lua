local args = {...}

local wRequire = require("wRequire")

local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load("#startup.lua", nil)

if config == nil then
	config = {}

    if args[1] == nil or args[1] == "" then
        print("Command to run:")
        config.command = read()
    else
        print("Command to run: " .. args[1])
        config.command = args[1]
    end
	
	print("Loop (y/n):")
	config.loop = string.lower(read())

    print("Sleep before command (int):")
	config.sleep = tonumber(read())

    print("Modem Side:")
	config.modemSide = read()

    if args[1] == nil or args[1] == "" then
        print("Install " .. config.command .. " (y/n):")
        if string.lower(read()) == "y" then
            print("[startup] Running: installer " .. config.command)
            shell.run("installer " .. config.command .. " dr")
        end
    end
end

tableFile.save(config, "#startup.lua")

rednet.open(config.modemSide)
rednet.host("bb-startup", "bb-startup-" .. os.getComputerID())

function runCommand()
    print("[startup] Sleeping for: " .. tostring(config.sleep))
    sleep(config.sleep)
    print("[startup] Running: " .. config.command)
    shell.run(config.command)
end

function runCommandLoop()
    runCommand()

    if config.loop == "y" then
        while true do
            runCommand()
        end
    end
end

--[[
    network message format:
    {
        ["type"] = "command" or "file"
        ["command"] = "command as string or nil if message is not of type command"
        ["fileName"] = "file name as string or nil if message is not of type file"
        ["fileContent"] = "file content as string or nil if message is not of type file"
    }
]]

function network()
    while true do
        local id, message = rednet.receive("bb-startup")

        if message.type == "command" then
            print("[startup] Received command: " .. message.command)
            shell.run(message.command)
        elseif message.type == "file" then
            print("[startup] Received file: " .. message.fileName)
            local file = fs.open(message.fileName, "w")
            file.write(message.fileContent)
            file.close()
        elseif message.type == "getFile" then
            print("[startup] Requested file: " .. message.fileName)
            local file = fs.open(message.fileName, "r")
            local fileContent = file.readAll()
            rednet.send(id, fileContent)
            file.close()
        elseif message.type == "ping" then
            print("[startup] Requested ping")
            rednet.send(id, "pong")
        else
            print("[startup] Unknown message type: " .. message.type)
        end 
    end
end

parallel.waitForAny(runCommand, network)