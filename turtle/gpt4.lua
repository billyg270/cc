local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local tableFile = wRequire("tableFile")

local config = tableFile.load("#gpt4.lua", nil)

if config == nil then
    config = {}

    print("Open AI API Key:")
    config.apiKey = read()

    config.url = "https://api.openai.com/v1/chat/completions"

    tableFile.save(config, "#gpt4.lua")
end

function doGpt(inputs)
    local headers = {
        ["Content-Type"] = "application/json",
        ["Authorization"] = "Bearer " .. config.apiKey
    }

    local body = {
        ["model"] = "gpt-4",
        ["messages"] = inputs,
        ["temperature"] = 1,
        ["max_tokens"] = 2000,
        ["top_p"] = 1,
        ["frequency_penalty"] = 0,
        ["presence_penalty"] = 0
    }

    local gpt4requestBody = fs.open("gpt4reqBody.json", "w")
    gpt4requestBody.write(textutils.serialiseJSON(body))
    gpt4requestBody.close()

    local respRaw, respErrorMsg, respRaw2 = http.post(config.url, textutils.serialiseJSON(body), headers)
    local resp = "N/A"
    if respRaw then
        local respStr = respRaw.readAll()
        --print(respStr)
        resp = textutils.unserializeJSON(respStr)
        respRaw.close()
    else
        if respErrorMsg then
            print("Request error: " .. respErrorMsg)
        else
            print("Request error: Unknown")
        end
    end

    tableFile.save(resp, "#gpt4resp.lua")
    --print(resp.choices[1].message.content)

    local gpt4codeFile = fs.open("gpt4code.lua", "w")
    gpt4codeFile.write("local scriptPrints = \"\"\nfunction print2(msg)\nprint(msg)\nscriptPrints = scriptPrints .. tostring(msg)\nend\n\nfunction gptCode()\n" .. resp.choices[1].message.content:gsub("print%(", "print2(") .. "\nend\nfunction gptErrorHandler(err)\nprint(\"Code Execution Error: \" .. err)\nend\nxpcall(gptCode, gptErrorHandler)\nlocal scriptPrintsFile = fs.open(\"gpt4codePrints.txt\", \"w\")\nscriptPrintsFile.write(scriptPrints)\nscriptPrintsFile.close()")
    gpt4codeFile.close()

    shell.run("gpt4code.lua")

    local scriptPrintsFile = fs.open("gpt4codePrints.txt", "r")
    local scriptPrints = scriptPrintsFile.readAll()
    scriptPrintsFile.close()

    return resp.choices[1].message.content, scriptPrints
end

local conversation = {
    {
        ["role"] = "system",
        ["content"] = "You are a CC:Tweaked turtle in Minecraft.\n\nYou assist the player.\n\nOutputs should be Lua code only. To tell the user something use a print statement.\n\nWhen you preform an action you should print the output of the action for future reference.\n\nShould only print strings or numbers, don't print tables."
    },
    {
        ["role"] = "user",
        ["content"] = "User: Who are you?"
    },
    {
        ["role"] = "assistant",
        ["content"] = "print(\"I am \" .. tostring(os.computerLabel()) .. \" #\" .. tostring(os.computerID()))"
    },
    {
        ["role"] = "user",
        ["content"] = "Lua output: I am " .. tostring(os.computerLabel()) .. " #" .. tostring(os.computerID())
    }
}

local autoUserInput = nil
while true do
    if autoUserInput == nil then
        print("User:")
        table.insert(
            conversation,
            {
                ["role"] = "user",
                ["content"] = "User: " .. read()
            }
        )
    else
        table.insert(
            conversation,
            {
                ["role"] = "user",
                ["content"] = "User: " .. autoUserInput
            }
        )
    end

    print("Turtle:")
    local outputLua, outputTxt = doGpt(conversation)

    table.insert(
        conversation,
        {
            ["role"] = "assistant",
            ["content"] = outputLua
        }
    )
    table.insert(
        conversation,
        {
            ["role"] = "user",
            ["content"] = "Lua output: " .. textutils.urlEncode(outputTxt)
        }
    )

    if string.find(outputTxt, "Code Execution Error:") then
        autoUserInput = "Fix error and try again"
    else
        autoUserInput = nil
    end
end