local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local bbTurtle = wRequire("bbTurtle")
local invClient = wRequire("invClient")
local DiscordHook = wRequire("DiscordHook")

local hookSuccess, hook = DiscordHook.createWebhook("https://discord.com/api/webhooks/889783581839339541/mtTZmSBCoa-KGi2DN5S5_tQMPJhpPjOjDB3dy8EtY-Dxu2CK9pTeBwMy7IkKMGyVpkti")
if not hookSuccess then
	print("Webhook connection failed! Reason: " .. hook)
    hook = nil
end

local function webhook(msg)
    print(msg)
    if hook ~= nil then
        hook.send(msg, "MWBS #" .. tostring(os.getComputerID()))
    end
end

local config = tableFile.load("#mineWithBlockScanner.lua", nil)

if config == nil then
	config = {}
	
	config.slots = {}
	config.items = {}
	
	print("Pick slot (13-16):")
	config.slots.pick = tonumber(read())
	print("Pick name (d):")
	config.items.pick = read()
	if config.items.pick == "d" then
		config.items.pick = "minecraft:diamond_pickaxe"
	end
	
	print("Modem slot (13-16):")
	config.slots.modem = tonumber(read())
	print("Modem name (e12/e16):")
	config.items.modem = read()
	if config.items.modem == "e12" then
		config.items.modem = "computercraft:advanced_modem"
	end
	if config.items.modem == "e16" then
		config.items.modem = "computercraft:wireless_modem_advanced"
	end
	
	print("Block scanner slot (13-16):")
	config.slots.blockScanner = tonumber(read())
	print("Block scanner name (b = plethora / g = geo):")
	config.items.blockScanner = read()
	if config.items.blockScanner == "b" then
		config.items.blockScanner = "plethora:module"
	end
	if config.items.blockScanner == "g" then
		config.items.blockScanner = "advancedperipherals:geo_scanner"
	end
	
	print("Ender chest slot (13-16):")
	config.slots.chest = tonumber(read())
	
	--print("Slot to trigger empty when filled:")
	--print("eg: 6")
	config.emptyOnSlot = 6--tonumber(read())
	config.emptyToSlot = 12--tonumber(read())
	
	print("Controller ID:")
	config.controllerID = tonumber(read())
	
	print("Ender chest on home network name:")
	config.chestOnHomeNetworkName = read()--'minecraft:ender chest_2'
	
	print("Fuel name (c12/cc12/c13/cc13):")
	config.fuelName = read()
	if config.fuelName == "c12" or config.fuelName == "c13" then
		config.fuelName = "minecraft:coal.0."
	end
	if config.fuelName == "cc12" then
		config.fuelName = "minecraft:coal.1."
	end
	if config.fuelName == "cc13" then
		config.fuelName = "minecraft:charcoal.0."
	end
	
	print("Storage server computer ID:")
	config.storageComputerID = tonumber(read())
    
    --print("Min scan y (inclusive):")
	--config.minScanY = tonumber(read())
    
    --print("Max scan y (inclusive):")
	--config.maxScanY = tonumber(read())
end

invClient.storageComputerID = config.storageComputerID

tableFile.save(config, "#mineWithBlockScanner.lua")

local minFuel = turtle.getFuelLimit() * 0.25
local maxFuel = turtle.getFuelLimit() * 0.75

local distanceWeight = 1
local blockScoreWeight = 1

local maxY = 125
local minY = 5

local blockScores = {}

local validFuels = {
    ["minecraft:coal_block"] = true,
    ["minecraft:coal"] = true,
    ["minecraft:charcoal"] = true,
    ["minecraft:blaze_rod"] = true
}

local blocksToDump = {}

sleep(1)

function Hold(s)
    if turtle.getItemCount(s) == 1 then
        turtle.select(s)
        turtle.equipLeft()
        local i = turtle.getItemDetail()
        if i ~= nil then
            if i.name == config.items.pick then
                turtle.transferTo(config.slots.pick)
            elseif i.name == config.items.blockScanner then
                turtle.transferTo(config.slots.blockScanner)
            elseif i.name == config.items.modem then
                turtle.transferTo(config.slots.modem)
            else
                turtle.transferTo(1)
            end
        end
        turtle.select(1)
    end
end

function Locate()--will assume is holding modem
    local x, y, z = gps.locate()
    local c = 0
    while x == nil do
        c = c + 1
        print("waiting for GPS " .. tostring(c))
        sleep(3)
        x, y, z = gps.locate()
    end
    
    return x, y, z
end

function Network()
    Hold(config.slots.modem)
    local x, y, z = Locate()
    rednet.open("left")
    
    function SendUpdate()
        rednet.send(config.controllerID, {
            ["timestamp"] = os.time(),
			---@diagnostic disable-next-line: undefined-field
            ["id"] = os.getComputerID(),
            ["x"] = x,
            ["y"] = y,
            ["z"] = z,
            ["fuel"] = turtle.getFuelLevel()
        })
    end
    
    SendUpdate()
    
    local id, message = nil, nil
    while true do
        print("Waiting for message from controller")
        id, message = rednet.receive(nil, 10)
        if id ~= config.controllerID then--if nothing send another update
            SendUpdate()
        else
            break
        end
        if message ~= nil and type(message) == "table" then
            --could be a broadcast for new controller
            if message["controllerID"] ~= nil and message["newControllerPassword"] == "dododo" then
                config.controllerID = message["controllerID"]
            end
        end
    end
    
    print("Message from controller received")
    print(textutils.serialise(message))
	
	if message["goto"] ~= nil then
		print("Recived command to goto: ")
		print(textutils.serialise(message["goto"]))
		Hold(config.slots.pick)
		GotoRel(
			message["goto"].x - x,
			0,
			message["goto"].z - z
		)
		GotoRel(
			0,
			message["goto"].y - y,
			0
		)
		error("Reached goto destination")
	end
    
    if message["distanceWeight"] ~= nil then
        distanceWeight = message["distanceWeight"]
    end
    
    if message["blockScoreWeight"] ~= nil then
        blockScoreWeight = message["blockScoreWeight"]
    end
    
    if message["controllerID"] ~= nil then
        config.controllerID = message["controllerID"]
    end
    
    if message["blockScores"] ~= nil then
        blockScores = message["blockScores"]
    end
    
    if message["maxY"] ~= nil then
        maxY = message["maxY"]
    end
    
    if message["minY"] ~= nil then
        minY = message["minY"]
    end
	
	if message["blocksToDump"] ~= nil then
		blocksToDump = message["blocksToDump"]
	end
    
    rednet.close("left")
    
    Hold(config.slots.pick)
end

function Scan(rad)
    Hold(config.slots.blockScanner)
    local geo = peripheral.wrap("left")
    local s, r
    while s == nil do
        s, r = geo.scan(rad)
        if r ~= nil then
            print(r)
        end
        sleep(2)
    end
    --print(textutils.serialize(s))
    return s
end

function ScoreScan(s, d)
	Hold(config.slots.modem)
    local x, y, z = Locate()
	
    local hsi, hsv, hsx, hsy, hsz, blockName
    for k, v in pairs(s) do
        if blockScores[v.name] ~= nil and (y + v.y) >= minY and (y + v.y) <= maxY then
            --local score = (distanceWeight / (math.abs(v.x) + math.abs(v.y) + math.abs(v.z))) * (blockScores[v.name] * blockScoreWeight)
            local score = (distanceWeight * (math.abs(v.x) + math.abs(v.y) + math.abs(v.z))) + (blockScores[v.name] * blockScoreWeight)
            
            if v.x == 0 and v.z == 0 then
                score = score * 0.7
            end
            
            --prefer routes with no turning
            if d == "north" and v.x == 0 and v.z <= 0 then
                score = score * 0.6
            end
            
            if d == "south" and v.x == 0 and v.z >= 0 then
                score = score * 0.6
            end
            
            if d == "east" and v.x >= 0 and v.z == 0 then
                score = score * 0.6
            end
            
            if d == "west" and v.x <= 0 and v.z == 0 then
                score = score * 0.6
            end
            
            if hsi == nil or score > hsv then
                hsi = k
                hsv = score
                hsx = v.x
                hsy = v.y
                hsz = v.z
                blockName = v.name
            end
        end
    end
    
    return hsi, hsv, hsx, hsy, hsz, blockName
end

local dirs = {
    [1] = "north",
    [2] = "east",
    [3] = "south",
    [4] = "west"
}

function GetIndex(v, t)
    for tk, tv in pairs(t) do
        if tv == v then
            return tk
        end
    end
end

function Left(d)
    turtle.turnLeft()
    
    local cd = GetIndex(d, dirs)
    
    local nd = cd - 1
    if nd == 0 then
        nd = 4
    end
    
    print("Facing " .. dirs[nd])
    return dirs[nd]
end

function Right(d)
    turtle.turnRight()
    
    local cd = GetIndex(d, dirs)
    
    local nd = cd + 1
    if nd == 5 then
        nd = 1
    end
    
    print("Facing " .. dirs[nd])
    return dirs[nd]
end

function Face(d, nd)
    local cd = GetIndex(d, dirs)
    local td = GetIndex(nd, dirs)
    
    if cd == td then
        print("Already facing " .. nd)
        return nd
    end
    
    if cd == 1 and td == 4 then
        turtle.turnLeft()
        print("Facing " .. nd)
        return nd
    end
    
    if cd == 4 and td == 1 then
        turtle.turnRight()
        print("Facing " .. nd)
        return nd
    end
    
    if cd < td then
        for i = cd, td - 1 do
            turtle.turnRight()
        end
    end
    
    if cd > td then
        for i = cd, td + 1, -1 do
            turtle.turnLeft()
        end
    end
    
    print("Facing " .. nd)
    return nd
end

function Inv()
    Hold(config.slots.pick)
    bbTurtle.rDig("u")
    turtle.select(config.slots.chest)
    while turtle.placeUp() == false do
        bbTurtle.rDig("u")
        turtle.attackUp()
    end
    
    sleep(0.5)
	
    local c = peripheral.wrap("top")
	
	Hold(config.slots.modem)
	rednet.open("left")
    
    while turtle.getItemCount(config.emptyOnSlot) > 0 or turtle.getItemCount(1) > 0 do
        for i = 1, config.emptyToSlot do
			local item = turtle.getItemDetail(i)
			if item ~= nil then
				turtle.select(i)
				if blocksToDump[item.name] == true then
					print("Dumping: " .. item.name)
					turtle.dropDown(64)
				else
					print("Storing: " .. item.name)
					turtle.dropUp(64)
				end
			end
        end
		local cl = c.list()
		for i = 2, c.size() - 1 do
			if cl[i] ~= nil then
				invClient.store(config.chestOnHomeNetworkName, i, 64)
			end
		end
    end
    
    if turtle.getFuelLevel() < minFuel then
        local fuelAttempts = 0
        turtle.select(1)
        while turtle.getFuelLevel() < maxFuel do
            local cl = c.list()
            local fuelSlot = 1 -- c.size() - 1
			if invClient.get(config.fuelName, 16, config.chestOnHomeNetworkName, fuelSlot) == 0 then
                webhook("Not enough " .. config.fuelName .. " in storage to refuel with <@198768745278078976>")
            end
            if cl[fuelSlot] ~= nil and validFuels[cl[fuelSlot].name] and cl[fuelSlot].count > 32 then
				print(cl[fuelSlot].name)
                --[[
                    -- works in 1.12 but not in 1.18 it seems
				c.pushItems(
					"down",
					fuelSlot,
					cl[fuelSlot].count - 1
				)
                ]]--
                turtle.suckUp(16)
                turtle.refuel(64)
			elseif cl[fuelSlot] ~= nil and validFuels[cl[fuelSlot].name] ~= true then
				invClient.store(config.chestOnHomeNetworkName, fuelSlot, 64)
            else
                print("Waiting for fuel")
            end
            sleep(1)
            fuelAttempts = fuelAttempts + 1
            if fuelAttempts > 10 and turtle.getFuelLevel() > minFuel then
                print("Will try get more fuel later")
                break
            end
        end
    end
    
    Hold(config.slots.pick)
	turtle.select(config.slots.chest)
    bbTurtle.rDig("u")
    turtle.select(1)
end

function GetDir()
    Hold(config.slots.modem)
    local sx, sy, sz = Locate()
    Hold(config.slots.pick)
    bbTurtle.rForward()
    Hold(config.slots.modem)
    local nx, ny, nz = Locate()
    
    turtle.turnLeft()
    turtle.turnLeft()
    
    Hold(config.slots.pick)
    bbTurtle.rForward()
    
    turtle.turnLeft()
    turtle.turnLeft()
    
    if nz > sz then
        return "south"
    end
    if nz < sz then
        return "north"
    end
    if nx > sx then
        return "east"
    end
    if nx < sx then
        return "west"
    end
end

---

if turtle.getItemCount(config.slots.chest) == 0 then
    Hold(config.slots.pick)
    turtle.select(config.slots.chest)
    turtle.digUp()
end

local dir

function GotoRel(rx, ry, rz)
	print("Goto rel:")
	print(tostring(rx))
	print(tostring(ry))
	print(tostring(rz))
    if ry >= 0 then
        for i = 1, ry do
            bbTurtle.rUp()
        end
    else
        for i = -1, ry, -1 do
            bbTurtle.rDown()
        end
    end
    
    if rx > 0 then
        dir = Face(dir, "east")
        for i = 1, rx do
            bbTurtle.rForward()
        end
    elseif rx < 0 then
        dir = Face(dir, "west")
        for i = -1, rx, -1 do
            bbTurtle.rForward()
        end
    end
    
    if rz > 0 then
        dir = Face(dir, "south")
        for i = 1, rz do
            bbTurtle.rForward()
        end
    elseif rz < 0 then
        dir = Face(dir, "north")
        for i = -1, rz, -1 do
            bbTurtle.rForward()
        end
    end
end

function Cycle()
    local scanned = Scan(8)
    for noScanTimes = 1, 20 do
        local hsi, hsv, hsx, hsy, hsz, blockName = ScoreScan(scanned)
		Hold(config.slots.pick)
		
        if turtle.getItemCount(config.emptyOnSlot) > 0 or turtle.getFuelLevel() < minFuel then
            Inv()
        end
        
        turtle.select(1)
        
        -- if nothing found, move forward
        if hsi == nil then
            print("Nothing found")
            for i = 1, 8 do
                bbTurtle.rForward()
            end
            return
        else
            print(hsi, hsv, hsx, hsy, hsz, blockName)
            GotoRel(hsx, hsy, hsz)
            webhook("Mined: " .. blockName)
            --offset scanned by moved
            for k, v in pairs(scanned) do
                scanned[k].x = v.x - hsx
                scanned[k].y = v.y - hsy
                scanned[k].z = v.z - hsz
                if scanned[k].x == 0 and scanned[k].y == 0 and scanned[k].z == 0 then
                    scanned[k] = nil
                end
            end
        end
    end
end

function Run()
	while true do
		Network()
        
        Hold(config.slots.modem)
        local x, y, z = Locate()
        Hold(config.slots.pick)
        
        if y > maxY then
            for i = y-1, maxY, -1 do
                bbTurtle.rDown()
            end
        end
        
        if y < minY then
            for i = y+1, minY do
                bbTurtle.rUp()
            end
        end
        
		for c = 1, 2 do
			print("c: " .. tostring(c))
			Cycle()
			sleep(0)
		end
	end
end

webhook("Online")

Network()
Inv()

dir = GetDir()
print("facing " .. dir)

Run()