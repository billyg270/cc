local wRequire = require("wRequire")
local invClient = wRequire("invClient")
local tableFile = wRequire("tableFile")
local util = wRequire("util")

--local ioChestName = "minecraft:barrel_0"
--local clientName  = "clientLocal1"
--local modemName   = "bottom"

local items = {
    ["all"] = {
        ["minecraft:dirt"] = "Dirt",
        ["minecraft:stone"] = "Stone",
        ["minecraft:cobblestone"] = "Cobblestone",
    },
    ["recents"] = {
    },
    ["favorites"] = {
        "minecraft:cobblestone"
    }
}

local results = {}
local query = ""
local selectedIndex = 1
local screen = "search"--"favorites"

function Save()
    tableFile.save(items, "#items.lua")
end

function Load()
    items = tableFile.load("#items.lua", items)
end

Load()

if items.ioChestName == nil then
    print("IO chest name:")
    items.ioChestName = read()
    Save()
end

if items.clientName == nil then
    print("Client name:")
    items.clientName = read()
    Save()
end

if items.modemName == nil then
    print("Modem name:")
    items.modemName = read()
    Save()
end

if items.storageComputerID == nil then
    print("Storage Computer ID:")
    items.storageComputerID = tonumber(read())
    Save()
end

if items.arControllerName == nil then
    print("AR Controller Name:")
    items.arControllerName = read()
    Save()
end

if items.chatBoxName == nil then
    print("Chat Box Name:")
    items.chatBoxName = read()
    Save()
end


invClient.storageComputerID = items.storageComputerID
invClient.clientName = items.clientName

local ioChest = peripheral.wrap(items.ioChestName)
local arController = peripheral.wrap(items.arControllerName)
local chatBox = peripheral.wrap(items.chatBoxName)

local showOnAr = true

rednet.open(items.modemName)

function UpdateSearchResults()
    results = {}
    for k, v in pairs(items.all) do
        if string.find(string.lower(v), string.lower(query)) ~= nil then
            table.insert(results, k)
        end
    end
    if selectedIndex > util.tableSize(results) then
        selectedIndex = util.tableSize(results)
    end
    if selectedIndex <= 0 then
        selectedIndex = 1
    end
    SortResults()
end

function SortResults()
    local results2 = {}

    while util.tableSize(results) > 0 do
        local shortestKey = nil
        local shortest = nil
        for k, v in pairs(results) do
            if shortest == nil or string.len(v) < string.len(shortest) then
                shortest = v
                shortestKey = k
            end
        end
        if shortest == nil then
            break
        end
        table.insert(results2, shortest)
        results[shortestKey] = nil
    end

    results = results2
end

function RenderResults()
    if showOnAr == false then
        arController.clear()
        print("ar toggled off")
        return
    end
    
    local width, height = term.getSize()
    term.clear()
    arController.clear()
    term.setCursorPos(1, 1)
    term.setTextColour(colors.orange)
    if screen == "search" then
        term.write("search: " .. query .. "_")
        arController.drawString("search: " .. query .. "_", 1*10, 1*10, 0xf09a32)
    else
        term.write(screen)
        arController.drawString(screen, 1*10, 1*10, 1)
    end
    local resCount = util.tableSize(results)
    term.setCursorPos(width - string.len(tostring(resCount)) + 1, 1)
    term.write(resCount)
    arController.drawString(tostring(resCount), (-string.len(tostring(resCount)) - 10)*10, 1*10, 0xf09a32)
    local line = 1
    for k, v in pairs(results) do
        line = line + 1
        if line > 8 then
            return
        end
        term.setCursorPos(1, line)
        local color = 0x6e58ed
        if selectedIndex + 1 == line then
            term.setTextColour(colors.lightBlue)
            color = 0x7aa0c4
        else
            term.setTextColour(colors.blue)
        end
        if items.all[v] ~= nil then
            term.write(items.all[v])
            arController.drawString(items.all[v], 1*10, line*10, color)
        end
    end
end

function MenuKeys()
    while true do
        ---@diagnostic disable-next-line: undefined-field
        --local event, key, is_held = os.pullEvent("key")
        local event, username, message, uuid, isHidden = os.pullEvent("chat")
        if username == "BillBodkin" and isHidden then
            local resCount = util.tableSize(results)
            if message == "up" or message == "u" then
                selectedIndex = selectedIndex - 1
                if selectedIndex < 1 then
                    selectedIndex = 1
                end
                RenderResults()
            elseif message == "down" or message == "d" then
                selectedIndex = selectedIndex + 1
                if selectedIndex > resCount then
                    selectedIndex = resCount
                end
                if selectedIndex < 1 then
                    selectedIndex = 1
                end
                RenderResults()
            elseif message == "left" or message == "l" then
                if screen == "favorites" then
                    screen = "search"
                    UpdateSearchResults()
                end
                selectedIndex = 1
                RenderResults()
            elseif message == "right" or message == "r" then
                if screen == "search" then
                    screen = "favorites"
                    results = items.favorites
                end
                selectedIndex = 1
                RenderResults()
            elseif string.find(message, "^search ") or string.find(message, "^s ") then
                if screen == "search" then
                    query = string.gsub(message, "search ", "")
                    query = string.gsub(message, "s ", "")
                    UpdateSearchResults()
                    RenderResults()
                end
            elseif message == "count" or message == "c" then
                if results[selectedIndex] ~= nil then
                    term.clear()
                    term.setCursorPos(1, 1)
                    term.setTextColour(colors.green)
                    local count = invClient.count(results[selectedIndex])
                    term.clear()
                    term.setCursorPos(1, 1)
                    print("Count: " .. tostring(count))
                    chatBox.sendMessageToPlayer("Count: " .. tostring(count), "BillBodkin", "BB Storage")
                    term.setTextColour(colors.blue)
                    RenderResults()
                end
            elseif message == "get" or message == "g" then
                if results[selectedIndex] ~= nil then
                    term.clear()
                    term.setCursorPos(1, 1)
                    term.setTextColour(colors.green)
                    while invClient.getToAnySlot(results[selectedIndex], 64, items.ioChestName, ioChest) == false do
                        invClient.storeAllFromIoChest(items.ioChestName, ioChest)
                    end
                    RenderResults()
                end
            elseif message == "store" or message == "st" then
                term.clear()
                term.setCursorPos(1, 1)
                term.setTextColour(colors.green)
                invClient.storeAllFromIoChest(items.ioChestName, ioChest)
                RenderResults()
            elseif message == "refresh" or message == "re" then
                items["all"] = invClient.getItemNamesFromServer()
                UpdateSearchResults()
                RenderResults()
            elseif message == "toggle" or message == "t" then
                if showOnAr then
                    showOnAr = false
                else
                    showOnAr = true
                end
                
                chatBox.sendMessageToPlayer("Toggled " .. tostring(showOnAr), "BillBodkin", "BB Storage")
                RenderResults()
            end
        end
    end
end

items["all"] = invClient.getItemNamesFromServer()

UpdateSearchResults()
RenderResults()

MenuKeys()