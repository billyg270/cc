local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")
local inv = wRequire("invClient")

local config = tableFile.load("#store.lua", nil)

if config == nil then
	config = {}
	print("Modem Side:")
	config.modemSide = read()
	print("Chest Network Name:")
	config.chestNetworkName = read()
	print("Storage server ID:")
	config.storageServerID = tonumber(read())
end

tableFile.save(config, "#store.lua")

inv.storageComputerID = config.storageServerID

rednet.open(config.modemSide)

while true do
	local chest = peripheral.wrap(config.chestNetworkName)
	if #(chest.list()) > (chest.size() / 2) then
		print("Storing...")
		inv.storeAllFromIoChest(config.chestNetworkName)
	else
		print("Not enough items to rerquest store")
	end
	longSleep(60)
end