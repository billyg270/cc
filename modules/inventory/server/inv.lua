local wRequire = require("wRequire")
local util = wRequire("util")
local tableFile = wRequire("tableFile")
local itemF = wRequire("storageServerItem")

local module = {}

module.inventory = {}

--- Inventory file ---

module.save = function()
	tableFile.save(module.inventory, "#inventory.lua")
end

module.load = function()
	module.inventory = tableFile.load("#inventory.lua", module.inventory)
	if module.inventory.chestsName == nil then
		print("Chests name: ")
		print("eg: minecraft:chest")
		print("eg: ironchest:obsidian_chest")
		module.inventory.chestsName = read()
	end
end

module.load()

module.chests = { peripheral.find(module.inventory.chestsName) }

--- Inventory mapping ---

module.mapInventory = function()
	module.inventory["items"] = {}
	module.inventory["itemLimits"] = {}--max stack size per item type, interact with this via GetItemLimit(chestName, slot, itemnbtAndName)
	module.inventory["partialStacks"] = {}--stacks that are not completly full or empty, should try to store or get from these first
	module.inventory["itemCounts"] = {}
	module.inventory["itemNames"] = {}
	
	for chestName, chest in pairs(module.chests) do
		util.log("Mapping inventory: " .. tostring(chestName) .. " / " .. tostring(util.tableSize(module.chests)))
		local chestItems = chest.list()
		for slot = 1, chest.size() do
			--local item = chest.getItemDetail(slot)
			local item = chestItems[slot]
			local itemDetail = chest.getItemDetail(slot)
			if item == nil then
				module.setSlot("", 0, chestName, slot)
			else
				module.setSlot(itemF.getItemNameAndNbt(itemDetail), item.count, chestName, slot)
				module.addItemCount(itemF.getItemNameAndNbt(itemDetail), item.count)
			end
		end
	end
	module.save()
	util.log("Inventory mapped")
end

-- Get all locations of an item
module.getItemInv = function(itemnbtAndName)
	if module.inventory["items"][itemnbtAndName] == nil then
		module.inventory["items"][itemnbtAndName] = {}
	end
	if itemnbtAndName ~= "" then
		if module.inventory["partialStacks"][itemnbtAndName] == nil then
			module.inventory["partialStacks"][itemnbtAndName] = {}
		end
		if module.inventory["itemCounts"][itemnbtAndName] == nil then
			module.inventory["itemCounts"][itemnbtAndName] = 0
		end
	end
	return module.inventory["items"][itemnbtAndName]
end

module.addItemCount = function(itemnbtAndName, count)
	module.getItemInv(itemnbtAndName)
	module.inventory["itemCounts"][itemnbtAndName] = module.inventory["itemCounts"][itemnbtAndName] + count
end

module.getItemLimit = function(chestName, slot, itemnbtAndName)
	if module.inventory["itemLimits"][itemnbtAndName] == nil then
		local itemDetail = nil
		if module.chests[chestName] ~= nil then
			itemDetail = module.chests[chestName].getItemDetail(slot)
		else
			itemDetail = peripheral.wrap(chestName).getItemDetail(slot)
		end
		
		if itemDetail ~= nil then
			module.inventory["itemLimits"][itemnbtAndName] = itemDetail.maxCount
		else
			return 64
		end
	end
	return module.inventory["itemLimits"][itemnbtAndName]
end

module.getItemName = function(chestName, slot, itemnbtAndName)
	if module.inventory["itemNames"][itemnbtAndName] == nil then
		if chestName == nil or slot == nil then
			return nil
		end
		local itemDetail = nil
		if module.chests[chestName] ~= nil then
			itemDetail = module.chests[chestName].getItemDetail(slot)
		else
			itemDetail = peripheral.wrap(chestName).getItemDetail(slot)
		end
		
		if itemDetail ~= nil then
			module.inventory["itemNames"][itemnbtAndName] = itemDetail.displayName
		else
			return nil
		end
	end
	return module.inventory["itemNames"][itemnbtAndName]
end

-- Set a slot in the chests for where an item is
module.setSlot = function(itemnbtAndName, count, chestName, slot)
	if count < 0 then
		util.err({
			["type"] = "Neg slot",
			["itemnbtAndName"] = itemnbtAndName,
			["itemCount"] = count,
			["chestName"] = chestName,
			["chestSlot"] = slot
		})
	end
	
	--set new value in items table
	module.getItemInv(itemnbtAndName)
	if module.inventory["items"][itemnbtAndName][chestName] == nil then
		module.inventory["items"][itemnbtAndName][chestName] = {}
	end
	if itemnbtAndName ~= "" then
		if module.inventory["partialStacks"][itemnbtAndName][chestName] == nil then
			module.inventory["partialStacks"][itemnbtAndName][chestName] = {}
		end
	end
	
	module.getItemInv("")
	if module.inventory["items"][""][chestName] == nil then
		module.inventory["items"][""][chestName] = {}
	end
	
	if count == 0 then
		if itemnbtAndName ~= "" then
			module.inventory["items"][itemnbtAndName][chestName][slot] = nil
			module.inventory["partialStacks"][itemnbtAndName][chestName][slot] = nil
		end
		module.inventory["items"][""][chestName][slot] = 0
	else
		module.inventory["items"][itemnbtAndName][chestName][slot] = count
		module.inventory["items"][""][chestName][slot] = nil
		
		if itemnbtAndName ~= "" then
			if count < module.getItemLimit(chestName, slot, itemnbtAndName) then
				module.inventory["partialStacks"][itemnbtAndName][chestName][slot] = true
			else
				module.inventory["partialStacks"][itemnbtAndName][chestName][slot] = nil
			end
		end
	end
	
	module.getItemName(chestName, slot, itemnbtAndName)--will set it if not on there

	--Save()
end

--- Get free slots avaliable ---
module.countEmptySlots = function()
	if module.inventory.items[""] == nil then
		return 0
	end
	local emptySlotCount = 0
	for ecsn, ecs in pairs(module.inventory.items[""]) do
		for esn, es in pairs(ecs) do
			emptySlotCount = emptySlotCount + 1
		end
	end
	return emptySlotCount
end

--- IO ---

-- Take from outChest and store
module.store = function(fromChest, fromSlot, toMove)
	if module.countEmptySlots() == 0 then
		util.log("Inventory full")
		return 0
	end
	local item = peripheral.wrap(fromChest).getItemDetail(fromSlot)
	if item ~= nil then
		local totalMoved = 0
		local maxStackSize = module.getItemLimit(fromChest, fromSlot, itemF.getItemNameAndNbt(item))--peripheral.wrap(fromChest).getItemLimit(fromSlot)
		if toMove == nil then
			toMove = maxStackSize
		end
		toMove = math.min(item.count, toMove)
		util.log("Storing " .. tostring(toMove) .. " " .. itemF.getItemNameAndNbt(item))
		function StoreToSlot(slotItemnbtAndName)
			local itemInv = module.getItemInv(slotItemnbtAndName)
			for chestName, chestSlots in pairs(itemInv) do
				for slot, count in pairs(chestSlots) do
					if count < maxStackSize then
						local moved = module.chests[chestName].pullItems(fromChest, fromSlot, toMove, slot)
						toMove = toMove - moved
						totalMoved = totalMoved + moved
						if moved > 0 then
							module.setSlot(itemF.getItemNameAndNbt(item), count + moved, chestName, slot)
						end
						if toMove == 0 then
							return true
						end
						if toMove < 0 then
							util.err({
								["type"] = "Over move",
								["chestName"] = chestName,
								["itemnbtAndName"] = itemF.getItemNameAndNbt(item),
								["slotItemnbtAndName"] = slotItemnbtAndName,
								["totalToMove"] = toMove,
								["fromChest"] = fromChest,
								["fromSlot"] = fromSlot
							})
						end
					end
				end
			end
			return false
		end
		if maxStackSize ~= 1 and StoreToSlot(itemF.getItemNameAndNbt(item)) then
			util.log("Stored " .. tostring(totalMoved) .. " " .. itemF.getItemNameAndNbt(item))
			module.addItemCount(itemF.getItemNameAndNbt(item), totalMoved)
			return totalMoved
		elseif StoreToSlot("") then
			util.log("Stored " .. tostring(totalMoved) .. " " .. itemF.getItemNameAndNbt(item))
			module.addItemCount(itemF.getItemNameAndNbt(item), totalMoved)
			return totalMoved
		else
			util.err({
				["type"] = "Desync"
			})
		end
	end
	util.log("Was asked to store slot but is empty")
	return 0
end

-- Get from storage to outChest by name
module.get = function(itemnbtAndName, count, toChest, toSlot)
	util.log("Getting " .. tostring(count) .. " " .. itemnbtAndName)
	local totalMoved = 0
	local itemInv = module.getItemInv(itemnbtAndName)--gets all slots where this item should be from file
	for chestName, chestSlots in pairs(itemInv) do
		for slotName, slotCount in pairs(chestSlots) do
			local slotItemDetail = module.chests[chestName].getItemDetail(slotName)
			if slotItemDetail == nil or itemF.getItemNameAndNbt(slotItemDetail) ~= itemnbtAndName or slotItemDetail.count ~= slotCount then
				util.err({
					["type"] = "Slot desync",
					["chestName"] = chestName,
					["slotName"] = slotName,
					["expectedItemnbtAndName"] = itemnbtAndName,
					["expectedItemCount"] = slotCount,
					["actualItemnbtAndName"] = util.ternary(slotItemDetail == nil, "Air", itemF.getItemNameAndNbt(slotItemDetail)),
					["actualItemCount"] = util.ternary(slotItemDetail == nil, 0, slotItemDetail.count),
				})
			end
			local moved = module.chests[chestName].pushItems(toChest, slotName, count - totalMoved, toSlot)
			totalMoved = totalMoved + moved
			--Log("Get item")
			--Log(itemnbtAndName)
			if moved == 0 then
				--cant move any more as output slot full / diffrent item
				util.log("Got " .. tostring(totalMoved) .. " " .. itemnbtAndName)
				module.addItemCount(itemnbtAndName, -totalMoved)
				return totalMoved
			end
			module.setSlot(itemnbtAndName, slotCount - moved, chestName, slotName)
			if count == totalMoved then
				util.log("Got " .. tostring(totalMoved) .. " " .. itemnbtAndName)
				module.addItemCount(itemnbtAndName, -totalMoved)
				return totalMoved
			end
			if totalMoved > count then
				util.err({
					["type"] = "Over move",
					["chestName"] = chestName,
					["slotName"] = slotName,
					["itemnbtAndName"] = itemnbtAndName,
					["totalToMove"] = count,
					["toChest"] = toChest,
					["toSlot"] = toSlot
				})
			end
		end
	end
	util.log("Got " .. tostring(totalMoved) .. " " .. itemnbtAndName)
	module.addItemCount(itemnbtAndName, -totalMoved)
	return totalMoved
end

return module