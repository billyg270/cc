-- require and if fail, wget then require
-- wget https://gitlab.com/billyg270/cc/-/raw/master/modules/wRequire.lua

local bbModules = "https://gitlab.com/billyg270/cc/-/raw/master/modules/"

local fileUrls = {
	["DiscordHook"] = "https://raw.githubusercontent.com/Wendelstein7/DiscordHook-CC/master/DiscordHook.lua",
	["util"] = bbModules .. "util.lua",
	["dimGps"] = bbModules .. "dimGps.lua",
	["goto"] = bbModules .. "goto.lua",
	["bbTurtle"] = bbModules .. "turtle/bbTurtle.lua",
	["bbTurtleDock"] = bbModules .. "turtle/bbTurtleDock.lua",
	["farm"] = bbModules .. "turtle/farm.lua",
	["tableFile"] = bbModules .. "tableFile.lua",
	["longSleep"] = bbModules .. "longSleep.lua",
	["storageServerInv"] = bbModules .. "inventory/server/inv.lua",
	["storageServerAe2InvPp1"] = bbModules .. "inventory/server/ae2InvPp1.lua",
	["storageServerCrafting"] = bbModules .. "inventory/server/crafting.lua",
	["storageServerMachine"] = bbModules .. "inventory/server/machine.lua",
	["storageServerItem"] = bbModules .. "inventory/server/item.lua",
	["invClient"] = bbModules .. "inventory/client/inv.lua",
}

local function wRequire(fileName)
	if fs.exists(fileName) == false then
		if fileUrls[fileName] == nil then
			error("Could not find URL for module " .. fileName ..  " in wRequire table")
		end
		shell.execute("wget", fileUrls[fileName], fileName .. ".lua")
	end
	return require("./" .. fileName)
end

return wRequire