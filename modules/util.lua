local module = {}

module.tableSize = function(tab)
	if tab == nil then
		return 0
	end
	local tabSize = 0
	for aTab, bTab in pairs(tab) do
		tabSize = tabSize + 1
	end
	return tabSize
end

module.tableContainsValue = function(tab, value)
	if tab == nil then
		return false
	end
	for k, v in pairs(tab) do
		if v == value then
			return true
		end
	end
	return false
end

module.ternary = function(cond, T, F)
	if cond then
		return T
	else
		return F
	end
end

module.log = function(msg)
	print(msg)
end

module.err = function(e)
	module.log("ERROR!")
	module.log(textutils.serialize(e))
	error(e)
end

module.splitString = function(inputstr, sep)--https://stackoverflow.com/a/7615129/9160599
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		table.insert(t, str)
    end
    return t
end

module.firstCharUpper = function(str)--https://stackoverflow.com/a/2421746/9160599
    return (str:gsub("^%l", string.upper))
end

module.padString = function (str, len)
	if str == nil then
		str = ""
	end
	return string.rep(" ", len - string.len(str)) .. str
end

module.getIndex = function(v, t)
    for tk, tv in pairs(t) do
        if tv == v then
            return tk
        end
    end
end

return module