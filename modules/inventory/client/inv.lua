local wRequire = require("wRequire")
local util = wRequire("util")



local module = {
	["storageComputerID"] = 6,
	["clientName"] = "StorageClient-" .. tostring(math.random(1, 10000000)),
	["silent"] = false
}

module.getChest = function(chestNameOrPerp)
    if type(chestNameOrPerp) == "string" then
        return peripheral.wrap(chestNameOrPerp)
    end
    return chestNameOrPerp
end

module.print = function(msg)
    if module.silent == false then
        print(msg)
    end
end

module.get = function(itemName, itemCount, chest, slot)
    while true do
        module.print("Getting " .. tostring(itemCount) .. "x  " .. itemName .. " to " .. chest .. " slot " .. tostring(slot))
        rednet.send(module.storageComputerID, {
            ["action"] = "get",
            ["instructionRef"] = "outChestTopup",
            ["chest"] = chest,
            ["slot"] = slot,
            ["nbtAndName"] = itemName,
            ["count"] = itemCount
        }, "inv")
        while true do
            local id, msg = rednet.receive("invResp", 2)
            if not id then
                module.print("Timed out")
                break
            end
            if id == module.storageComputerID then
                if msg["status"] == "success" then
                    module.print("Got " .. tostring(msg["moved"]) .. " " .. chest .. " slot " .. tostring(slot))
                    return msg["moved"]
                else
                    module.print("NOT Got " .. chest .. " slot " .. tostring(slot) .. " - " .. msg["message"])
                    return 0
                end
            end
            sleep(0)
        end
    end
end

module.getToAnySlot = function(itemName, itemCount, chest, chestNameFromLocal)
	if chestNameFromLocal == nil then
		chestNameFromLocal = chest
	end
	local ioChest = module.getChest(chestNameFromLocal)
    local ioChestItems = ioChest.list()
    for i = 1, ioChest.size() do
        if ioChestItems[i] == nil then
            module.get(itemName, itemCount, chest, i)
            return true
        end
    end
    return false
end

module.store = function(chest, slot, count)
    while true do
        module.print("Storing " .. tostring(count) .. " " .. chest .. " slot " .. tostring(slot))
        rednet.send(module.storageComputerID, {
            ["action"] = "store",
            ["instructionRef"] = module.clientName,
            ["chest"] = chest,
            ["slot"] = slot,
            ["count"] = count
        }, "inv")
        while true do
            local id, msg = rednet.receive("invResp", 2)
            if not id then
                module.print("Timed out")
                break
            end
            if id == module.storageComputerID then
                if msg["status"] == "success" then
                    module.print("Stored " .. tostring(msg["moved"]) .. " " .. chest .. " slot " .. tostring(slot))
                    return msg["moved"]
                else
                    module.print("NOT Stored " .. chest .. " slot " .. tostring(slot) .. " - " .. msg["message"])
                    return 0
                end
            end
            sleep(0)
        end
        sleep(1)
    end
end

module.storeAllFromIoChest = function(chest, chestNameFromLocal)
	if chestNameFromLocal == nil then
		chestNameFromLocal = chest
	end
	local ioChest = module.getChest(chestNameFromLocal)
    local ioChestItems = ioChest.list()
    for i = 1, ioChest.size() do
        if ioChestItems[i] ~= nil then
            module.store(chest, i, 64)
        end
    end
end

module.count = function(itemName)
    while true do
        module.print("Getting count of: " .. itemName)
        rednet.send(module.storageComputerID, {
            ["action"] = "count",
            ["instructionRef"] = module.clientName,
            ["nbtAndName"] = itemName
        }, "inv")
        while true do
            local id, msg = rednet.receive("invResp", 2)
            if not id then
                module.print("Timed out")
                break
            end
            if id == module.storageComputerID then
                if msg["status"] == "success" then
                    module.print("Count: " .. tostring(msg["count"]))
                    return msg["count"]
                else
                    module.print("Get count failed: " .. msg["message"])
                    return nil
                end
            end
            sleep(0)
        end
        sleep(1)
    end
end

module.getCounts = function()
    while true do
        module.print("Getting getCounts")
        rednet.send(module.storageComputerID, {
            ["action"] = "getCounts",
            ["instructionRef"] = module.clientName
        }, "inv")
        while true do
            local id, msg = rednet.receive("invResp", 2)
            if not id then
                module.print("Timed out")
                break
            end
            if id == module.storageComputerID then
                if msg["status"] == "success" then
                    module.print("Got counts")
                    return msg["counts"]
                else
                    module.print("Get count failed: " .. msg["message"])
                    return nil
                end
            end
            sleep(0)
        end
        sleep(1)
    end
end

module.getItemNamesFromServer = function()
    while true do
        module.print("Getting item names list")
        rednet.send(module.storageComputerID, {
            ["action"] = "getNames",
            ["instructionRef"] = module.clientName
        }, "inv")
        while true do
            local id, msg = rednet.receive("invResp", 2)
            if not id then
                module.print("Timed out")
                break
            end
            if id == module.storageComputerID then
                if msg["status"] == "success" then
                    return msg["names"]
                else
                    module.print("Get items name list failed: " .. msg["message"])
                    return nil
                end
            end
            sleep(0)
        end
        sleep(1)
    end
end

return module