local module = {}

module.farm = function(width, depth, step)
	step()
	for w = 1, width do
		for d = 1, depth - 1 do
			step()
		end
		if w < width then
			if w % 2 == 0 then
				turtle.turnLeft()
				step()
				turtle.turnLeft()
			else
				turtle.turnRight()
				step()
				turtle.turnRight()
			end
		end
	end
	if width % 2 == 1 then
		turtle.turnRight()
		turtle.turnRight()
		for d = 1, depth - 1 do
			step()
		end
	end
	turtle.turnRight()
	for w = 1, width - 1 do
		step()
	end
	turtle.turnRight()
	turtle.back()
end

return module