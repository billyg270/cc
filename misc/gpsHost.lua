-- will be connected to all 4 and will also give dimension / world

local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")

local config = tableFile.load("#gpsHost.lua", nil)

if config == nil then
	config = {}
	
	print("Dimension:")
	print("eg: overworld, nether, moon, mars")
	config.dimension = read()
	
	config.modems = {}
	
	local c = 0
	while true do
		c = c + 1
		print("Modem " .. tostring(c) .. " name:")
		print("eg: bottom, modem_0")
		print("Leave blank if no more modems to add")
		local modemName = read()
		if modemName == nil or modemName == "" then
			break
		end
		
		print("Modem x (of block it is placed on)")
		local x = tonumber(read())
		print("Modem y (of block it is placed on)")
		local y = tonumber(read())
		print("Modem z (of block it is placed on)")
		local z = tonumber(read())
		
		config.modems[c] = {
			["name"] = modemName,
			["x"] = x,
			["y"] = y,
			["z"] = z
		}
	end
end

tableFile.save(config, "#gpsHost.lua")

if config.modems[4] == nil then
	error("Not enough modems added, please update config")
end

local modems = {}
for mk, mv in pairs(config.modems) do
	modems[mk] = peripheral.wrap(mv.name)
end

modems[1].open(gps.CHANNEL_GPS)

while true do
	local event, side, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	if channel == gps.CHANNEL_GPS and replyChannel == gps.CHANNEL_GPS and message == "PING" then
		for mk, mv in pairs(modems) do
			mv.transmit(gps.CHANNEL_GPS, gps.CHANNEL_GPS, {
				config.modems[mk].x,
				config.modems[mk].y,
				config.modems[mk].z--,
				--config.dimension
			})
			--commented dim for now, built in client expects table len of 3, can re-add when make custom client
		end
	end
end