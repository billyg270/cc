-- Place chest above for gravel
-- Place chest below for dirt
-- Leave space infront for placing cource dirt
-- Equip shovel and crafting table

function fillInv()
    if turtle.getItemCount(16) > 0 then
        return
    end

    while turtle.getItemCount(1) < 16 do
        turtle.select(1)
        print("Getting dirt from chest below")
        turtle.suckDown(16 - turtle.getItemCount())
        sleep(0.1)
    end

    while turtle.getItemCount(2) < 16 do
        turtle.select(2)
        print("Getting gravel from chest above")
        turtle.suckUp(16 - turtle.getItemCount())
        sleep(0.1)
    end

    while turtle.getItemCount(5) < 16 do
        turtle.select(5)
        print("Getting gravel from chest above")
        turtle.suckUp(16 - turtle.getItemCount())
        sleep(0.1)
    end

    while turtle.getItemCount(6) < 16 do
        turtle.select(6)
        print("Getting dirt from chest below")
        turtle.suckDown(16 - turtle.getItemCount())
        sleep(0.1)
    end
end

function craftCourse()
    if turtle.getItemCount(16) == 0 then
        turtle.select(16)
        turtle.craft()
    end
end

function convertCourse()
    while turtle.getItemCount(15) > 0 do
        turtle.select(15)
        turtle.dropDown()
        print("Dropping excess dirt into chest below")
    end

    while turtle.getItemCount(16) > 0 do
        turtle.select(16)
        turtle.place()
        turtle.place()

        if turtle.getItemCount(1) < 16 then
            print("Placing mined dirt into slot 1")
            turtle.select(1)
        elseif turtle.getItemCount(6) < 16 then
            print("Placing mined dirt into slot 6")
            turtle.select(6)
        else
            print("Placing mined dirt into slot 15 for chest")
            turtle.select(15)
        end

        turtle.dig()
    end

    turtle.select(15)

    while turtle.getItemCount(15) > 0 do
        turtle.dropDown()
        print("Dropping excess dirt into chest below")
    end
end

while true do
    fillInv()
    craftCourse()
    convertCourse()
    sleep(1)
end