local args = {...}

local wRequire = require("wRequire")

--[[
	TODO
	
	Calculate target height based on an average of how far the player falls over a given distance at (whatever the best pitch is)
	The target height should match the slope based on how close they are to the target
]]

local route = {
	[1] = {
		["x"] = tonumber(args[1]),
		["y"] = tonumber(args[2]),
		["z"] = tonumber(args[3])
	}
}
local routeIndex = 1



local positionHistory = {}
local velocityHistory = {}
local trajectoryAngleHistory = {}

local function updatePosition()
	for i = 100, 1, -1 do
		positionHistory[i + 1] = positionHistory[i]
	end
	
	local x, y, z = gps.locate()
	
	positionHistory[1] = {
		["x"] = x,
		["y"] = y,
		["z"] = z
	}
end

local function updateVelocity()
	for i = 100, 1, -1 do
		velocityHistory[i + 1] = velocityHistory[i]
	end
	
	if(positionHistory[1] ~= nil and positionHistory[2] ~= nil) then
		velocityHistory[1] = {
			["x"] = positionHistory[1].x - positionHistory[2].x,
			["y"] = positionHistory[1].y - positionHistory[2].y,
			["z"] = positionHistory[1].z - positionHistory[2].z
		}
	end
end

local function updateTrajectoryAngle()
	for i = 100, 1, -1 do
		trajectoryAngleHistory[i + 1] = trajectoryAngleHistory[i]
	end
	
	if(velocityHistory[1] ~= nil) then
		trajectoryAngleHistory[1] = {
			["pitch"] = (math.atan2(velocityHistory[1].y, math.sqrt(math.pow(velocityHistory[1].x, 2) + math.pow(velocityHistory[1].z, 2))) * 57.2958) * -1,
			["yaw"] = (math.atan2(velocityHistory[1].z, velocityHistory[1].x) * 57.2958) + 270,
		}
		
		while trajectoryAngleHistory[1].yaw > 180 do
			trajectoryAngleHistory[1].yaw = trajectoryAngleHistory[1].yaw - 360
		end
		
		while trajectoryAngleHistory[1].yaw < -180 do
			trajectoryAngleHistory[1].yaw = trajectoryAngleHistory[1].yaw + 360
		end
	end
end

local function calculateTargetTrajectoryAngle()
	if(positionHistory[1] == nil or route[routeIndex] == nil) then
		return nil
	end
	
	local toRet = {
		["pitch"] = (math.atan2(positionHistory[1].y - route[routeIndex].y, math.sqrt(math.pow(positionHistory[1].x - route[routeIndex].x, 2) + math.pow(positionHistory[1].z - route[routeIndex].z, 2))) * 57.2958) * -1,
		["yaw"] = (math.atan2(positionHistory[1].z - route[routeIndex].z, positionHistory[1].x - route[routeIndex].x) * 57.2958) + 90,
	}
	
	while toRet.yaw > 180 do
		toRet.yaw = toRet.yaw - 360
	end
	
	while toRet.yaw < -180 do
		toRet.yaw = toRet.yaw + 360
	end
	
	return toRet
end

local function calculateTrajectoryAngleDiff(targetTrajectoryAngle)
	if(trajectoryAngleHistory[1] == nil or route[routeIndex] == nil) then
		return nil
	end
	
	--local targetTrajectoryAngle = calculateTargetTrajectoryAngle()
	
	if(targetTrajectoryAngle == nil) then
		return nil
	end
	
	local toRet = {
		["pitch"] = targetTrajectoryAngle.pitch - trajectoryAngleHistory[1].pitch,
		["yaw"] = targetTrajectoryAngle.yaw - trajectoryAngleHistory[1].yaw
	}
	
	while toRet.yaw > 180 do
		toRet.yaw = toRet.yaw - 360
	end
	
	while toRet.yaw < -180 do
		toRet.yaw = toRet.yaw + 360
	end
	
	return toRet
end

local function flatDistance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.z - b.z, 2))
end

local function distance(a, b)
	return math.sqrt(math.pow(a.y - b.y, 2) + math.pow(flatDistance(a, b), 2))
end

local function printCoords(tab)
	if tab == nil then
		return
	end
	
	for k, v in pairs(tab) do
		local s = string.format("%.3f", v)
		
		print(string.format("%-10s",k) .. ": " .. string.format("%10s", s))
	end
end

local function loop()
	while true do
		updatePosition()
		updateVelocity()
		updateTrajectoryAngle()
		
		term.clear()
		term.setCursorPos(1, 1)
		
		--print("-- Position --")
		--printCoords(positionHistory[1])
		
		--print("-- Velocity --")
		--printCoords(velocityHistory[1])
		
		--print("-- Trajectory --")
		--printCoords(trajectoryAngleHistory[1])
		
		print("-- Next waypoint --")
		printCoords(route[routeIndex])
		if positionHistory[1] ~= nil then
			printCoords({ ["Distance"] = distance(positionHistory[1], route[routeIndex])})
		else
			print("Loading...")
		end
		
		--print("-- Target Trajectory --")
		--printCoords(calculateTargetTrajectoryAngle())
		
		--print("-- Target Trajectory Angle Diff --")
		--printCoords(calculateTrajectoryAngleDiff())
		
		local targetTrajectoryAngle = calculateTargetTrajectoryAngle()
		local tad = calculateTrajectoryAngleDiff(targetTrajectoryAngle)
		
		print("")
		
		if tad == nil then
			print("Loading...")
		else
			-- if(tad.pitch > 10) then
			-- 	print("Sharp up")
			-- end
			
			-- if(tad.pitch > 10) then
			-- 	print("Up")
			-- end
			
			-- if(tad.pitch > 2) then
			-- 	print("Slight Up")
			-- end
			
			-- if(tad.pitch < -10) then
			-- 	print("Sharp up")
			-- end
			
			-- if(tad.pitch < -10) then
			-- 	print("Up")
			-- end
			
			-- if(tad.pitch < -2) then
			-- 	print("Slight Up")
			-- end
			
			if(tad.yaw > 0) then
				if(tad.yaw > 90) then
					print("Sharp Right")
				elseif(tad.yaw > 45) then
					print("Right")
				elseif(tad.yaw > 5) then
					print("Slight Right")
				elseif(tad.yaw > 2) then
					print("Touch Right")
				else
					print("Stay Straight")
				end
			else
				if(tad.yaw < -90) then
					print("Sharp Left")
				elseif(tad.yaw < -45) then
					print("Left")
				elseif(tad.yaw < -5) then
					print("Slight Left")
				elseif(tad.yaw < -2) then
					print("Touch Left")
				else
					print("Stay Straight")
				end
			end
		end
		
		sleep(1)
	end
end



parallel.waitForAny(loop)