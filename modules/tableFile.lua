local function save(tab, filename)
    local file = fs.open(filename, "w")
    file.write(textutils.serialise(tab))
    file.close()
end

local function load(filename, default)
    local file = fs.open(filename, "r")
    if file == nil then
        return default
    end
    local toRet = textutils.unserialise(file.readAll())
    file.close()
	return toRet
end

return {
	["save"] = save,
	["load"] = load
}