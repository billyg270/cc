local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load("#railwaySignal.lua", nil)

if config == nil then
	config = {}
	
	print("Signal ID:")
	config.signalID = read()
	
	print("Server Computer ID:")
	config.serverComputerID = tonumber(read())
	
	config.modemSide = "left"
	config.detectorRailSide = "back"
	config.poweredRailSide = "right"
	config.monitorSide = "top"
	config.invertPowerState = false
	config.points = {}
end

if config.points == nil then
	config.points = {}-- For older signals with their configs not having it
end

tableFile.save(config, "#railwaySignal.lua")

local points = {}
for k, v in pairs(config.points) do
	points[k] = peripheral.wrap(v)
	
	if points[k] == nil then
		print("Cant wrap peripheral for point: " .. k)
	end
end

rednet.open(config.modemSide)
local monitor = peripheral.wrap(config.monitorSide)

local networkQueue = {}

local signalRoutingSidesSet = {}

local function setSignalRouting(signalRoutingRedstoneSignals)
	local sides = {
		"front",
		"back",
		"left",
		"right",
		"top",
		"bottom"
	}
	
	for k, v in pairs(signalRoutingSidesSet) do
		if util.tableContainsValue(sides, k) then
			redstone.setOutput(k, false)
			signalRoutingSidesSet[k] = false
		end
	end
	
	for k, v in pairs(signalRoutingRedstoneSignals) do
		if util.tableContainsValue(sides, k) then
			signalRoutingSidesSet[k] = true
			redstone.setOutput(k, v)
		elseif points[k] ~= nil then
			for k2, v2 in pairs(sides) do
				points[k].setOutput(v2, v)
			end
		else
			print("!!! Unknown point to set: " .. k)
		end
	end
end

local function setSignalDisplayState(signalDisplayState)
	if monitor ~= nil then
		if signalDisplayState == true then
			monitor.setBackgroundColour(colors.lime)
		else
			monitor.setBackgroundColour(colors.red)
		end
		monitor.clear()
	else
		redstone.setOutput(config.monitorSide, signalDisplayState)
	end
end

local function setSignalPowerState(signalPowerState)
	if config.invertPowerState == true then
		redstone.setOutput(config.poweredRailSide, signalPowerState == false)
	else
		redstone.setOutput(config.poweredRailSide, signalPowerState)
	end
end

local function processNetworkMessage(computerIdFrom, message)
	if type(message) ~= "table" then
		print("Won't process network message as not a table")
		return
	end
	
	if message.type == nil then
		print("Won't process network message as 'type' key not set in table")
		return
	end
	
	if message.type == "railwaySignalState" then
		if message.signalID == nil then
			print("Won't process railwaySignalState message as 'signalID' key not set in table")
			return
		end
		
		if message.signalDisplayState == nil then
			print("Won't process railwaySignalState message as 'signalDisplayState' key not set in table")
			return
		end
		
		if message.signalPowerState == nil then
			print("Won't process railwaySignalState message as 'signalPowerState' key not set in table")
			return
		end
		
		if message.signalID == config.signalID then
			print(textutils.formatTime(os.time()))
			print("Updating signal state:")
			print("Display State: " .. tostring(message.signalDisplayState))
			print("Power State: " .. tostring(message.signalPowerState))
			print("Switches: " .. textutils.serialize(message.signalRoutingRedstoneSignals))
			
			setSignalRouting(message.signalRoutingRedstoneSignals)
			setSignalDisplayState(message.signalDisplayState)
			setSignalPowerState(message.signalPowerState)
		end
		
		return
	end
	
	print("Unknown message type '" .. message.type .. "'")
end

setSignalDisplayState(false)
setSignalPowerState(false)

rednet.send(config.serverComputerID, {
	["type"] = "railwayRequestSignalState",
	["signalID"] = config.signalID
}, "railwaySignal")

local function networkLoop()
	while true do
		local id, message = rednet.receive("railwaySignal")
		table.insert(networkQueue, {
			["computerID"] = id,
			["message"] = message
		})
	end
end

local function processNetworkMessagesLoop()
	while true do
		if util.tableSize(networkQueue) > 0 then
			local networkIn = table.remove(networkQueue, 1)
			processNetworkMessage(networkIn.computerID, networkIn.message)
		end
		sleep(0)
	end
end

local function redstoneLoop()
	local powerInputNum = 0--Makes it so it pretends an input lasts 1 second longer then it actually does, will hopefully prevent blips where there is too big of a gap within a train
	local detectorRailWasPowered = false
	
	while true do
		local inputRaw = redstone.getInput(config.detectorRailSide)
		
		if inputRaw then
			powerInputNum = 100
		elseif powerInputNum > 0 then
			powerInputNum = powerInputNum - 1
		end
		
		local input = powerInputNum > 0
		
		if input ~= detectorRailWasPowered then
			rednet.send(config.serverComputerID, {
				["type"] = "railwaySignalDetectorRail",
				["signalID"] = config.signalID,
				["powered"] = input
			}, "railwaySignal")
			
			detectorRailWasPowered = input
		end
		sleep(0.01)
	end
end

local function updateMonitorLoop()
	-- Because players who see the monitor after it was last rendered will just see a blank screen
	
	if monitor == nil then
		return
	end
	
	while true do
		sleep(15)
		-- Dont need to set colour here as aready stored using setBackgroundColour
		monitor.clear()
	end
end

parallel.waitForAll(networkLoop, processNetworkMessagesLoop, redstoneLoop, updateMonitorLoop)
