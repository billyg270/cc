local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")

local config = tableFile.load("#easyVilIronFarm.lua", nil)

-- Should be run on turtles who have access to crafting

if config == nil then
	config = {}
	print("Modem side:")
	config.modemSide = read()
	print("Iron Output Chest ID:")
	config.ironChestID = read()
	print("Red Dye Output Chest ID:")
	config.redDyeChestID = read()
end

tableFile.save(config, "#easyVilIronFarm.lua")

local modem = peripheral.wrap(config.modemSide)
local ironFarms = { peripheral.find("easy_villagers:iron_farm") }
local ironChest = peripheral.wrap(config.ironChestID)
local redDyeChest = peripheral.wrap(config.redDyeChestID)

local function cycle()
	for _, ironFarm in pairs(ironFarms) do
		local ironFarmName = peripheral.getName(ironFarm)
		for slot, item in pairs(ironFarm.list()) do
			if item ~= nil then
				if item.name == "minecraft:iron_ingot" then
					ironChest.pullItems(ironFarmName, slot)
				elseif item.name == "minecraft:poppy" then
					ironFarm.pushItems(modem.getNameLocal(), slot, item.count, 1)
					turtle.craft()
					redDyeChest.pullItems(modem.getNameLocal(), slot)
				else
					error("Unexpected item in the iron farm area: " .. item.name)
				end
			end
		end
	end
end

while true do
	cycle()
	sleep(60 * 4)
end