local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")
local inv = wRequire("invClient")

--Assumes Super Smelter barrels, this computer, and the storage server are all on the same wired network

local config = tableFile.load("#makeCharcoalBetweenStorageNetworkAndSuperSmelter.lua", nil)

if config == nil then
	config = {}
	print("Modem side:")
	config.modemSide = read()
	print("storageComputerID:")
	config.storageComputerID = tonumber(read())
	print("Super Smelter Input Chest ID:")
	config.superSmelterInputChestID = read()
	print("Super Smelter Output Chest ID:")
	config.superSmelterOutputChestID = read()
	print("Cycle frequency (seconds):")
	config.cycleFrequency = tonumber(read())
	print("Log item ID (eg: minecraft:oak_log.0.):")
	config.logType = read()
end

tableFile.save(config, "#makeCharcoalBetweenStorageNetworkAndSuperSmelter.lua")

inv.storageComputerID = config.storageComputerID
rednet.open(config.modemSide)

local charcoalType = "minecraft:charcoal.0."

function Cycle()
	print("--------")
	local charcoalCount = inv.count(charcoalType)
	local logCount = inv.count(config.logType)
	print("Charcoal count: " .. tostring(charcoalCount))
	print("Log count: " .. tostring(logCount))
	
	-- Add logs to input chest
	if charcoalCount < 500 and logCount > 200 then
		for _ = 1, 8 do
			inv.getToAnySlot(config.logType, 64, config.superSmelterInputChestID)
		end
	end
	
	-- Take charcoal from output chest
	local outputChest = inv.getChest(config.superSmelterOutputChestID)
	
	for slot, item in pairs(outputChest.list()) do
		if item ~= nil and item.name == "minecraft:charcoal" then
			inv.store(config.superSmelterOutputChestID, slot, item.count)
		end
	end
end

while true do
	Cycle()
	longSleep(config.cycleFrequency)
end