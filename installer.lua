local args = {...}

--wget https://gitlab.com/billyg270/cc/-/raw/master/installer.lua

local remoteBase = "https://gitlab.com/billyg270/cc/-/raw/master/"

shell.execute("rm", "wRequire.lua")
shell.execute("wget", remoteBase .. "modules/wRequire.lua")

local programs = {
	["installer"] = remoteBase .. "installer.lua",
	["startup"] = remoteBase .. "startup.lua",
	["sugarCaneFarm"] = remoteBase .. "turtle/sugarCaneFarm.lua",
	["cropFarm"] = remoteBase .. "turtle/cropFarm.lua",
	["treeFarm"] = remoteBase .. "turtle/treeFarm.lua",
	["bambooFarm"] = remoteBase .. "turtle/bambooFarm.lua",
	["arStorageClient"] = remoteBase .. "storage/arClient.lua",
	["storageClient"] = remoteBase .. "storage/client.lua",
	["storageServer"] = remoteBase .. "storage/server.lua",
	["storageGetterClient"] = remoteBase .. "misc/get.lua",
	["superSmelter"] = remoteBase .. "misc/superSmelter.lua",
	["makeCharcoalBetweenStorageNetworkAndSuperSmelter"] = remoteBase .. "misc/makeCharcoalBetweenStorageNetworkAndSuperSmelter.lua",
	["witherKiller9002"] = remoteBase .. "misc/witherKiller9002.lua",
	["erwdes"] = remoteBase .. "misc/extremeReactorWithDraconicEnergyStorage.lua",
	["gpsHost"] = remoteBase .. "misc/gpsHost.lua",
	["turtleSetupDisk"] = remoteBase .. "misc/turtleSetupDisk.lua",
	["mineWithBlockScanner"] = remoteBase .. "turtle/mineWithBlockScanner.lua",
	["excavate"] = remoteBase .. "turtle/excavate.lua",
	["stripMine"] = remoteBase .. "turtle/stripMine.lua",
	["mineWithBlockScannerController"] = remoteBase .. "misc/mineWithBlockScannerController.lua",
	["vendingMachineAttachedToStorageNetwork"] = remoteBase .. "vendingMachine/vendingMachineAttachedToStorageNetwork.lua",
	["vendingMachineAttachedToStorageNetworkController"] = remoteBase .. "vendingMachine/vendingMachineAttachedToStorageNetworkController.lua",
	["xpShopFurnace"] = remoteBase .. "xpShop/furnace.lua",
	["xpShop"] = remoteBase .. "xpShop/shop.lua",
	["anvilDrop"] = remoteBase .. "turtle/anvilDrop.lua",
	["store"] = remoteBase .. "misc/store.lua",
	["elevator"] = remoteBase .. "misc/elevator.lua",
	["elytraAssist"] = remoteBase .. "pocket/elytraAssist.lua",
	["easyVilIronFarm"] = remoteBase .. "misc/easyVilIronFarm.lua",
	["railwayServer"] = remoteBase .. "railway/railwayServer.lua",
	["railwaySignal"] = remoteBase .. "railway/railwaySignal.lua",
	["railwayDispatch"] = remoteBase .. "railway/railwayDispatch.lua",
	["railwayMonitor"] = "https://gitlab.com/BeanFeed/cc-tweaked-programs/-/raw/main/TrainScreen/trainMonitor.lua",
	["battery"] = remoteBase .. "turtle/thermalBatteryFillEmpty.lua",
	["createRailwayOutpost"] = remoteBase .. "createRailway/outpost.lua",
	["createRailwayDepo"] = remoteBase .. "createRailway/depo.lua",
	["createPowerPlant"] = remoteBase .. "misc/createPowerPlant.lua",
	["redstonePingDiscord"] = remoteBase .. "misc/redstonePingDiscord.lua",
	["gpt4turtle"] = remoteBase .. "turtle/gpt4.lua",
	["dirtFarm"] = remoteBase .. "turtle/dirtFarm.lua",
	["feedingTroughWithEasyViligers"] = remoteBase .. "turtle/feedingTroughWithEasyViligers.lua",
}

local function run(program, arg)
	if program == nil or programs[program] == nil then
		print("Program name invaid")
		return
	end
	shell.execute("rm", program .. ".lua")
	shell.execute("wget", programs[program], program .. ".lua")

	if arg ~= nil then
		if string.lower(arg) == "s" then
			-- Startup
			shell.execute("wget", programs["startup"], "startup.lua")
			shell.execute("startup " .. program)
		elseif string.lower(arg) ~= "dr" then-- arg for don't run
			shell.execute(program .. ".lua")
		end
	else
		shell.execute(program .. ".lua")
	end
end

run(args[1], args[2])

--hello lol
