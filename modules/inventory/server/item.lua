local wRequire = require("./wRequire")
local util = wRequire("util")

local module = {}

module.getItemNameAndNbt = function(itemDetail)
	if itemDetail == nil then
		return nil
	end
	
	local damage = "0"
	local nbt = ""
	
	if itemDetail.damage ~= nil then
		damage = tostring(itemDetail.damage)
	end
	if itemDetail.nbt ~= nil then
		nbt = itemDetail.nbt
	end
	
	return itemDetail.name .. "." .. damage .. "." .. nbt
end

module.getItemNameAndNbtFromString = function(itemNameAndNbt)
	local split = util.splitString(itemNameAndNbt, ".")
	
	local damage = 0
	local nbt = nil
	
	if split[2] ~= nil and split[2] ~= "" then
		damage = tonumber(split[2])
	end
	
	if split[3] ~= nil and split[3] ~= "" then
		nbt = split[3]
	end
	
	return {
		["name"] = split[1],
		["damage"] = damage,
		["nbt"] = nbt,
	}
end

module.getItemDetail = function(chest, slot)
	if chest.getItemDetail ~= nil then
		return chest.getItemDetail(slot)
	end
	if chest.getItemMeta ~= nil then
		local meta = chest.getItemMeta(slot)
		if meta == nil then
			return nil
		end
		return {
			["name"] = meta.name,
			["count"] = meta.count,
			["maxCount"] = meta.maxCount,
			["damage"] = meta.damage,
			["displayName"] = meta.displayName,
			["nbt"] = meta.nbtHash
		}
	end
end

module.getItemCount = function(chest, slot)
	local s = module.getItemDetail(chest, slot)
	if s == nil then
		return 0
	end
	return s.count
end

return module