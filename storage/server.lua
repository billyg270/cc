local wRequire = require("wRequire")
local util = wRequire("util")
--local crafting = wRequire("storageServerCrafting")
--local machine = wRequire("storageServerMachine")
local tableFile = wRequire("tableFile")

local config = tableFile.load("#invServer.lua", nil)

if config == nil then
	config = {}
	print("Modem name:")
	print("eg: bottom")
	config.modemName = read()
	
	print("Modem 2 name:")
	print("eg: modem_0")
	config.modem2Name = read()
	
	print("Storage mode:")
	print("eg: chests")
	print("eg: ae2pp1")
	config.invMode = read()
end

tableFile.save(config, "#invServer.lua")

local inv = nil
if config.invMode == "chests" then
	inv = wRequire("storageServerInv")
elseif config.invMode == "ae2pp1" then
	inv = wRequire("storageServerAe2InvPp1")
else
	error("Unknown storage mode: " .. config.invMode)
end


--- Queue ---

local Queue = {}

function Network()
	while true do
		local id, msg = rednet.receive("inv")
		msg["fromID"] = id
		table.insert(Queue, msg)
		sleep(0)
	end
end

function ProcessQueue()
	function ProcessItem(msg)
		--print(textutils.serialise(msg))
		if msg["action"] == "store" then
			if msg["chest"] == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Missing 'chest'"
				}, "invResp")
				return
			end
			if peripheral.wrap(msg["chest"]) == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Cant find 'chest'"
				}, "invResp")
				return
			end
			if msg["slot"] == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Missing 'slot'"
				}, "invResp")
				return
			end
			local moved = 0
			if msg["count"] == nil then
				moved = inv.store(msg["chest"], msg["slot"])
			else
				moved = inv.store(msg["chest"], msg["slot"], msg["count"])
			end
			rednet.send(msg["fromID"], {
				["instructionRef"] = msg["instructionRef"],
				["status"] = "success",
				["moved"] = moved
			}, "invResp")
			return
		end
		if msg["action"] == "get" then
			if msg["nbtAndName"] == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Missing 'nbtAndName'"
				}, "invResp")
				return
			end
			if msg["chest"] == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Missing 'chest'"
				}, "invResp")
				return
			end
			if peripheral.wrap(msg["chest"]) == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Cant find 'chest'"
				}, "invResp")
				return
			end
			if msg["count"] == nil then
				msg["count"] = 1
			end
			local moved = 0
			if msg["slot"] == nil then
				moved = inv.get(msg["nbtAndName"], msg["count"], msg["chest"])
			else
				moved = inv.get(msg["nbtAndName"], msg["count"], msg["chest"], msg["slot"])
			end
			rednet.send(msg["fromID"], {
				["instructionRef"] = msg["instructionRef"],
				["status"] = "success",
				["moved"] = moved
			}, "invResp")
			return
		end
		if msg["action"] == "count" then
			if msg["nbtAndName"] == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Missing 'nbtAndName'"
				}, "invResp")
				return
			end
			local count = inv.inventory["itemCounts"][msg["nbtAndName"]]
			if count == nil then
				count = 0
			end
			rednet.send(msg["fromID"], {
				["instructionRef"] = msg["instructionRef"],
				["status"] = "success",
				["count"] = count
			}, "invResp")
			return
		end
		if msg["action"] == "getCounts" then
			rednet.send(msg["fromID"], {
				["instructionRef"] = msg["instructionRef"],
				["status"] = "success",
				["counts"] = inv.inventory["itemCounts"]
			}, "invResp")
			return
		end
		if msg["action"] == "getName" then
			if msg["nbtAndName"] == nil then
				rednet.send(msg["fromID"], {
					["instructionRef"] = msg["instructionRef"],
					["status"] = "fail",
					["message"] = "Missing 'nbtAndName'"
				}, "invResp")
				return
			end
			rednet.send(msg["fromID"], {
				["instructionRef"] = msg["instructionRef"],
				["status"] = "success",
				["name"] = inv.getItemName(nil, nil, msg["nbtAndName"])
			}, "invResp")
			return
		end
		if msg["action"] == "getNames" then
			rednet.send(msg["fromID"], {
				["instructionRef"] = msg["instructionRef"],
				["status"] = "success",
				["names"] = inv.inventory["itemNames"]
			}, "invResp")
			print("Sent to " .. tostring(msg["fromID"]))
			return
		end
	end
	while true do
		if util.tableSize(Queue) > 0 then
			ProcessItem(table.remove(Queue))
			sleep(0)
		else
			sleep(0.1)
		end
	end
end


--- Run ---

inv.mapInventory()

rednet.open(config.modemName)
if config.modem2Name ~= nil and config.modem2Name ~= "" then
	rednet.open(config.modem2Name)
end

parallel.waitForAny(Network, ProcessQueue)

Save()