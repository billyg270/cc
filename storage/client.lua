local wRequire = require("wRequire")
local invClient = wRequire("invClient")
local tableFile = wRequire("tableFile")
local util = wRequire("util")

--local ioChestName = "minecraft:barrel_0"
--local clientName  = "clientLocal1"
--local modemName   = "bottom"

local items = {
    ["all"] = {
        ["minecraft:dirt"] = "Dirt",
        ["minecraft:stone"] = "Stone",
        ["minecraft:cobblestone"] = "Cobblestone",
    },
    ["recents"] = {
    },
    ["favorites"] = {
        "minecraft:cobblestone"
    }
}

local results = {}
local query = ""
local selectedIndex = 1
local screen = "search"--"favorites"

function Save()
    tableFile.save(items, "#items.lua")
end

function Load()
    items = tableFile.load("#items.lua", items)
end

Load()

if items.ioChestNameLocal == nil then
    print("IO chest name local:")
    items.ioChestNameLocal = read()
    Save()
end

if items.ioChestNameRemote == nil then
    print("IO chest name remote:")
    items.ioChestNameRemote = read()
    Save()
end

if items.clientName == nil then
    print("Client name:")
    items.clientName = read()
    Save()
end

if items.modemName == nil then
    print("Modem name:")
    items.modemName = read()
    Save()
end

if items.storageComputerID == nil then
    print("storageComputerID:")
    items.storageComputerID = tonumber(read())
    Save()
end

if items.keys == nil then
    items.keys = {
        ["up"] = 0,
        ["down"] = 0,
        ["left"] = 0,
        ["right"] = 0,
        ["backspace"] = 0,
        ["get"] = 0,
        ["store"] = 0,
        ["count"] = 0,
        ["clearSearch"] = 0,
        ["refresh"] = 0
    }
    print("=== Key mapping ===")
    for kn, kv in pairs(items.keys) do
        print("press " .. kn)
        local event, key, is_held = os.pullEvent("key")
        items.keys[kn] = key
        print(kn .. " is now bound to " .. tostring(items.keys[kn]))
    end
    print("=== End of key mapping ===")
    Save()
end

invClient.storageComputerID = items.storageComputerID
invClient.clientName = items.clientName

local ioChestLocal = peripheral.wrap(items.ioChestNameLocal)

rednet.open(items.modemName)

function UpdateSearchResults()
    results = {}
    for k, v in pairs(items.all) do
        if string.find(string.lower(v), string.lower(query)) ~= nil then
            table.insert(results, k)
        end
    end
    if selectedIndex > util.tableSize(results) then
        selectedIndex = util.tableSize(results)
    end
    SortResults()
end

function SortResults()
    local results2 = {}

    while util.tableSize(results) > 0 do
        local shortestKey = nil
        local shortest = nil
        for k, v in pairs(results) do
            if shortest == nil or string.len(v) < string.len(shortest) then
                shortest = v
                shortestKey = k
            end
        end
        if shortest == nil then
            break
        end
        table.insert(results2, shortest)
        results[shortestKey] = nil
    end

    results = results2
end

function RenderResults()
    local width, height = term.getSize()
    term.clear()
    term.setCursorPos(1, 1)
    term.setTextColour(colors.orange)
    if screen == "search" then
        term.write("search: " .. query .. "_")
    else
        term.write(screen)
    end
    local resCount = util.tableSize(results)
    term.setCursorPos(width - string.len(tostring(resCount)) + 1, 1)
    term.write(resCount)
    local line = 1
    for k, v in pairs(results) do
        line = line + 1
        term.setCursorPos(1, line)
        if selectedIndex + 1 == line then
            term.setTextColour(colors.lightBlue)
        else
            term.setTextColour(colors.blue)
        end
        term.write(items.all[v])
    end
end

function MenuKeys()
    while true do
        ---@diagnostic disable-next-line: undefined-field
        local event, key, is_held = os.pullEvent("key")
        local resCount = util.tableSize(results)
        if key == items.keys.up then
            selectedIndex = selectedIndex - 1
            if selectedIndex < 1 then
                selectedIndex = 1
            end
            RenderResults()
        elseif key == items.keys.down then
            selectedIndex = selectedIndex + 1
            if selectedIndex > resCount then
                selectedIndex = resCount
            end
            if selectedIndex < 1 then
                selectedIndex = 1
            end
            RenderResults()
        elseif key == items.keys.left then
            if screen == "favorites" then
                screen = "search"
                UpdateSearchResults()
            end
            selectedIndex = 1
            RenderResults()
        elseif key == items.keys.right then
            if screen == "search" then
                screen = "favorites"
                results = items.favorites
            end
            selectedIndex = 1
            RenderResults()
        elseif key == items.keys.clearSearch then
            if screen == "search" then
                query = ""
                UpdateSearchResults()
                RenderResults()
            end
        elseif key == items.keys.backspace then
            if screen == "search" then
                query = query:sub(1, -2)
                UpdateSearchResults()
                RenderResults()
            end
        elseif key == items.keys.count then
            if results[selectedIndex] ~= nil then
                term.clear()
                term.setCursorPos(1, 1)
                term.setTextColour(colors.green)
                local count = invClient.count(results[selectedIndex])
                term.clear()
                term.setCursorPos(1, 1)
                print("Count: " .. tostring(count))
                term.setTextColour(colors.blue)
                print("")
                print("Press enter to close")
                read()
                RenderResults()
            end
        elseif key == items.keys.get then
            if results[selectedIndex] ~= nil then
                term.clear()
                term.setCursorPos(1, 1)
                term.setTextColour(colors.green)
                while invClient.getToAnySlot(results[selectedIndex], 64, items.ioChestNameRemote, items.ioChestNameLocal) == false do
                    invClient.storeAllFromIoChest(items.ioChestNameRemote, items.ioChestNameLocal)
                end
                RenderResults()
            end
        elseif key == items.keys.store then
            term.clear()
            term.setCursorPos(1, 1)
            term.setTextColour(colors.green)
            invClient.storeAllFromIoChest(items.ioChestNameRemote, items.ioChestNameLocal)
            RenderResults()
        elseif key == items.keys.refresh then
            items["all"] = invClient.getItemNamesFromServer()
            UpdateSearchResults()
            RenderResults()
        end
    end
end

function QueryInput()
    while true do
        ---@diagnostic disable-next-line: undefined-field
        local event, character = os.pullEvent("char")
        if screen == "search" then
            query = query .. character
            UpdateSearchResults()
            RenderResults()
        end
    end
end

items["all"] = invClient.getItemNamesFromServer()

UpdateSearchResults()
RenderResults()

parallel.waitForAny(MenuKeys, QueryInput)