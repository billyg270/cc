local args = {...}

--[[
	Will suck batteries from below, place them infront to charge / empty, then drop them up.
]]

if args[1] == "f" then
	args[1] = "fill"
end

if args[1] == "e" then
	args[1] = "empty"
end

if args[1] ~= "fill" and args[1] ~= "empty" then
	error("Argument should be fill (f) or empty (e)")
end

if args[2] == "startup" then
	-- Create startup file, using arg 1 also
	local file = fs.open("startup.lua", "w")
	file.write("shell.execute(\"battery\", \"" .. args[1] .. "\")")
	file.close()
	print("Set startup file")
end

local function waitForBatteryToFill()
    local bat = peripheral.wrap("front")
    while true do
		if args[1] == "fill" then
			if bat.getEnergy() >= bat.getEnergyCapacity() then
				print("Battery filled")
				return
			end
			print("Waiting for battery to fill")
		else
			if bat.getEnergy() <= 0 then
				print("Battery emptied")
				return
			end
			print("Waiting for battery to empty")
		end
        sleep(1)
    end
end

local function dropUp()
	while turtle.getItemCount() > 0 do
		turtle.dropUp()
		sleep(1)
	end
end

if turtle.detect() then
    waitForBatteryToFill()
    turtle.dig()
end

dropUp()

while true do
    if turtle.suckDown(1) then
        print("Got battery")
        turtle.place()
        sleep(1)
        waitForBatteryToFill()
        turtle.dig()
        dropUp()
    end
    sleep(1)
end