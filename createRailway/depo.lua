local wRequire = require("wRequire")

local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load("#createRailwayDepo.lua", nil)

print("-------")
print("All stations should have unique names except depos which can share the same name")
print("All trains using this station should be in the following format: \"auto-ITEM-1\" where ITEM is either ITEM / FLUID / ENERGY and the number is unique to each train")
print("-------")

local startupFile = fs.open("startup.lua", "w")
startupFile.write("shell.run(\"createRailwayDepo.lua\")")
startupFile.close()

if config == nil then
	config = {}

    print("Modem Side:")
	config.modemSide = read()
	
	print("Station Side:")
	config.stationSide = read()
end

tableFile.save(config, "#createRailwayDepo.lua")

local station = peripheral.wrap(config.stationSide)
rednet.open(config.modemSide)

rednet.unhost("createRailwayDepo-ITEM")
rednet.unhost("createRailwayDepo-FLUID")
rednet.unhost("createRailwayDepo-ENERGY")

function setSchedule(name, pickupStation, dropoffStation)
    print("--- Setting schedule ---")
    print("Name:" .. name)
    print("Pickup:" .. pickupStation)
    print("Dropoff:" .. dropoffStation)
    print("------------------------")

    station.setSchedule({
        cyclic = false,
        entries = {
            [0] = {
                instruction = {
                    data = {
                        text = name,
                    },
                    id = "create:rename",
                },
            },
            [1] = {
                instruction = {
                    data = {
                        text = pickupStation,
                    },
                    id = "create:destination",
                },
                conditions = {
                    {
                        {
                            data = {},
                            id = "create:powered",
                        },
                        {
                            data = {
                                value = 2,
                                time_unit = 1,
                            },
                            id = "create:idle",
                        },
                    },

                },
            },
            [2] = {
                instruction = {
                    data = {
                        text = dropoffStation,
                    },
                    id = "create:destination",
                },
                conditions = {
                    {
                        {
                            data = {},
                            id = "create:powered",
                        },
                        {
                            data = {
                                value = 2,
                                time_unit = 1,
                            },
                            id = "create:idle",
                        },
                        {
                            data = {
                                item = {
                                    count = 1,
                                    id = "minecraft:air",
                                },
                                threshold = 0,
                                operator = 2,
                                measure = 0
                            },
                            id = "create:item_threshold",
                        },
                        {
                            data = {
                                bucket = {
                                    count = 1,
                                    id = "minecraft:air",
                                },
                                threshold = 0,
                                operator = 2,
                                measure = 0
                            },
                            id = "create:fluid_threshold",
                        },
                        {
                            data = {
                                threshold = "0",
                                operator = 2,
                                measure = 0
                            },
                            id = "createaddition:energy_threshold",
                        },
                    },
                },
            },
            [3] = {
                instruction = {
                    data = {
                        text = station.getStationName(),
                    },
                    id = "create:destination",
                },
                conditions = {
                    {
                        {
                            data = {
                                value = 10,
                                time_unit = 1,
                            },
                            id = "create:delay",
                        },
                    },
                },
            },
        },
    })
end

while true do
    if station.isTrainPresent() then
        local trainName = station.getTrainName()
        local trainNameParts = util.splitString(trainName, "-")--eg: "auto-ITEM-1"
        if trainNameParts[1] == "auto" then
            print("AUTO TRAIN: " .. trainName)
            local nameToHost = "createRailwayDepo-" .. trainNameParts[2]
            print("Hosting: " .. nameToHost)
            rednet.host(nameToHost, nameToHost .. "-" .. os.getComputerID())

            local dropoffStationId, dropoffStationMsg = rednet.receive(nameToHost)
            print("Train has been called for [" .. dropoffStationMsg.cargoType .. "-" .. dropoffStationMsg.cargoName .. "] to PU: " .. dropoffStationMsg.pickup .. " DO: " .. dropoffStationMsg.dropoff)

            if dropoffStationId then
                setSchedule("auto-" .. dropoffStationMsg.cargoType .. "-" .. dropoffStationMsg.cargoName, dropoffStationMsg.pickup, dropoffStationMsg.dropoff)
                rednet.unhost(nameToHost)
                print("Un-hosting: " .. nameToHost)
                sleep(0.1)
                rednet.send(dropoffStationId, "OK", nameToHost .. "-resp-" .. os.getComputerID())
            else
                rednet.unhost(nameToHost)
                print("Un-hosting: " .. nameToHost)
            end
        else
            print("Train is present but is not auto train")
            print(trainName)
        end
    else
        print("Waiting for train")
        sleep(2)
    end
    sleep(0.1)
end