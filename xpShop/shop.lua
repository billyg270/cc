--local basalt = require("basalt")--pastebin run ESs1mg7P packed
local wRequire = require("./wRequire")
local tableFile = wRequire("tableFile")

local config = tableFile.load("#xpShop.lua", {})

if config.monitor == nil then
	print("Monitor:")
	config.monitor = read()
end
if config.storageIOlocally == nil then
	--print("storage IO chest locally (perhaps an ender chest):")
	--config.storageIOlocally = read()
end
if config.storageIOremotely == nil then
	--print("storage IO chest remotely (perhaps an ender chest):")
	--config.storageIOremotely = read()
end
if config.storageServerID == nil then
	print("Storage server ID:")
	config.storageServerID = tonumber(read())
end
if config.userChest == nil then
	print("User Chest (that the user can see):")
	config.userChest = read()
end
if config.holdingChest == nil then
	print("Holding Chest (regular chest in the back):")
	config.holdingChest = read()
end
if config.modemName == nil then
	print("Modem name:")
	config.modemName = read()
end
if config.speakerName == nil then
	print("Speaker name:")
	config.speakerName = read()
end
if config.paymentName == nil then
	print("Payment Name (eg: minecraft:diamond):")
	config.paymentName = read()
end
if config.paymentCount == nil then
	print("Payment Count:")
	config.paymentCount = tonumber(read())
end
if config.xpAmount == nil then
	print("XP Amount (eg: 1395):")
	config.xpAmount = tonumber(read())
end

tableFile.save(config, "#xpShop.lua")

local userChest = peripheral.wrap(config.userChest)
local holdingChest = peripheral.wrap(config.holdingChest)
local speaker = peripheral.wrap(config.speakerName)
local monitor = peripheral.wrap(config.monitor)

rednet.open(config.modemName)

local function moveAllItems(from, to, itemType, max)
	local left = max
	for slot, item in pairs(from.list()) do
		if left > 0 and item ~= nil and (itemType == nil or item.name == itemType) then
			left = left - from.pushItems(peripheral.getName(to), slot, math.min(left, 64))
		end
	end
end

local function countInChest(chest, itemType)
	local total = 0
	
	for slot, item in pairs(chest.list()) do
		if item ~= nil and (itemType == nil or item.name == itemType) then
			total = total + item.count
		end
	end
	
	return total
end

local width, height = monitor.getSize()

local mostXpAval = 0


local function networkLoop()
	rednet.broadcast(0, "bbXpShop-s2r")
	while true do
		local id, message = rednet.receive("bbXpShop-r2s", 10)
		if message ~= nil then
			mostXpAval = 0
			for _, furnace in pairs(message) do
				if furnace.xp > mostXpAval then
					mostXpAval = furnace.xp
				end
			end
		end
	end
end

local function mainLoop()
	while true do
		sleep(1)
		
		monitor.setBackgroundColour(colors.cyan)
		monitor.setTextColour(colors.lime)
		monitor.clear()
		
		monitor.setCursorPos(1, 1)
		monitor.write("Bill's XP Shop")
		
		monitor.setTextColour(colors.white)
		
		if mostXpAval >= config.xpAmount then
			monitor.setCursorPos(1, 3)
			monitor.write("Please insert payment")
			
			monitor.setCursorPos(1, 4)
			monitor.write("10 diamonds for 30 levels")
			
			monitor.setCursorPos(1, 5)
			monitor.write("(" .. tostring(config.xpAmount) .. " XP)")
			
			if countInChest(userChest, config.paymentName) >= config.paymentCount then
				moveAllItems(userChest, holdingChest, config.paymentName, config.paymentCount)
				rednet.broadcast(config.xpAmount, "bbXpShop-s2r")
			end
		else
			monitor.setCursorPos(1, 3)
			monitor.write("Not enough XP in stock")
			
			monitor.setCursorPos(1, 4)
			monitor.write(tostring(mostXpAval) .. " / " .. tostring(config.xpAmount) .. " XP")
			
			monitor.setCursorPos(1, 6)
			monitor.write("Please check back later")
		end
	end
end

parallel.waitForAny(mainLoop, networkLoop)