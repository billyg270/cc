local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")
local farmModule = wRequire("farm")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")

local config = tableFile.load("#cropFarm.lua", nil)

if config == nil then
	config = {}
	print("Width:")
	config.width = tonumber(read())
	
	print("Depth:")
	config.depth = tonumber(read())
	
	print("Crop:")
	config.crop = read()
	
	print("Seed:")
	config.seed = read()
	
	print("Require rear redstone signal (y/n):")
	config.requireRearRedstoneSignal = string.lower(read()) == "y"
end

tableFile.save(config, "#cropFarm.lua")

local function checkFuel()
	local minFuel = config.width * config.depth * 2
	while turtle.getFuelLevel() < minFuel do
		print("Refueling")
		turtle.turnRight()
		turtle.select(16)
		turtle.suck(16)
		turtle.refuel(64)
		turtle.turnLeft()
		print("Fuel level: " .. turtle.getFuelLevel() .. "/" .. minFuel)
	end
end

local function step()
	turtle.forward()
	local hasBelow, below = turtle.inspectDown()
	if hasBelow and below.name == config.crop and (below.state.age == 7 or config.crop == "minecraft:pumpkin" or config.crop == "minecraft:melon") then
		turtle.select(1)
		turtle.digDown()
	end
	if config.seed ~= nil and config.seed ~= "" and bbTurtle.select(config.seed) ~= nil then
		turtle.placeDown()
	end
end

local function farm()
	print("Farming")
	turtle.select(1)
	turtle.suckUp(64)
	farmModule.farm(config.width, config.depth, step)
end

local function dumpItems()
	print("Dumping items")
	while bbTurtle.select(config.crop) ~= nil do
		turtle.dropDown(64)
	end
	while bbTurtle.select(config.seed) ~= nil do
		turtle.dropUp(64)
	end
end

local function cycle()
	checkFuel()
	farm()
	dumpItems()
end

while true do
	if config.requireRearRedstoneSignal then
		while redstone.getInput("back") == false do
			print("Waiting for rear redstone input")
			sleep(1)
		end
	end
	cycle()
	longSleep(60 * 20)
end