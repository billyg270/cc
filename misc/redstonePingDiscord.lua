local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local tableFile = wRequire("tableFile")

local config = tableFile.load("#redstonePingDiscord.lua", nil)

if config == nil then
    config = {}

    print("Redstone side:")
    config.redstoneSide = read()

    print("Webhook URL:")
    config.webhookUrl = read()

    print("Title:")
    config.title = read()

    print("Message:")
    config.message = read()

    tableFile.save(config, "#redstonePingDiscord.lua")
end

local hookSuccess, hook = DiscordHook.createWebhook(config.webhookUrl)
if not hookSuccess then
	print("Webhook connection failed! Reason: " .. hook)
    exit()
end

local redstoneLast = false
while true do
    local rs = redstone.getInput(config.redstoneSide)

    if rs and redstoneLast == false then
        hook.send(config.message, config.title)
    end

    redstoneLast = rs

    sleep(0.1)
end