local wRequire = require("wRequire")
local util = wRequire("util")
local tableFile = wRequire("tableFile")
local itemF = wRequire("storageServerItem")

local module = {}

module.inventory = {}

--- Inventory file ---

module.save = function()
	tableFile.save(module.inventory, "#inventory.lua")
end

module.load = function()
	module.inventory = tableFile.load("#inventory.lua", module.inventory)
	if module.inventory.inChestName == nil then
		print("In chest name: ")
		print("eg: minecraft:chest_0")
		module.inventory.inChestName = read()
	end
	if module.inventory.outChestName == nil then
		print("Out chest name: ")
		print("eg: minecraft:chest_1")
		module.inventory.outChestName = read()
	end
	if module.inventory.outChestMeSide == nil then
		print("Out chest Me side: ")
		print("eg: west")
		module.inventory.outChestMeSide = read()
	end
end

module.load()

module.meBridge = peripheral.find("meBridge")
module.inChest = peripheral.wrap(module.inventory.inChestName)
module.outChest = peripheral.wrap(module.inventory.outChestName)

while itemF.getItemCount(module.outChest, 1) > 0 do
	module.outChest.pushItems(module.inventory.inChestName, 1, 1, 1)
end

--- Inventory mapping ---

module.mapInventory = function()
	module.inventory["itemCounts"] = {}
	if module.inventory["itemNames"] == nil then
		module.inventory["itemNames"] = {}
	end
	
	util.log("Mapping inventory")
	local items = module.meBridge.listItems()
	
	for _, item in pairs(items) do
		
		local itemNbtAndName = itemF.getItemNameAndNbt({
			["name"] = item.name,
			["damage"] = item.meta,
			["nbt"] = item.nbt
		})
		
		module._setItem(
			itemNbtAndName,
			item.amount,
			nil--item.displayName--gives wrong display name from me bridge
		)
		
		if module.getItemName(itemNbtAndName) == nil then
			if item.nbt == nil or item.nbt == "" then
				module.meBridge.retrieve(item.name, item.meta, 1, module.inventory.outChestMeSide)
			else
				module.meBridge.retrieve(item.name, item.meta, 1, module.inventory.outChestMeSide, item.nbt)
			end
			
			module.inventory["itemNames"][itemNbtAndName] = itemF.getItemDetail(module.outChest, 1).displayName
			
			while itemF.getItemCount(module.outChest, 1) > 0 do
				module.outChest.pushItems(module.inventory.inChestName, 1, 1, 1)
			end
		end
	end
	
	module.save()
	util.log("Inventory mapped")
end

module._setItem = function(itemNbtAndName, count, displayName)
	if itemNbtAndName ~= "" then
		module.inventory["itemCounts"][itemNbtAndName] = count
		if displayName ~= nil then
			module.inventory["itemNames"][itemNbtAndName] = displayName
		end
	end
end

module.getItemName = function(itemnbtAndName)
	return module.inventory["itemNames"][itemnbtAndName]
end

--- IO ---

-- Take from outChest and store
module.store = function(fromChest, fromSlot, toMove)
	local item = itemF.getItemDetail(peripheral.wrap(fromChest), fromSlot)
	if item ~= nil then
		local itemNbtAndName = itemF.getItemNameAndNbt(item)
		if module.inventory["itemNames"][itemNbtAndName] == nil then
			module.inventory["itemNames"][itemNbtAndName] = item.displayName
		end
		util.log("Storing " .. tostring(toMove) .. " " .. itemF.getItemNameAndNbt(item))
		local moved = 0
		while moved < toMove do
			moved = moved + module.inChest.pullItems(fromChest, fromSlot, math.min(toMove - moved, 64))
			if itemF.getItemCount(peripheral.wrap(fromChest), fromSlot) == 0 then
				break
			end
		end
		util.log("Stored " .. tostring(moved) .. " " .. itemF.getItemNameAndNbt(item))
		--module._setItem(itemF.getItemNameAndNbt(item), )
		module.mapInventory()
		return moved
	end
	util.log("Was asked to store slot but is empty")
	return 0
end

-- Get from storage to outChest by name
module.get = function(itemNbtAndName, count, toChest, toSlot)
	util.log("Getting " .. tostring(count) .. " " .. itemNbtAndName)
	
	while itemF.getItemCount(module.outChest, 1) > 0 do
		module.outChest.pushItems(module.inventory.inChestName, 1, 1, 1)
	end
	
	if itemF.getItemCount(module.outChest, 1) ~= 0 then
		error("There is something in the out slot of the Me bridge chest")
	end
	
	local item = itemF.getItemNameAndNbtFromString(itemNbtAndName)
	local movedA = 0
	if item.nbt == nil or item.nbt == "" then
		movedA = module.meBridge.retrieve(item.name, item.damage, count, module.inventory.outChestMeSide)
	else
		movedA = module.meBridge.retrieve(item.name, item.damage, count, module.inventory.outChestMeSide, item.nbt)
	end
	
	local movedB = module.outChest.pushItems(toChest, 1, movedA, toSlot)
	
	while itemF.getItemCount(module.outChest, 1) > 0 do
		module.outChest.pushItems(module.inventory.inChestName, 1, 1, 1)
	end
	
	util.log("Got " .. tostring(movedB) .. " " .. itemNbtAndName)
	module.mapInventory()
	return movedB
end

return module