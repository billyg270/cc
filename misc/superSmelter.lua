local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")

local config = tableFile.load("#superSmelter.lua", nil)

if config == nil then
	config = {}
	
	print("In Chest:")
	config.inChest = read()
	
	print("Out Chest:")
	config.outChest2 = read()
	
	print("Tmp Chest (because of CC bug deleting items):")
	config.outChest = read()
	
	print("Fuel Chest:")
	config.fuelChest = read()
	
	print("Furnace:")
	config.furnace = read()
	
	print("Furnace In Slot:")
	config.furnaceInSlot = tonumber(read())
	
	print("Furnace Out Slot:")
	config.furnaceOutSlot = tonumber(read())
	
	print("Furnace Fuel Slot:")
	config.furnaceFuelSlot = tonumber(read())
	
	config.furnaceFuelsToRecycle = {
		["minecraft:coal"] = true,
		["minecraft:charcoal"] = true
	}
	
	tableFile.save(config, "#superSmelter.lua")
end

local function wrapChests(name)
	if tonumber(string.sub(name, -1)) == nil then
		return { peripheral.find(name) }
	end
	return { peripheral.wrap(name) }
end

local inChests = wrapChests(config.inChest)
local outChests = wrapChests(config.outChest)
local outChests2 = wrapChests(config.outChest2)
local fuelChests = wrapChests(config.fuelChest)
local furnaces = wrapChests(config.furnace)

local function getFromChests(chestsTable, toName, toSlot, amt)
	local moved = 0
	for _, chest in pairs(chestsTable) do
		local chestItems = chest.list()
		for slot = 1, chest.size() do
			if chestItems[slot] ~= nil then
				moved = moved + chest.pushItems(toName, slot, amt - moved, toSlot)
				if moved >= amt then
					return 0
				end
			end
		end
	end
	return amt - moved
end

local function getIntoChests(chestsTable, fromName, fromSlot, amt)
	local moved = 0
	for _, chest in pairs(chestsTable) do
		for slot = 1, chest.size() do
			moved = moved + chest.pullItems(fromName, fromSlot, amt - moved, slot)
			if moved >= amt then
				return 0
			end
		end
	end
	return amt - moved
end

local function cycle()
	for _, furnace in pairs(furnaces) do
		local furnaceName = peripheral.getName(furnace)
		if string.find(furnaceName, config.furnace) then
			local furnaceItems = furnace.list()
			print(textutils.serialise(furnaceItems))
			
			if furnaceItems[config.furnaceInSlot] == nil or furnaceItems[config.furnaceInSlot].count < 8 then
				print("Getting items to smelt")
				getFromChests(inChests, furnaceName, config.furnaceInSlot, 8)
			end
			
			if config.furnaceFuelSlot ~= -1 then
				if furnaceItems[config.furnaceFuelSlot] == nil or furnaceItems[config.furnaceFuelSlot].count < 8 then
					print("Getting fuel")
					getFromChests(fuelChests, furnaceName, config.furnaceFuelSlot, 8)
				end
			end
			
			if furnaceItems[config.furnaceOutSlot] ~= nil then
				getIntoChests(outChests2, config.outChest, 1, 64)
				
				if config.furnaceFuelsToRecycle[furnaceItems[config.furnaceOutSlot].name] then
					print("Using output as fuel")
					getIntoChests(outChests, furnaceName, config.furnaceOutSlot, furnaceItems[config.furnaceOutSlot].count)
					if getIntoChests(fuelChests, config.outChest, 1, furnaceItems[config.furnaceOutSlot].count) > 0 then
						print("Putting excess fuel into output")
						getIntoChests(outChests,  config.outChest, 1, furnaceItems[config.furnaceOutSlot].count)
					end
				else
					print("Getting furnace output")
					getIntoChests(outChests,  furnaceName, config.furnaceOutSlot, furnaceItems[config.furnaceOutSlot].count)
					getIntoChests(outChests2, config.outChest, 1, 64)
				end
			end
		end
	end
end

while true do
	cycle()
	sleep(1)
end
