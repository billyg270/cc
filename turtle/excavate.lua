-- Fuel goes in chest on the left
-- output is chests it places as it goes in slot 16

local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")
local farmModule = wRequire("farm")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")

local config = nil

if config == nil then
	config = {}
	print("Width:")
	config.width = tonumber(read())
	
	print("Depth Back:")
	config.depth = tonumber(read())
	
	print("Depth Down:")
	config.depthDown = tonumber(read())
	
	print("Case about storing items in chests (y/n)")
	config.careAcoutChests = read() == "y"
end

local function checkFuel()
	local minFuel = config.width * config.depth * 2 + config.depthDown * 2
	while turtle.getFuelLevel() < minFuel do
		print("Refueling")
		turtle.turnLeft()
		turtle.select(15)
		turtle.suck(16)
		turtle.refuel(64)
		turtle.turnRight()
		print("Fuel level: " .. turtle.getFuelLevel() .. "/" .. minFuel)
	end
end

local function step()
	bbTurtle.rForward()
	bbTurtle.rDig("u")
	bbTurtle.rDig("d")
	if turtle.getItemCount(14) > 0 then
		while turtle.getItemCount(16) == 0 and config.careAcoutChests do
			print("Waiting for chest in slot 16")
			sleep(4)
		end
		if config.careAcoutChests then
			turtle.select(16)
			turtle.placeUp()
		end
		for i = 1, 15 do
			turtle.select(i)
			turtle.dropUp(64)
		end
	end
end

local function farm()
	print("Mining")
	farmModule.farm(config.width, config.depth, step)
end

local function dumpItems()
	print("Dumping items")
	turtle.turnLeft()
	turtle.turnLeft()
	while bbTurtle.select("minecraft:coal") ~= nil and turtle.getFuelLevel() < turtle.getFuelLimit() do
		turtle.refuel(64)
	end
	for i = 1, 15 do
		turtle.select(i)
		while turtle.getItemCount(i) > 0 do
			turtle.drop(64)
		end
	end
	turtle.turnLeft()
	turtle.turnLeft()
end

local function cycle(down)
	checkFuel()
	for i = 2, down do
		bbTurtle.rDown()
	end
	farm()
	for i = 2, down do
		bbTurtle.rUp()
	end
	dumpItems()
end

for i = 0, math.floor(config.depthDown / 3) * 3, 3 do
	cycle(i)
end