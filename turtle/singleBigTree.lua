local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")
local tableFile = wRequire("tableFile")

local hookSuccess, hook = DiscordHook.createWebhook("https://discord.com/api/webhooks/889783581839339541/mtTZmSBCoa-KGi2DN5S5_tQMPJhpPjOjDB3dy8EtY-Dxu2CK9pTeBwMy7IkKMGyVpkti")
if not hookSuccess then
	error("Webhook connection failed! Reason: " .. hook)
end

local discordWebhookName = "Single big tree robot"

print("v1.5")

local function checkFuel()
	turtle.select(1)
	local fuelTaken = 0
	while turtle.getFuelLevel() < 500 do
		turtle.turnLeft()
		fuelTaken = fuelTaken + 1
		turtle.suck(1)
		turtle.refuel(64)
		turtle.turnRight()
	end
	if fuelTaken > 0 then
		turtle.turnLeft()
		turtle.suck(math.min(math.ceil(fuelTaken / 8), 64))--get fuel to re-fuel furnace later
		turtle.turnRight()
	end
end

local function waitForTree()
	bbTurtle.rUp()
	while turtle.detect() == false do
		turtle.turnLeft()
		turtle.turnLeft()
		bbTurtle.rForward()
		turtle.turnLeft()
		turtle.turnLeft()
		sleep(60)
		bbTurtle.rForward()
	end
	bbTurtle.rDown()
end

local function doTree()
	turtle.select(1)
	local c = 0
	bbTurtle.rForward(false)
	while turtle.detect() do
		bbTurtle.dig("f")
		bbTurtle.rUp(false)
		c = c + 1
	end
	turtle.turnRight()
	bbTurtle.rForward(false)
	turtle.turnLeft()
	for i = 1, c do
		bbTurtle.dig("f")
		bbTurtle.rDown(false)
	end
	bbTurtle.dig("f")
	turtle.turnLeft()
	bbTurtle.rForward(false)
	turtle.turnRight()
	turtle.back()
end

local function collectDrops()
	turtle.select(1)
	bbTurtle.rDown()
	bbTurtle.rDown()
	for i = 1, 3 do
		turtle.suckDown(64)
		turtle.suckUp(64)
		sleep(1)
	end
	bbTurtle.rUp()
	bbTurtle.rUp()
end

local function dumpItems()
	if bbTurtle.select("minecraft:spruce_log") ~= nil then
		turtle.back()
		turtle.turnLeft()
		for j = 1, 3 do
			bbTurtle.rUp()
		end
		while bbTurtle.select("minecraft:spruce_log") ~= nil do
			if turtle.drop(64) == false then
				hook.send("Log chest full <@198768745278078976>", discordWebhookName)
				print("Press enter to retry")
				read()
			end
		end
		for j = 1, 3 do
			bbTurtle.rDown()
		end
		turtle.turnRight()
		bbTurtle.rForward()
	end
	while bbTurtle.select("minecraft:stick") ~= nil do
		turtle.refuel(turtle.getItemCount())
	end
	if bbTurtle.select("minecraft:spruce_sapling") ~= nil or bbTurtle.select("minecraft:charcoal") ~= nil then
		turtle.back()
		turtle.back()
		turtle.turnLeft()
		while bbTurtle.select("minecraft:spruce_sapling") ~= nil do
			if turtle.drop(64) == false then
				turtle.dropDown(64)
			end
		end
		
		if bbTurtle.select("minecraft:charcoal") ~= nil then
			bbTurtle.rUp()
			bbTurtle.rUp()
			while bbTurtle.select("minecraft:charcoal") ~= nil do
				if turtle.drop(64) == false then
					turtle.dropDown(64)
				end
			end
			bbTurtle.rDown()
			bbTurtle.rDown()
		end
		
		turtle.turnRight()
		bbTurtle.rForward()
		bbTurtle.rForward()
	end
	for i = 1, 16 do
		turtle.select(i)
		turtle.dropDown(64)
	end
end

local function plantTree()
	if bbTurtle.select("minecraft:spruce_sapling") == nil or turtle.getItemCount() < 4 then
		turtle.back()
		turtle.back()
		turtle.turnLeft()
		turtle.suck(4)
		bbTurtle.select("minecraft:spruce_sapling")
		turtle.turnRight()
		bbTurtle.rForward()
		bbTurtle.rForward()
	end
	if bbTurtle.select("minecraft:spruce_sapling") == nil or turtle.getItemCount() < 4 then
		hook.send("Out of saplings <@198768745278078976>", discordWebhookName)
		print("Place saplings in selected slot and press enter")
		read()
	end
	bbTurtle.rUp()
	bbTurtle.rForward()
	turtle.placeDown()
	bbTurtle.rForward()
	turtle.placeDown()
	turtle.turnRight()
	bbTurtle.rForward()
	turtle.placeDown()
	turtle.turnRight()
	bbTurtle.rForward()
	turtle.placeDown()
	bbTurtle.rForward()
	turtle.turnRight()
	bbTurtle.rForward()
	turtle.turnRight()
	bbTurtle.rDown()
end

while true do
	checkFuel()
	waitForTree()
	hook.send("Tree has grown", discordWebhookName)
	doTree()
	collectDrops()
	plantTree()
	dumpItems()
end