--local basalt = require("./basalt")--pastebin run ESs1mg7P packed
local wRequire = require("./wRequire")
local tableFile = wRequire("tableFile")
local invClient = wRequire("invClient")
local util = wRequire("util")
local itemF = wRequire("storageServerItem")

local config = tableFile.load("#vendingMachineAttachedToStorageNetworkController.lua", {})

if config.modemName == nil then
	print("Modem name:")
	config.modemName = read()
end
if config.monitor == nil then
	print("Monitor:")
	config.monitor = read()
end
if config.storageServerID == nil then
	print("storageServerID:")
	config.storageServerID = tonumber(read())
end

invClient.storageComputerID = config.storageServerID
invClient.silent = true

tableFile.save(config, "#vendingMachineAttachedToStorageNetworkController.lua")

if config.modemName ~= "" then
	rednet.open(config.modemName)
end

local trades = {}


local function fetchLatestTrades()
	local tradesBaseUrl = "https://gitlab.com/billyg270/cc/-/raw/master/vendingMachine/trades/"
	local tradesTabsUrl = tradesBaseUrl .. "tabs.txt"
	shell.execute("rm", "#tabs.txt")
	shell.execute("wget", tradesTabsUrl, "#tabs.txt")
	local tradeTabs = tableFile.load("#tabs.txt", {})
	
	local dispNamesUrl = tradesBaseUrl .. "dispNames.txt"
	shell.execute("rm", "#dispNames.txt")
	shell.execute("wget", dispNamesUrl, "#dispNames.txt")
	local dispNames = tableFile.load("#dispNames.txt", {})
	
	local function getDispName(id, damage, nbt)
		local value = dispNames[id .. "," .. tostring(damage) .. "," .. nbt]
		if value == nil then
			
			local idParts = {}
			for part in string.gmatch(id, '([^:]+)') do
				table.insert(idParts, part)
			end
			
			local toRet = idParts[2]
			toRet = string.gsub(toRet, "%_", " ")
			toRet = string.gsub(toRet, "^%l", string.sub(string.upper(toRet), 1, 1))
			return toRet
		end
		return value
	end
	
	trades = {}
	
	for disName, fileName in pairs(tradeTabs) do
		trades[disName] = {}
		
		local tradesTabUrl = tradesBaseUrl .. fileName
		shell.execute("rm", "#tab.csv")
		shell.execute("wget", tradesTabUrl, "#tab.csv")
		
		local file = fs.open("#tab.csv", "r")
		
		--for line in string.gmatch(file.readAll(), "([^\n]*)\n?") do
		while true do
			local line = file.readLine()
			if not line then
				break
			end
			
			local lineParts = {}
			for part in string.gmatch(line, '([^,]+)') do
				table.insert(lineParts, part)
			end
			
			print(textutils.serialise(lineParts))
			
			if lineParts[1] ~= "Cost Count" then--header row
				if lineParts[4] == "." then
					lineParts[4] = ""
				end
				if lineParts[9] == "." then
					lineParts[9] = ""
				end
				
				table.insert(trades[disName], {
					["cost"] = {
						["id"] = lineParts[2],
						["displayName"] = getDispName(lineParts[2], tonumber(lineParts[3]), lineParts[4]),
						["damage"] = tonumber(lineParts[3]),
						["nbtHash"] = lineParts[4],
						["count"] = tonumber(lineParts[1]),
						["max"] = tonumber(lineParts[5])
					},
					["product"] = {
						["id"] = lineParts[7],
						["displayName"] = getDispName(lineParts[7], tonumber(lineParts[8]), lineParts[9]),
						["damage"] = tonumber(lineParts[8]),
						["nbtHash"] = lineParts[9],
						["count"] = tonumber(lineParts[6]),
						["min"] = tonumber(lineParts[10]),
					}
				})
			end
		end
	end
end

fetchLatestTrades()

print(textutils.serialise(trades))

--local itemsToCheck = {}

local function networkLoop()
	while true do
		local id, message = rednet.receive("bbMerch-c2s")
		if message ~= nil and type(message) == "table" then
			if message["message"] == "started" then
				rednet.send(id, {
					["trades"] = trades
				}, "bbMerch-s2c")
			elseif message["message"] == "trade" then
				--print("Trade:")
				--print("Computer ID: " .. tostring(id))
				--print(textutils.serialise(message["trade"]))
				print(
					"[" .. tostring(os.date("%d/%m/%y %H:%M:%S")) .. "] " ..
					"(" .. tostring(id) .. ") " ..
					tostring(message["trade"].cost.count) .. "x " ..
					message["trade"].cost.displayName ..
					"  -->  " ..
					tostring(message["trade"].product.count) .. "x " ..
					message["trade"].product.displayName ..
					" - " .. message["res"]
				)
				
				--table.insert(itemsToCheck, message["trade"].cost.id)
				--table.insert(itemsToCheck, message["trade"].product.id)
			end
		end
	end
end

local monitor = peripheral.wrap(config.monitor)
monitor.setTextScale(0.5)
monitor.clear()
term.redirect(monitor)
monitor.setCursorPos(1, 1)

--[[
local mainFrame = basalt.createFrame("mainFrame")
	:setMonitor(config.monitor)
	:setSize(monitor.getSize())
	:show()


local refreshTradesFileButton = mainFrame:addButton("refreshTradesFileButton")
	:setPosition(1, 1)
	:setSize(20, 1)
	:setText("Reload trades file")
	:onClick(function ()
		trades = tableFile.load("#trades.lua")
		rednet.broadcast({
			["trades"] = trades
		}, "bbMerch-s2c")
	end)
	:show()


parallel.waitForAny(networkLoop, basalt.autoUpdate)
]]--

local function countTradeItem(ti, counts)
	local item = itemF.getItemNameAndNbt({
		["name"] = ti.id,
		["damage"] = ti.damage,
		["nbt"] = ti.nbtHash
	})
	if counts == nil then
		return invClient.count(item)
	else
		local count = counts[item]
		if count == nil then
			return 0
		end
		return count
	end
end

local function stockCheckLoop()
	while true do
		local counts = invClient.getCounts()
		local changed = false
		for tradeCatagory, catagoryTrades in pairs(trades) do
			for i = 1, #catagoryTrades do
				local enabledBefore = trades[tradeCatagory][i].enabled
				
				trades[tradeCatagory][i].enabled = true
				
				local costCount = countTradeItem(catagoryTrades[i].cost, counts)
				
				if catagoryTrades[i].cost.max ~= nil then
					if costCount > catagoryTrades[i].cost.max then
						trades[tradeCatagory][i].enabled = false
					end
				end
				
				if catagoryTrades[i].cost.min ~= nil then
					if costCount < catagoryTrades[i].cost.min then
						trades[tradeCatagory][i].enabled = false
					end
				end
				
				local productCount = countTradeItem(catagoryTrades[i].product, counts)
				
				if catagoryTrades[i].product.max ~= nil then
					if productCount > catagoryTrades[i].product.max then
						trades[tradeCatagory][i].enabled = false
					end
				end
				
				if catagoryTrades[i].product.min ~= nil then
					if productCount < catagoryTrades[i].product.min then
						trades[tradeCatagory][i].enabled = false
					end
				end
				
				if enabledBefore ~= trades[tradeCatagory][i].enabled then
					changed = true
				end
			end
		end
		
		if changed then
			print("Pushing trades change")
			rednet.broadcast({
				["trades"] = trades
			}, "bbMerch-s2c")
		end
		
		sleep(10)
	end
end

parallel.waitForAny(networkLoop, stockCheckLoop)