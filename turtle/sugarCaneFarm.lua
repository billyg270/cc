local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")
local bbTurtleDock = wRequire("bbTurtleDock")
local farm = wRequire("farm")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")
local inv = wRequire("invClient")

local config = tableFile.load("#sugarCaneFarm.lua", nil)

if config == nil then
	config = {}
	print("Width:")
	config.width = tonumber(read())
	print("Depth:")
	config.depth = tonumber(read())
	print("Modem Side:")
	config.modemSide = read()
	print("Chest Side:")
	config.chestSide = read()
	print("Chest Network Name:")
	config.chestNetworkName = read()
	print("Min sugar cane in storage:")
	config.minItemInStorage = read()
end

tableFile.save(config, "#sugarCaneFarm.lua")

bbTurtleDock.modemSide = config.modemSide
bbTurtleDock.chestSide = config.chestSide
bbTurtleDock.chestNetworkName = config.chestNetworkName

function Cycle()
	bbTurtleDock.doBasicDock(500)
	turtle.select(1)
	farm.farm(
		config.width,
		config.depth,
		function ()
			bbTurtle.rForward(false)
			bbTurtle.dig("d")
		end
	)
	bbTurtleDock.doBasicDock(500)
end

while true do
	rednet.open(config.modemSide)
	while inv.count("minecraft:sugar_cane") > config.minItemInStorage do
		print("Got enough in storage, will wait")
		longSleep(60 * 15)
	end
	rednet.close(config.modemSide)
	Cycle()
	longSleep(18 * 2 * 60 * 1.5)--18 mins per block on average - then multiplied by 1.5 to allow others to grow
end