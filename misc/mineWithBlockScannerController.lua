local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load("#mineController.lua", {
	["miningConfigs"] = {
		["goHome"] = {
			["goto"] = {
				["x"] = 2499,
				["y"] = 146,
				["z"] = -60
			}
		},
		["overworld11"] = {
			["minY"] = 5 + 8,
			["maxY"] = 15,
			["blockScores"] = {
				["minecraft:diamond_ore"] = 10,
				["minecraft:gold_ore"] = 8,
				["minecraft:iron_ore"] = 5,
				["minecraft:coal_ore"] = 16,
				["minecraft:redstone_ore"] = 0.1,
				["minecraft:lapis_ore"] = 0.15,
				["minecraft:emerald_ore"] = 50,
				["minecraft:oak_log"] = 9,
				["minecraft:dark_oak_log"] = 9,
				["minecraft:birch_log"] = 9,
				["minecraft:jungle_log"] = 9,
				["minecraft:acaia_log"] = 9,
				["minecraft:crying_obsidian"] = 31,
				["minecraft:obsidian"] = 1,
				["minecraft:sand"] = 3,
				["minecraft:deepslate_diamond_ore"] = 10,
				["minecraft:deepslate_gold_ore"] = 8,
				["minecraft:deepslate_iron_ore"] = 5,
				["minecraft:deepslate_coal_ore"] = 16,
				["minecraft:deepslate_redstone_ore"] = 0.1,
				["minecraft:deepslate_lapis_ore"] = 0.15,
				["minecraft:deepslate_emerald_ore"] = 50,
			}
		},
		["nether"] = {
			["minY"] = 5 + 8,
			["maxY"] = 123 - 8,
			["blockScores"] = {
				["minecraft:nether_gold_ore"] = 5,
				["minecraft:nether_quartz_ore"] = 5,
				["minecraft:ancient_debris"] = 30,
				["minecraft:crying_obsidian"] = 31,
				["minecraft:obsidian"] = 1,
				["minecraft:glowstone"] = 15,
				["minecraft:soul_sand"] = 3,
			}
		}
	},
	["turtleConfigAssignments"] = {
		[21] = "overworld11"
	},
	["blocksToDump"] = {
		["minecraft:cobblestone"] = true
	}
})

if config.modemName == nil then
	print("Modem name:")
	config.modemName = read()
end

if config.monitorName == nil then
	print("Monitor name:")
	config.monitorName = read()
end

local monitor = nil
if config.monitorName ~= nil and config.monitorName ~= "" then
	monitor = peripheral.wrap(config.monitorName)
	monitor.setTextScale(0.5)
end

tableFile.save(config, "#mineController.lua")

rednet.open(config.modemName)

local lastSeen = tableFile.load("#mineControllerLastSeen.lua", {})

print("Started controller")

while true do
    local id, message = rednet.receive()
	if type(message) == "table" then
		print("-----------------------------")
		print("[" .. tostring(os.date()) .. "] Message from " .. id .. ": ")
		message["timestamp2"] = tostring(os.date("%d/%m/%y %H:%M:%S"))
		print(textutils.serialize(message))
		print("-----------------------------")
	end
    
    lastSeen[id] = message
	tableFile.save(lastSeen, "#mineControllerLastSeen.lua")
	
	if monitor ~= nil then
		monitor.clear()
		local c = 0
		for lsk, lsv in pairs(lastSeen) do
			c = c + 1
			monitor.setCursorPos(1, c)
			monitor.write(
				util.padString(tostring(lsk), 4) .. " | " ..
				util.padString(lsv["timestamp2"], 16) .. " | " ..
				util.padString(tostring(lsv["x"]), 6) .. "  " ..
				util.padString(tostring(lsv["y"]), 4) .. "  " ..
				util.padString(tostring(lsv["z"]), 6) .. " | " ..
				util.padString(config.turtleConfigAssignments[lsk], 6)
			)
		end
	end
    
    if config.turtleConfigAssignments[id] ~= nil then
        local toSend = config.miningConfigs[config.turtleConfigAssignments[id]]
		toSend["timestamp"] = os.time()
		toSend["timestamp2"] = tostring(os.date())
		toSend["blocksToDump"] = config.blocksToDump
		rednet.send(id, toSend)
	else
		print("No config assigned to " .. tostring(id))
    end
end
