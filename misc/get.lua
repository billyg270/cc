local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")
local inv = wRequire("invClient")

local config = tableFile.load("#get.lua", nil)

if config == nil then
	config = {}
	print("Modem Side:")
	config.modemSide = read()
	print("Chest Network Name:")
	config.chestNetworkName = read()
	print("Chest Local Name:")
	config.chestLocalName = read()
	print("Storage server ID:")
	config.storageServerID = tonumber(read())
	print("Item ID to get (eg: minecraft:stone.0.):")
	config.itemID = read()
	print("Item stack size (eg: 64):")
	config.itemStackSize = tonumber(read())
	--print("Min item count in network (eg: 1000):")
	--config.minItemCountInNetwork = tonumber(read())
	print("Sleep time (eg: 1):")
	config.sleepTime = tonumber(read())
end

tableFile.save(config, "#get.lua")

inv.storageComputerID = config.storageServerID

rednet.open(config.modemSide)

while true do
	local chest = peripheral.wrap(config.chestNetworkName)
	if #(chest.list()) < (chest.size() / 2) then
		print("Getting...")
		inv.getToAnySlot(config.itemID, config.itemStackSize, config.chestNetworkName, config.chestLocalName)
	else
		print("Chest has enough items")
	end
	longSleep(config.sleepTime)
end