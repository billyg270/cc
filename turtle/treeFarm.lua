local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")
local farm = wRequire("farm")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")
local inv = wRequire("invClient")

--[[
	Expects network connected chest above
]]--

local hookSuccess, hook = DiscordHook.createWebhook("https://discord.com/api/webhooks/889783581839339541/mtTZmSBCoa-KGi2DN5S5_tQMPJhpPjOjDB3dy8EtY-Dxu2CK9pTeBwMy7IkKMGyVpkti")
if not hookSuccess then
	error("Webhook connection failed! Reason: " .. hook)
end

local config = tableFile.load("#treeFarm.lua", nil)

if config == nil then
	config = {}
	print("Width:")
	config.width = tonumber(read())
	print("Depth:")
	config.depth = tonumber(read())
	print("Modem Side:")
	config.modemSide = read()
	print("Chest Network Name:")
	config.chestNetworkName = read()
	print("Min logs in storage:")
	config.minItemInStorage = tonumber(read())
	print("storageComputerID:")
	config.storageComputerID = tonumber(read())
	print("Log block ID:")
	config.logType = read()--minecraft:spruce_logs
	print("Fuel item ID in storage network:")
	config.fuelItemIdInStorageNetwork = read()
	print("Plant chance: (0 - 100):")
	config.plantChance = tonumber(read())
	print("Sapling item ID:")
	config.saplingType = read()
end

tableFile.save(config, "#treeFarm.lua")

inv.storageComputerID = config.storageComputerID

function Cycle()
	rednet.open(config.modemSide)
	
	local logCount = 0
	local saplingToKeepCount = 0
	for i = 1, 16 do
		turtle.select(i)
		local itemDetail = turtle.getItemDetail()
		if itemDetail ~= nil then
			if itemDetail.name == config.logType then
				logCount = logCount + itemDetail.count
				turtle.dropUp(turtle.getItemCount())
			elseif itemDetail.name == config.saplingType then
				if saplingToKeepCount > 64 * 1 then
					turtle.dropUp(turtle.getItemCount())
				end
				saplingToKeepCount = saplingToKeepCount + itemDetail.count
			else
				turtle.dropUp(turtle.getItemCount())
			end
		end
	end
	inv.storeAllFromIoChest(config.chestNetworkName, "top")
	hook.send("Chopped: " .. tostring(logCount) .. " logs", "Tree Farm #" .. tostring(os.getComputerID()))
	
	turtle.select(1)
	local requiredFuel = 5000
	while turtle.getFuelLevel() < requiredFuel do
		inv.storeAllFromIoChest(config.chestNetworkName, "top")
		if(inv.get(config.fuelItemIdInStorageNetwork, 5, config.chestNetworkName, 1) < 5) then
			print("Run out of " .. config.fuelItemIdInStorageNetwork .. " in storage network")
		end
		turtle.suckUp(5)
		turtle.refuel(turtle.getItemCount())
		print("Refueling: " .. tostring(turtle.getFuelLevel()) .. "/" .. tostring(requiredFuel))
		sleep(3)
	end
	
	rednet.close(config.modemSide)
	
	bbTurtle.rForward(false)
	farm.farm(
		config.width,
		config.depth,
		function ()
			print("Step")
			turtle.select(1)
			bbTurtle.rForward(false)
			
			local c = 0
			while turtle.detectUp() do
				bbTurtle.rUp(false)
				for _ = 1, 4 do
					local blockInfront, blockInfrontDetails = turtle.inspect()
					if blockInfront and blockInfrontDetails.name ~= config.logType then
						turtle.dig()
					end
					turtle.turnLeft()
				end
				c = c + 1
			end
			
			while c > 0 do
				c = c - 1
				bbTurtle.rDown(false)
			end
			
			local blockBelow, blockBelowDetails = turtle.inspectDown()
			if blockBelow and blockBelowDetails.name == config.logType then
				bbTurtle.dig("d")
			end
			
			if math.random(100) <= config.plantChance then
				if bbTurtle.select(config.saplingType) ~= nil then
					turtle.placeDown()
				end
			end
		end
	)
	turtle.turnLeft()
	turtle.turnLeft()
	bbTurtle.rForward(false)
	turtle.turnLeft()
	turtle.turnLeft()
end

while true do
	rednet.open(config.modemSide)
	while inv.count(config.logType) > config.minItemInStorage do
		print("Got enough in storage, will wait")
		longSleep(60 * 15)
	end
	rednet.close(config.modemSide)
	Cycle()
	--longSleep(18 * 2 * 60 * 1.1)--18 mins per block on average - then multiplied by 1.1 to allow others to grow
	longSleep(2 * 2 * 60)--2 min sleep
end