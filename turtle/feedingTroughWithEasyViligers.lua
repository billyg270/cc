-- Vil farm on top
-- Feeder below

local wRequire = require("wRequire")
local longSleep = wRequire("longSleep")

while true do
    for i = 1, 16 do
        turtle.select(i)
        turtle.suckUp(64)
    end

    for i = 1, 16 do
        turtle.select(i)
        local itemDetail = turtle.getItemDetail()

        if itemDetail ~= nil then
            if itemDetail.name == "minecraft:wheat" then
                turtle.dropDown(64)
            else
                turtle.drop(64)
            end
        end
    end

    longSleep(60 * 5)
end