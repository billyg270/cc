local function longSleep(t)
	for i = 1, t do
		print("Sleeping: " .. i .. "/" .. t)
		sleep(1)
	end
end

return longSleep