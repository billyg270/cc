local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local invClient = wRequire("invClient")

rednet.open("right")

invClient.storageComputerID = 12

local width = 6
local depth = 11

local botChestName = "enderstorage:ender_chest_14"
local toSmeltChestName = "enderstorage:ender_chest_13"
local smeltedChestName = "storagedrawers:standard_drawers_1_0"

local xpFor30levels = 1395
local checkFurnaceEach = 1
local checkFurnaceCount = 10000

local furnaces = tableFile.load("#furnaces.lua", {})

local function refuel()
	if turtle.getFuelLevel() > 2000 then
		return
	end
	
	local networkName = peripheral.wrap("top").getNameLocal()
	local botChest = peripheral.wrap(botChestName)
	
	turtle.select(1)
	turtle.dropDown(turtle.getItemCount())
	
	while turtle.getFuelLevel() < 5000 do
		print("Fuel: " .. tostring(turtle.getFuelLevel()) .. "/5000")
		local botChestList = botChest.list()
		if botChestList[1] ~= nil and botChestList[1].count > 32 then
			print("Refueling")
			botChest.pushItems(networkName, 1, 10, 1)
			turtle.refuel(turtle.getItemCount())
		else
			print("Fetching fuel from storage network")
			invClient.get("minecraft:charcoal.0.", 16, "enderstorage:ender_chest_2", 1)
		end
	end
end

local function goto(x, y, action)
	turtle.turnRight()
	for x2 = 1, x do
		turtle.forward()
	end
	turtle.turnLeft()
	
	for y2 = 1, y do
		turtle.forward()
	end
	
	action()
	
	for y2 = 1, y do
		turtle.back()
	end
	
	turtle.turnRight()
	for x2 = 1, x do
		turtle.back()
	end
	turtle.turnLeft()
end

local function mapFurnaces()
	refuel()
	local toSmeltChest = peripheral.wrap(toSmeltChestName)
	
	turtle.select(1)
	turtle.dropDown(turtle.getItemCount())
	
	local furnacePeripherals = { peripheral.find("minecraft:furnace") }
	
	for _, furnacePeripheral in pairs(furnacePeripherals) do
		furnacePeripheral.pushItems(peripheral.wrap("top").getNameLocal(), 1, 64)
	end
	
	while turtle.getItemCount() < 1 do
		print("Fetching filler items")
		toSmeltChest.pushItems(peripheral.wrap("top").getNameLocal(), 1, 1)
	end
	
	for _, furnacePeripheral in pairs(furnacePeripherals) do
		local furnaceName = peripheral.getName(furnacePeripheral)
		if furnaces[furnaceName] == nil then
			furnacePeripheral.pullItems(peripheral.wrap("top").getNameLocal(), 1, 1)
			print("Searching for position of furnace: " .. furnaceName)
			local found = false
			
			local function positionAlreadyFound(x, y)
				for _, furnace in pairs(furnaces) do
					if furnace.x == x and furnace.y == y then
						return true
					end
				end
				return false
			end
			
			local function find()
				for x = 0, (width - 1) do
					for y = 0, (depth - 1) do
						if (x == 0 and y == 0) or positionAlreadyFound(x, y) then
						else
							goto(x, y, function()
								local above = peripheral.wrap("top")
								if above ~= nil then
									local aboveList = above.list()
									if aboveList[1] ~= nil and aboveList[1].count == 1 then
										found = true
									end
								end
							end)
							
							if found then
								print("Found position of furnace: " .. furnaceName .. " " .. tostring(x) .. ", " .. tostring(y))
								furnaces[furnaceName] = {
									["name"] = furnaceName,
									["x"] = x,
									["y"] = y,
									["xp"] = 0
								}
								tableFile.save(furnaces, "#furnaces.lua")
								rednet.broadcast(furnaces, "bbXpShop-r2s")
								furnacePeripheral.pushItems(peripheral.wrap("top").getNameLocal(), 1, 64)
								return
							end
						end
					end
				end
			end
			
			find()
		
			if found == false then
				error("Not found")
			end
		end
	end
	
	turtle.dropDown(64)
end

mapFurnaces()

local xpRequested = 0

local function networkLoop()
	--rednet.broadcast("ready", "bbXpShop-r2s")
	while true do
		local id, message = rednet.receive("bbXpShop-s2r", 10)
		if message ~= nil then
			xpRequested = xpRequested + message
			print(xpRequested)
			rednet.broadcast(furnaces, "bbXpShop-r2s")
		end
	end
end

local function getXp()
	if xpRequested > 0 then
		for _, furnace in pairs(furnaces) do
			os.sleep()
			if furnace.xp >= xpRequested then
				refuel()
				goto(furnace.x, furnace.y, function()
					turtle.digUp()
					if furnace.y > 1 then
						turtle.back()
						sleep(0.8)
						turtle.forward()
					else
						turtle.forward()
						sleep(0.8)
						turtle.back()
					end
					for i = 1, 16 do
						turtle.select(i)
						local itemDetail = turtle.getItemDetail()
						if itemDetail ~= nil and itemDetail.name == "minecraft:furnace" then
							turtle.placeUp()
							break
						else
							turtle.dropDown(turtle.getItemCount())
						end
					end
				end)
				
				xpRequested = xpRequested - furnace.xp
				furnaces[furnace.name].xp = 0
				tableFile.save(furnaces, "#furnaces.lua")
				rednet.broadcast(furnaces, "bbXpShop-r2s")
				
				if xpRequested <= 0 then
					xpRequested = 0
					break
				end
			end
		end
	end
end

local function checkFurnaces()
	local checked = 0
	for _, furnace in pairs(furnaces) do
		os.sleep()
		if furnace.timesSinceLastCheck == nil then
			furnace.timesSinceLastCheck = 0
		end
		if furnace.timesSinceLastCheck >= checkFurnaceEach and checked < checkFurnaceCount and furnaces[furnace.name].xp < xpFor30levels then
			furnace.timesSinceLastCheck = 0
			checked = checked + 1
			local f = peripheral.wrap(furnace.name)
			if f == nil then
				local furnacePeripherals = { peripheral.find("minecraft:furnace") }
				for _, furnacePeripheral in pairs(furnacePeripherals) do
					local fName = peripheral.getName(furnacePeripheral)
					local inList = false
					for _, furnace2 in pairs(furnaces) do
						if furnace2.name == fName then
							inList = true
						end
					end
					if inList == false then
						furnace.name = fName
						f = peripheral.wrap(furnace.name)
						break
					end
				end
			end
			
			if f == nil then
				error("Furnace not found")
			end
			
			local fList = f.list()
			
			--take items out
			if fList[3] ~= nil and fList[3].count >= 8 then
				f.pushItems(smeltedChestName, 3, fList[3].count)
				furnaces[furnace.name].xp = furnaces[furnace.name].xp + fList[3].count
				tableFile.save(furnaces, "#furnaces.lua")
				rednet.broadcast(furnaces, "bbXpShop-r2s")
			end
			
			--restock
			if fList[1] == nil or fList[1].count == 0 and furnaces[furnace.name].xp < xpFor30levels then
				local toSmeltPulled = 0
				for i = 1, 27 do
					if toSmeltPulled < 8 then
						toSmeltPulled = toSmeltPulled + f.pullItems(toSmeltChestName, i, 8 - toSmeltPulled, 1)
					end
				end
				
				local botChest = peripheral.wrap(botChestName)
				while f.list()[2] == nil or f.list()[2].count == 0 do
					local botChestList = botChest.list()
					if botChestList[1] ~= nil and botChestList[1].count > 32 then
						if f.pullItems(botChestName, 1, 1, 2) > 0 then
							break
						end
					else
						invClient.get("minecraft:charcoal.0.", 16, "enderstorage:ender_chest_2", 1)
					end
				end
			end
			
			getXp()
		else
			furnace.timesSinceLastCheck = furnace.timesSinceLastCheck + 1
		end
	end
end

local function actionLoop()
	while true do
		checkFurnaces()
		getXp()
	end
end

parallel.waitForAny(networkLoop, actionLoop)