local reactor = peripheral.find("BigReactors-Reactor")
local battery = peripheral.find("draconic_rf_storage")

local function cycle()
	print("-- Checking energy levels --")
	
	local active = reactor.getActive()
	local energy = battery.getEnergyStored()
	local maxEnergy = battery.getMaxEnergyStored()
	
	if energy > (maxEnergy * 0.75) and active == true then
		print("Stopping reactor as battey is 75% full")
		reactor.setActive(false)
	elseif energy < (maxEnergy * 0.25) and active == false then
		print("Starting reactor as battey is less than 25% full")
		reactor.setActive(true)
	else
		print("No action needed")
	end
end

while true do
	cycle()
	sleep(60)
end