local wRequire = require("wRequire")

local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load("#createRailwayOutpost.lua", nil)

print("-------")
print("All stations should have unique names except depos which can share the same name")
print("All trains using this station should be in the following format: \"auto-ITEM-1\" where ITEM is either ITEM / FLUID / ENERGY and the number is unique to each train")
print("-------")

if config == nil then
	config = {
        ["cargos"] = {}
    }

    print("Modem Side:")
	config.modemSide = read()
	
	print("Station Side:")
	config.stationSide = read()

    print("ITEM Interface threshold switch redstone peripheral - leave empty for computer:")
	config.interfaceThresholdSwitchPeripheral = read()

    print("ITEM Interface threshold switch redstone side:")
	config.interfaceThresholdSwitchSide = read()

    print("FLUID Interface threshold switch redstone peripheral - leave empty for computer:")
	config.fluidInterfaceThresholdSwitchPeripheral = read()

    print("FLUID Interface threshold switch redstone side:")
	config.fluidInterfaceThresholdSwitchSide = read()

    print("ENERGY interface threshold peripheral or side:")
	config.energyInterfaceSide = read()

	
	while true do
        print("------")

        print("Cargo type (ITEM / FLUID / ENERGY / END)")
        local cargoType = string.upper(read())
        if cargoType == "END" then
            print("Ended cargo inputs")
            print("------")
            break
        end

        print("Cargo direction (PU / DO)")
        local cargoDirection = string.upper(read())

        print("Cargo name")
        local cargoName = string.upper(read()):gsub("%s+", "_")

        print("Redstone input peripheral (where powered means it needs " .. cargoDirection .. ") - leave empty for computer")
        local cargoRedstoneInputPeripheral = read()

        print("Redstone input side (where powered means it needs " .. cargoDirection .. ")")
        local cargoRedstoneInputSide = read()

        print("Redstone output peripheral (where un-powered when train is docked for this cargo) - leave empty for computer")
        local cargoRedstoneOutputPeripheral = read()

        print("Redstone output side (where un-powered when train is docked for this cargo)")
        local cargoRedstoneOutputSide = read()

        table.insert(config.cargos, {
            ["type"] = cargoType,
            ["direction"] = cargoDirection,
            ["name"] = cargoName,
            ["redstoneInputPeripheral"] = cargoRedstoneInputPeripheral,
            ["redstoneInputSide"] = cargoRedstoneInputSide,
            ["redstoneOutputPeripheral"] = cargoRedstoneOutputPeripheral,
            ["redstoneOutputSide"] = cargoRedstoneOutputSide,
        })

        print("------")
    end
end

-- This if statement is because item and fluid interfaces have been seperated. Outposts build before this update only had ITEMs or FLUIDs, so this will work.
if config.fluidInterfaceThresholdSwitchPeripheral == nil then
    print("(Config migration) Copying ITEM interface settings to FLUID interface")
    config.fluidInterfaceThresholdSwitchPeripheral = config.interfaceThresholdSwitchPeripheral
    config.fluidInterfaceThresholdSwitchSide = config.interfaceThresholdSwitchSide
end

tableFile.save(config, "#createRailwayOutpost.lua")

local getItemInterfaceThresholdSwitch = nil

if config.interfaceThresholdSwitchSide ~= "" then
    if config.interfaceThresholdSwitchPeripheral ~= "" then
        local interfaceThresholdSwitchPeripheral = peripheral.wrap(config.interfaceThresholdSwitchPeripheral)
        getItemInterfaceThresholdSwitch = function()
            return interfaceThresholdSwitchPeripheral.getInput(config.interfaceThresholdSwitchSide)
        end
    else
        getItemInterfaceThresholdSwitch = function()
            return redstone.getInput(config.interfaceThresholdSwitchSide)
        end
    end
else
    getItemInterfaceThresholdSwitch = function()
        return false
    end
end

local getFluidInterfaceThresholdSwitch = nil

if config.fluidInterfaceThresholdSwitchSide ~= "" then
    if config.fluidInterfaceThresholdSwitchPeripheral ~= "" then
        local fluidInterfaceThresholdSwitchPeripheral = peripheral.wrap(config.fluidInterfaceThresholdSwitchPeripheral)
        getFluidInterfaceThresholdSwitch = function()
            return fluidInterfaceThresholdSwitchPeripheral.getInput(config.fluidInterfaceThresholdSwitchSide)
        end
    else
        getFluidInterfaceThresholdSwitch = function()
            return redstone.getInput(config.fluidInterfaceThresholdSwitchSide)
        end
    end
else
    getFluidInterfaceThresholdSwitch = function()
        return false
    end
end

local station = peripheral.wrap(config.stationSide)
rednet.open(config.modemSide)

local cargoRedstoneInputs = {}
local cargoRedstoneOutputs = {}
local cargoRedstonePuHosting = {}

for cargoIndex, cargo in pairs(config.cargos) do
    cargoRedstoneInputs[cargoIndex] = {
        ["peripheral"] = util.ternary(cargo.redstoneInputPeripheral == "", nil, peripheral.wrap(cargo.redstoneInputPeripheral)),
        ["side"] = cargo.redstoneInputSide,
        ["get"] = function(self)
            if self.side == "" then
                return false
            end

            if self.peripheral == nil then
                return redstone.getInput(self.side)
            end

            return self.peripheral.getInput(self.side)
        end,
    }

    cargoRedstoneOutputs[cargoIndex] = {
        ["peripheral"] = util.ternary(cargo.redstoneOutputPeripheral == "", nil, peripheral.wrap(cargo.redstoneOutputPeripheral)),
        ["side"] = cargo.redstoneOutputSide,
        ["set"] = function(self, powered)
            if self.side == "" then
                return
            end

            if self.peripheral == nil then
                redstone.setOutput(self.side, powered)
                return
            end

            self.peripheral.setOutput(self.side, powered)
            return
        end,
    }

    rednet.unhost("createRailwayOutpost-" .. cargo.type .. "-" .. cargo.name)
    cargoRedstonePuHosting[cargoIndex] = false
end

print("Started")

local trainOnWay = false
local trainPresentCount = 0

function cycle()
    if station.isTrainPresent() then
        trainPresentCount = trainPresentCount + 1
        print("Train present " .. trainPresentCount)
        -- logic for train at station (use schedule name (first instruction)?)
        local trainName = station.getTrainName()
        local trainNameParts = util.splitString(trainName, "-")--eg: "auto-ITEM-1"
        if trainNameParts[1] == "auto" and station.hasSchedule() then
            local schedule = station.getSchedule()
            local scheduleName = schedule.entries[1].instruction.data.text
            local scheduleNameParts = util.splitString(scheduleName, "-")--eg: "auto-ITEM-COBBLESTONE"

            if scheduleNameParts[1] == "auto" then
                local presentTrainCargoType = scheduleNameParts[2]
                local presentTrainCargoName = scheduleNameParts[3]

                local trainFull = false
                if presentTrainCargoType == "ENERGY" then
                    if config.energyInterfaceSide ~= "" and config.energyInterfaceSide ~= nil then
                        local p = peripheral.wrap(config.energyInterfaceSide)
                        print("ENERGY " .. p.getEnergy() .. "/" .. p.getCapacity())
                        trainFull = p.getEnergy() >= p.getCapacity()
                    end
                elseif presentTrainCargoType == "FLUID" then
                    trainFull = getFluidInterfaceThresholdSwitch()
                else
                    trainFull = getItemInterfaceThresholdSwitch()
                end

                print("Cargo type: " .. presentTrainCargoType)
                print("Cargo name: " .. presentTrainCargoName)
                print("Train full: " .. util.ternary(trainFull, "yes", "no"))

                for cargoIndex, cargo in pairs(config.cargos) do
                    if cargo.type == presentTrainCargoType and cargo.name == presentTrainCargoName then
                        if cargo.direction == "PU" then
                            cargoRedstoneOutputs[cargoIndex]:set(trainFull)
                            redstone.setOutput(config.stationSide, trainFull and trainPresentCount > 10)
                        else
                            cargoRedstoneOutputs[cargoIndex]:set(true)
                            redstone.setOutput(config.stationSide, trainFull == false)
                        end
                    end
                end

                trainOnWay = false
                return
            end
        end
    else
        trainPresentCount = 0
    end

    for _, cargoRedstoneOutput in pairs(cargoRedstoneOutputs) do
        cargoRedstoneOutput:set(true)
    end

    redstone.setOutput(config.stationSide, false)

    if trainOnWay then
        return
    end

    for cargoIndex, cargo in pairs(config.cargos) do
        if cargo.direction == "PU" then
            local nameToHost = "createRailwayOutpost-" .. cargo.type .. "-" .. cargo.name
            if cargoRedstonePuHosting[cargoIndex] then
                if trainOnWay == true or cargoRedstoneInputs[cargoIndex]:get() == false then
                    print("Unhosting: " .. nameToHost)
                    rednet.unhost(nameToHost)
                    cargoRedstonePuHosting[cargoIndex] = false
                else
                    print("Listening for messages from DO outposts: " ..  nameToHost)
                    local dropoffStationComputerId, dropoffStationMsg = rednet.receive(nameToHost, 2.5)
                    if dropoffStationComputerId then
                        print("Got message from dropoff station for " .. nameToHost .. ". Unhosting")
                        rednet.unhost(nameToHost)
                        cargoRedstonePuHosting[cargoIndex] = false
                        trainOnWay = true
                        sleep(0.1)
                        rednet.send(dropoffStationComputerId, station.getStationName(), nameToHost .. "-resp-" .. os.getComputerID())

                        local dropoffStationComputerId2, dropoffStationMsg2 = rednet.receive(nameToHost, 6)
                        if dropoffStationComputerId2 == dropoffStationComputerId and dropoffStationMsg2 == "OK" then
                            print("Confirmed OK")
                        else
                            print("No confirmation, re-hosting")
                            rednet.host(nameToHost, nameToHost .. "-" .. os.getComputerID())
                            cargoRedstonePuHosting[cargoIndex] = true
                            trainOnWay = false
                        end
                    end
                end
            else
                if trainOnWay == false and cargoRedstoneInputs[cargoIndex]:get() == true then
                    print("Hosting: " .. nameToHost)
                    rednet.host(nameToHost, nameToHost .. "-" .. os.getComputerID())
                    cargoRedstonePuHosting[cargoIndex] = true
                end
            end
        end
        if cargo.direction == "DO" then
            if cargoRedstoneInputs[cargoIndex]:get() then
                -- One that needs DO
                print(cargo.type .. "-" .. cargo.name .. " needs DO")

                -- Search for outposts with cargo avaliable for pickup
                local outpostsLookupResp = {rednet.lookup("createRailwayOutpost-" .. cargo.type .. "-" .. cargo.name)}
                local pickupOutpostId = nil
                for outpostIndex, outpostId in pairs(outpostsLookupResp) do
                    pickupOutpostId = outpostId
                    break
                end

                if pickupOutpostId ~= nil then
                    -- Ping pickup outpost
                    print("Pinging pickup outpost: " .. pickupOutpostId)
                    rednet.send(
                        pickupOutpostId,
                        {},
                        "createRailwayOutpost-" .. cargo.type .. "-" .. cargo.name
                    )

                    -- wait for reply with station name
                    local pickupStationRespId, pickupStationResp = rednet.receive("createRailwayOutpost-" .. cargo.type .. "-" .. cargo.name .. "-resp-" .. pickupOutpostId, 1)
                    if pickupStationRespId then
                        local pickupStationName = pickupStationResp
                        print("Reply from pickup outpost: " .. pickupStationName)

                        -- Search for depos that have train of type
                        local deposLookupResp = {rednet.lookup("createRailwayDepo-" .. cargo.type)}
                        local depoId = nil
                        for depoIndex, depoId2 in pairs(deposLookupResp) do
                            depoId = depoId2
                            break
                        end

                        if depoId ~= nil then
                            print("Pinging: " .. depoId)
                            rednet.send(
                                depoId,
                                {
                                    ["pickup"] = pickupStationName,
                                    ["dropoff"] = station.getStationName(),
                                    ["cargoType"] = cargo.type,
                                    ["cargoName"] = cargo.name,
                                },
                                "createRailwayDepo-" .. cargo.type
                            )

                            -- wait for reply
                            local depoRespId, depoResp = rednet.receive("createRailwayDepo-" .. cargo.type .. "-resp-" .. depoId, 1)
                            if depoRespId then
                                trainOnWay = true

                                rednet.send(
                                    pickupOutpostId,
                                    "OK",
                                    "createRailwayOutpost-" .. cargo.type .. "-" .. cargo.name
                                )

                                print("Sending train from depo " .. depoId .. " to pick up from " .. pickupOutpostId .. " and deliver [" .. cargo.type .. ", " .. cargo.name .. "]")
                                return
                            else
                                print("Depo " .. depoId .. " timed out")
                            end
                        else
                            print("No depos with matching trains found")
                        end
                    else
                        print("Pickup outpost " .. pickupOutpostId .. " timed out")
                    end
                end
            else
                print(cargo.type .. "-" .. cargo.name .. " not need DO")
            end
        end
    end
end

while true do
    cycle()
    sleep(1)
end