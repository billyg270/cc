local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local util = wRequire("util")
local longSleep = wRequire("longSleep")

local config = tableFile.load("#elevator.lua", nil)

if config == nil then
	config = {
		["speed"] = 1.9,
		["modem"] = "top",
		["reverse"] = "left",
		["defaultDirerction"] = "up",
		["stop"] = "bottom",
		["doubleSpeed"] = "right",
		["floors"] = {
			["Dock"] = {
				["y"] = 64,
				["monitors"] = {
					"monitor_4",
					"monitor_12"
				}
			},
			["Ground Floor"] = {
				["y"] = 96,
				["monitors"] = {
					"monitor_9",
					"monitor_2",
					"monitor_10",
					"monitor_11"
				}
			},
			["1st Floor"] = {
				["y"] = 104,
				["monitors"] = {
					"monitor_3",
					"monitor_6",
					"monitor_7",
					"monitor_8"
				}
			},
			["2nd Floor"] = {
				["y"] = 114,
				["monitors"] = {
					"monitor_5"
				}
			}
		}
	}
	
	tableFile.save(config, "#elevator.lua")
	
	print("A file named #elevator.lua has been created, please update accordingly.")
	return
end

local floor = nil
local highestFloor = nil
local lowestFloor =   nil
local floorsHighToLow = {}

local function setDirection(direction)
	redstone.setOutput(config.reverse, direction ~= config.defaultDirerction)
end

local function setDoubleSpeed(doubleSpeed)
	redstone.setOutput(config.doubleSpeed, doubleSpeed)
end

local function setStop(stop)
	redstone.setOutput(config.stop, stop)
end

local function renderMonitor(monitorName, floorName, elevatorY, moving, fromFloor, toFloor)
	local monitor = peripheral.wrap(monitorName)
	monitor.setTextScale(0.5)
	local width, height = monitor.getSize()
	monitor.setBackgroundColour(colors.gray)
	monitor.clear()
	
	local floorsCount = util.tableSize(config.floors)
	local gapBetweenFloors = math.floor(height / floorsCount)
	
	local fromFloorPy = 0
	local toFloorPy = 0
	local y = 1
	--for floorName2, floorValue in pairs(config.floors) do
	for _, floorName2 in pairs(floorsHighToLow) do
		if floorName == floorName2 and moving == false then
			monitor.setTextColour(colors.orange)
		else
			monitor.setTextColour(colors.cyan)
		end
		
		monitor.setCursorPos(4, y)
		monitor.write(floorName2)
		
		if floorName2 == fromFloor then
			fromFloorPy = y
		end
		
		if floorName2 == toFloor then
			toFloorPy = y
		end
		
		y = y + gapBetweenFloors
	end
	
	monitor.setCursorPos(1, fromFloorPy)
	monitor.setTextColour(colors.white)
	monitor.setBackgroundColour(colors.orange)
	monitor.write("  ")
	
	monitor.setCursorPos(1, toFloorPy)
	monitor.setTextColour(colors.white)
	monitor.setBackgroundColour(colors.green)
	monitor.write("  ")
	
	local hY = math.max(config.floors[fromFloor].y, config.floors[toFloor].y)--config.floors[highestFloor].y
	local lY = math.min(config.floors[fromFloor].y, config.floors[toFloor].y)--config.floors[lowestFloor].y
	
	local highestFloorInTravelPy = math.max(fromFloorPy, toFloorPy)
	local lowestFloorInTravelPy = math.min(fromFloorPy, toFloorPy)
	
	local wayUp01 = (elevatorY - lY) / (hY - lY)
	local wayDown01 = 1 - wayUp01
	--print(wayDown01)
	y = lowestFloorInTravelPy + math.floor(wayDown01 * (highestFloorInTravelPy - lowestFloorInTravelPy))
	--y = highestFloorInTravelPy
	
	if y < lowestFloorInTravelPy then
		y = lowestFloorInTravelPy
	end
	
	if y > highestFloorInTravelPy then
		y = highestFloorInTravelPy
	end
	
	monitor.setBackgroundColour(colors.gray)
	
	if y == fromFloorPy then
		monitor.setBackgroundColour(colors.orange)
	end
	
	if y == toFloorPy then
		monitor.setBackgroundColour(colors.green)
	end
	
	monitor.setCursorPos(1, y)
	monitor.setTextColour(colors.yellow)
	monitor.write("->")
end

local function renderMonitors(y, moving, fromFloor, toFloor)
	for floorName, floorValue in pairs(config.floors) do
		for _, monitorName in pairs(floorValue.monitors) do
			renderMonitor(monitorName, floorName, y, moving, fromFloor, toFloor)
		end
	end
end

local function timeToY(fromY, toY, double)
	if fromY > toY then
		local x = toY
		toY = fromY
		fromY = x
	end
	
	local speed = config.speed
	if double then
		speed = speed * 2
	end
	
	return (toY - fromY) / speed
end

local function gotoFloor(toFloor)
	print("Going to: " .. toFloor)
	
	local direction = "up"
	if config.floors[toFloor].y < config.floors[floor].y then
		direction = "down"
	end
	
	setDirection(direction)
	setDoubleSpeed(false)
	setStop(false)
	
	local time = timeToY(config.floors[floor].y, config.floors[toFloor].y, false)
	print(tostring(time) .. "s")
	
	local function s()
		sleep(time)
	end
	
	local function r()
		local estimatedY = config.floors[floor].y
		local t = time
		while t > 0 do
			
			if(direction == "up") then
				estimatedY = estimatedY + config.speed
			else
				estimatedY = estimatedY - config.speed
			end
			
			--print("Estimated Y: " .. tostring(estimatedY))
			
			renderMonitors(estimatedY, true, floor, toFloor)
			
			t = t - 1
			sleep(1)
		end
		
		renderMonitors(config.floors[toFloor].y, false, floor, toFloor)
	end
	
	parallel.waitForAll(s, r)
	
	setStop(true)
	
	print("Now on: " .. toFloor)
	floor = toFloor
end

for key, value in pairs(config.floors) do
    if highestFloor == nil or value.y > config.floors[highestFloor].y then
		highestFloor = key
	end
	if lowestFloor == nil or value.y < config.floors[lowestFloor].y then
		lowestFloor = key
	end
end

while util.tableSize(floorsHighToLow) < util.tableSize(config.floors) do
	local highestToAdd = nil
	
	for floorName, floorValue in pairs(config.floors) do
		if util.tableContainsValue(floorsHighToLow, floorName) == false and (highestToAdd == nil or floorValue.y > config.floors[highestToAdd].y) then
			highestToAdd = floorName
		end
	end
	
	table.insert(floorsHighToLow, highestToAdd)
end

floor = highestFloor
gotoFloor(lowestFloor)

--gotoFloor("1st Floor")

local function handleInput()
	---@diagnostic disable-next-line: undefined-field
	local event, side, x, y = os.pullEvent("monitor_touch")
	
	
	print("The monitor on side " .. side .. " was touched at (" .. x .. ", " .. y .. ")")
	
	local monitor = peripheral.wrap(side)
	local width, height = monitor.getSize()

	
	local floorsCount = util.tableSize(config.floors)
	local gapBetweenFloors = math.floor(height / floorsCount)
	
	local selectedFloor = nil
	local py = 1
	for _, floorName in pairs(floorsHighToLow) do
		
		if math.abs(y - py) < (gapBetweenFloors / 2) then
			selectedFloor = floorName
			break
		end
		
		py = py + gapBetweenFloors
	end
	
	if selectedFloor == nil then
		print("Coudn't tell which floor was selected")
		return
	end
	
	gotoFloor(selectedFloor)
end

local function startCycle()
	while true do
		handleInput()
	end
end

startCycle()