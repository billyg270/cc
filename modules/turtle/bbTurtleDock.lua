local wRequire = require("wRequire")
local inv = wRequire("invClient")
local bbTurtle = wRequire("bbTurtle")

local module = {
	["modemSide"] = "bottom",
	["chestSide"] = "top",
	["chestNetworkName"] = nil,
	["fuelSlot"] = 16,
	["fuelName"] = "minecraft:charcoal",
	["fuelAmt"] = 8--how much fuel one item gives
}

module.dump = function(startS, endS)
	if startS == nil then
		startS = 1
	end
	if endS == nil then
		endS = 16
	end
	inv.storeAllFromIoChest(module.chestNetworkName, module.chestSide)
	for i = startS, endS do
		turtle.select(i)
		bbTurtle.drop(bbTurtle.sidesToDirection[module.chestSide], 64)
	end
	inv.storeAllFromIoChest(module.chestNetworkName, module.chestSide)
end

module.refuel = function(minAmt)
	if turtle.getItemCount(module.fuelSlot) > 0 then
		error("Slot " .. tostring(module.fuelSlot) .. " is not empty, needed to be empty for refuel")
	end
	while turtle.getFuelLevel() < minAmt do
		local toGet = math.min(
			64,
			math.ceil(
				(minAmt - turtle.getFuelLevel()) / module.fuelAmt
			)
		)
		module.get(module.fuelName, toGet, module.fuelSlot)
		turtle.refuel(64)
	end
end

module.get = function(itemName, amt, slot)
	turtle.select(slot)
	local toGet = amt
	inv.storeAllFromIoChest(module.chestNetworkName, module.chestSide)
	while toGet > 0 do
		toGet = toGet - inv.get(itemName, toGet, module.chestNetworkName, 1)
		if toGet > 0 then
			print("Not got enough " .. itemName .. ", will wait and try again")
			sleep(20)
		end
	end
	bbTurtle.suck(
		bbTurtle.sidesToDirection[module.chestSide],
		64
	)
end

module.doBasicDock = function(minFuel)
	if minFuel == nil then
		minFuel = 500
	end
	rednet.open(module.modemSide)
	module.dump()
	module.refuel(minFuel)
	rednet.close(module.modemSide)
end

return module