local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")

local valuable = {
	["minecraft:diamond_ore"] = 10,
	["minecraft:coal_ore"] = 8,
	["minecraft:iron_ore"] = 7,
	["minecraft:gold_ore"] = 9,
	["minecraft:redstone_ore"] = 1,
	["minecraft:lapis_ore"] = 1,
	["minecraft:emerald_ore"] = 6,
	["minecraft:copper_ore"] = 6,
	["minecraft:deepslate_diamond_ore"] = 10,
	["minecraft:deepslate_coal_ore"] = 8,
	["minecraft:deepslate_iron_ore"] = 7,
	["minecraft:deepslate_gold_ore"] = 9,
	["minecraft:deepslate_redstone_ore"] = 1,
	["minecraft:deepslate_lapis_ore"] = 1,
	["minecraft:deepslate_emerald_ore"] = 6,
	["minecraft:deepslate_copper_ore"] = 6,
	["minecraft:nether_gold_ore"] = 7,
	["minecraft:nether_quartz_ore"] = 4,
	["minecraft:ancient_debris"] = 10,
	["minecraft:crying_obsidian"] = 9,
	["minecraft:obsidian"] = 3,
	["minecraft:glowstone"] = 8,
	["create:copper_ore"] = 1,
	["create:zinc_ore"] = 1,
	["create:deepslate_zinc_ore"] = 1,
	["draconicevolution:overworld_draconium_ore"] = 1,
	["draconicevolution:nether_draconium_ore"] = 1,
	["draconicevolution:end_draconium_ore"] = 1,
	["immersiveengineering:ore_copper"] = 1,
	["immersiveengineering:ore_aluminum"] = 1,
	["immersiveengineering:ore_lead"] = 1,
	["immersiveengineering:ore_silver"] = 1,
	["immersiveengineering:ore_nickel"] = 1,
	["immersiveengineering:ore_uranium"] = 1,
	["quark:biotite_ore"] = 1,
	["thermal:apatite_ore"] = 1,
	["thermal:cinnabar_ore"] = 1,
	["thermal:niter_ore"] = 1,
	["thermal:sulfur_ore"] = 1,
	["thermal:copper_ore"] = 1,
	["thermal:tin_ore"] = 1,
	["thermal:lead_ore"] = 1,
	["thermal:silver_ore"] = 1,
	["thermal:nickel_ore"] = 1,
}

print("If have, place geo scanner in slot 15")

print("tag:")
local tag = read()
print("\n")

if tag == "" or tag == nil then
	tag = "#" .. tostring(os.getComputerID())
end

local hasGeoScanner = turtle.getItemCount(15) == 1
local facing = nil

local lastOreSlot = 15
if hasGeoScanner then
	print("facing (n/e/s/w):")
	facing = read()
	print("\n")
	lastOreSlot = 14
end

local hookSuccess, hook = DiscordHook.createWebhook("https://discord.com/api/webhooks/889783581839339541/mtTZmSBCoa-KGi2DN5S5_tQMPJhpPjOjDB3dy8EtY-Dxu2CK9pTeBwMy7IkKMGyVpkti")
if not hookSuccess then
	error("Webhook connection failed! Reason: " .. hook)
end

local distanceOut = 0

local function checkBlock(dir)
	local hasBlock, blockData = bbTurtle.inspect(dir)
	if hasBlock and valuable[blockData.name] ~= nil and valuable[blockData.name] > 0 then
		hook.send("Mined: " .. blockData.name, tag)
		bbTurtle.dig(dir)
	end
end

local function scan()
    local geo = peripheral.find("geoScanner")
    local s, r = nil, nil
    while s == nil do
        s, r = geo.scan(8)
        if r ~= nil then
            print(r)
        end
        sleep(2)
    end
    --print(textutils.serialize(s))
    return s
end

local function scanToRel(blocks)
	if facing == "s" then
		return blocks
	end
	for k, v in pairs(blocks) do
		if facing == "n" then
			blocks[k].x = -blocks[k].x
			blocks[k].z = -blocks[k].z
		end
		if facing == "e" then
			local tmp = blocks[k].x
			blocks[k].x = blocks[k].z
			blocks[k].z = tmp
		end
		if facing == "w" then
			local tmp = blocks[k].x
			blocks[k].x = -blocks[k].z
			blocks[k].z = -tmp
		end
	end
	return blocks
end

local function trimScanned(blocks)
	for k, v in pairs(blocks) do
		if valuable[blocks[k].name] == nil or valuable[blocks[k].name] <= 0 then
			blocks[k] = nil
		end
	end
	return blocks
end

local function gotoRel(x, y, z)
	if x < 0 then
		turtle.turnLeft()
	elseif x > 0 then
		turtle.turnRight()
	end
	for i = 1, math.abs(x) do
		turtle.select(1)
		checkBlock("f")
		turtle.select(16)
		bbTurtle.rForward()
		turtle.drop(64)
	end
	if x < 0 then
		turtle.turnRight()
	elseif x > 0 then
		turtle.turnLeft()
	end
	
	for i = 1, math.abs(y) do
		if y > 0 then
			turtle.select(1)
			checkBlock("u")
			turtle.select(16)
			bbTurtle.rUp()
			turtle.drop(64)
		else
			turtle.select(1)
			checkBlock("d")
			turtle.select(16)
			bbTurtle.rDown()
			turtle.drop(64)
		end
	end
	
	if z < 0 then
		turtle.turnLeft()
		turtle.turnLeft()
	end
	
	for i = 1, math.abs(z) do
		turtle.select(1)
		checkBlock("f")
		turtle.select(16)
		bbTurtle.rForward()
		turtle.drop(64)
	end
	
	if z < 0 then
		turtle.turnLeft()
		turtle.turnLeft()
	end
end

local function step()
	turtle.select(1)
	if hasGeoScanner then
		if distanceOut % 8 == 0 then
			turtle.select(15)
			turtle.equipLeft()
			local blocks = scanToRel(trimScanned(scan()))
			turtle.equipLeft()
			local ox, oy, oz = 0, 0, 0
			for k, v in pairs(blocks) do
				print("going to: " .. v.name)
				--[[
				print(v.x)
				print(v.y)
				print(v.z)
				print("---")
				print(ox)
				print(oy)
				print(oz)
				print("---")
				print(v.x - ox)
				print(v.y - oy)
				print(v.z - oz)
				print("---")
				read()
				]]--
				gotoRel(v.x - ox, v.y - oy, v.z - oz)
				ox = v.x
				oy = v.y
				oz = v.z
			end
			--[[
			print("===")
			print(ox)
			print(oy)
			print(oz)
			print("===")
			read()
			]]--
			gotoRel(-ox, -oy, -oz)
		end
	else
		checkBlock("u")
		checkBlock("d")
		checkBlock("f")
		turtle.turnLeft()
		checkBlock("f")
		turtle.turnLeft()
		turtle.turnLeft()
		checkBlock("f")
		turtle.turnLeft()
	end
	turtle.select(16)
	bbTurtle.rForward(true)
end

while distanceOut < (turtle.getFuelLevel() - 100) and turtle.getItemCount(lastOreSlot) == 0 do
	step()
	distanceOut = distanceOut + 1
end

hook.send("Turning back", tag)
turtle.turnLeft()
turtle.turnLeft()

for i = 1, distanceOut do
	bbTurtle.rForward()
end

hook.send("Back <@198768745278078976>", tag)