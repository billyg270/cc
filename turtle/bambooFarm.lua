local wRequire = require("wRequire")
local DiscordHook = wRequire("DiscordHook")
local bbTurtle = wRequire("bbTurtle")
local farmModule = wRequire("farm")
local tableFile = wRequire("tableFile")
local longSleep = wRequire("longSleep")

local config = tableFile.load("#bambooFarm.lua", nil)

if config == nil then
	config = {}
	print("Width:")
	config.width = tonumber(read())
	print("Depth:")
	config.depth = tonumber(read())
	print("Sleep Time:")
	config.sleepTime = tonumber(read())
end

if config.sleepTime == nil then
	config.sleepTime = 10
end

tableFile.save(config, "#bambooFarm.lua")

local function checkFuel()
	local minFuel = config.width * config.depth * 2
	while turtle.getFuelLevel() < minFuel do
		print("Refueling")
		turtle.turnRight()
		turtle.select(16)
		turtle.suck(16)
		turtle.refuel(64)
		turtle.turnLeft()
		print("Fuel level: " .. turtle.getFuelLevel() .. "/" .. minFuel)
	end
end

local function step()
	turtle.dig()
	turtle.suckUp(64)
	turtle.suckDown(64)
	sleep(2)
	bbTurtle.rForward()
	turtle.suckUp(64)
	turtle.suckDown(64)
end

local function farm()
	print("Farming")
	turtle.select(1)
	farmModule.farm(config.width, config.depth, step)
end

local function dumpItems()
	print("Dumping items")
	for i = 1, 16 do
		turtle.select(i)
		turtle.dropDown(64)
	end
end

local function cycle()
	checkFuel()
	farm()
	dumpItems()
end

while true do
	cycle()
	longSleep(config.sleepTime)
end