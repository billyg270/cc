{
  [ "main_signal_+2_E_station" ] = {
    x = 10,
    computerID = 32,
    z = -4,
    signalsFromThis = {
      [ "main_signal_+2.5_E" ] = {
        trackSpine = {
          {
            x = 11.5,
            z = -2,
          },
        },
        redstoneRouting = {},
      },
    },
    destinationOnly = true,
  },
  [ "im_signal_+4_S" ] = {
    x = 23,
    computerID = 40,
    z = 15,
    signalsFromThis = {
      [ "im_signal_+3_N" ] = {
        trackSpine = {
          {
            x = 23,
            z = 17,
          },
          {
            x = 21,
            z = 17,
          },
        },
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "im_signal_+3_N" ] = {
    x = 21,
    computerID = 39,
    z = 12,
    signalsFromThis = {
      [ "im_signal_+2_N" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_-2_W" ] = {
    x = -10.5,
    computerID = 9,
    z = 2,
    signalsFromThis = {
      [ "main_signal_-3_W" ] = {
        trackSpine = {},
        redstoneRouting = {
          front = false,
        },
      },
      [ "bbb_signal_-1_N" ] = {
        trackSpine = {
          {
            x = -14.5,
            z = 2,
          },
        },
        redstoneRouting = {
          front = true,
        },
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-1_S" ] = {
    x = -12,
    computerID = 22,
    z = -4,
    signalsFromThis = {
      [ "main_signal_-2_E" ] = {
        trackSpine = {
			{x = -12, z = -2}
		},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+2_E" ] = {
    x = 10,
    computerID = 33,
    z = -2,
    signalsFromThis = {
      [ "main_signal_+2.5_E" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+2.5_E" ] = {
    x = 13.5,
    computerID = 41,
    z = -2,
    signalsFromThis = {
      [ "main_signal_+3_E" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-3_S" ] = {
    x = -12,
    computerID = 27,
    z = -12,
    signalsFromThis = {
      [ "bbb_signal_-2_S" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "im_signal_+2_N" ] = {
    x = 21,
    computerID = 36,
    z = 9,
    signalsFromThis = {
      [ "im_signal_+1_N" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "im_signal_+3_S" ] = {
    x = 23,
    computerID = 38,
    z = 12,
    signalsFromThis = {
      [ "im_signal_+4_S" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "im_signal_+2_S" ] = {
    x = 23,
    computerID = 37,
    z = 9,
    signalsFromThis = {
      [ "im_signal_+3_S" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+1_W" ] = {
    x = 5,
    computerID = 17,
    z = 2,
    signalsFromThis = {
      [ "main_signal_-1_W" ] = {
        trackSpine = {},
        redstoneRouting = {
          front = false,
          bottom = false,
        },
      },
      [ "main_signal_+1_E_station" ] = {
        trackSpine = {
          {
            x = 2.5,
            z = 2,
          },
          {
            x = 2.5,
            z = -2,
          },
          {
            x = 3.5,
            z = -2,
          },
        },
        redstoneRouting = {
          front = true,
          bottom = true,
        },
      },
      [ "main_signal_+1_E" ] = {
        trackSpine = {
          {
            x = 2.5,
            z = 2,
          },
          {
            x = 2.5,
            z = -2,
          },
        },
        redstoneRouting = {
          front = true,
          bottom = false,
        },
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-1_N" ] = {
    x = -14.5,
    computerID = 23,
    z = -4,
    signalsFromThis = {
      [ "bbb_signal_-2_N" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_-1_W" ] = {
    x = -5,
    computerID = 7,
    z = 2,
    signalsFromThis = {
      [ "depo_signal_+1_S" ] = {
        trackSpine = {
          {
            x = -6.5,
            z = 2,
          },
        },
        redstoneRouting = {
          front = true,
        },
      },
      [ "main_signal_-2_W" ] = {
        trackSpine = {},
        redstoneRouting = {
          front = false,
        },
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-2_N" ] = {
    x = -14.5,
    computerID = 24,
    z = -8,
    signalsFromThis = {
      [ "bbb_signal_-3_N" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_-3_W" ] = {
    x = -16,
    computerID = 20,
    z = 2,
    signalsFromThis = {
      [ "main_signal_-3_E" ] = {
        trackSpine = {
          {
            x = -20,
            z = 2,
          },
          {
            x = -20,
            z = -2,
          },
        },
        redstoneRouting = {
          front = false,
        },
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+1_E" ] = {
    x = 5,
    computerID = 16,
    z = -2,
    signalsFromThis = {
      [ "main_signal_+2_E" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_-2_E" ] = {
    x = -10.5,
    computerID = 10,
    z = -2,
    signalsFromThis = {
      [ "main_signal_-1_E" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+1_W_station" ] = {
    x = 5,
    computerID = 18,
    z = 4,
    signalsFromThis = {
      [ "main_signal_-1_W" ] = {
        trackSpine = {
          {
            x = 3.5,
            z = 2,
          },
        },
        redstoneRouting = {
          front = false,
          bottom = false,
        },
      },
      [ "main_signal_+1_E_station" ] = {
        trackSpine = {
          {
            x = 3.5,
            z = 2,
          },
          {
            x = 2.5,
            z = 2,
          },
          {
            x = 2.5,
            z = -2,
          },
          {
            x = 3.5,
            z = -2,
          },
        },
        redstoneRouting = {
          front = false,
          bottom = true,
        },
      },
      [ "main_signal_+1_E" ] = {
        trackSpine = {
          {
            x = 3.5,
            z = 2,
          },
          {
            x = 2.5,
            z = 2,
          },
          {
            x = 2.5,
            z = -2,
          },
        },
        redstoneRouting = {
          front = true,
          bottom = false,
        },
      },
    },
    destinationOnly = true,
  },
  [ "depo_signal_+1_N" ] = {
    x = -9,
    computerID = 8,
    z = 4,
    signalsFromThis = {
      [ "main_signal_-1_E" ] = {
        trackSpine = {
          {
            x = -9,
            z = -2,
          },
        },
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "im_signal_+1_S" ] = {
    x = 23,
    computerID = 35,
    z = 6,
    signalsFromThis = {
      [ "im_signal_+2_S" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+1_E_station" ] = {
    x = 5,
    computerID = 14,
    z = -4,
    signalsFromThis = {
      [ "main_signal_+2_E_station" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-4_N" ] = {
    x = -14.5,
    computerID = 28,
    z = -16,
    signalsFromThis = {
      [ "bbb_signal_-3_S" ] = {
        trackSpine = {
          {
            x = -14.5,
            z = -19,
          },
          {
            x = -12,
            z = -19,
          },
        },
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+2_W" ] = {
    x = 10,
    computerID = 31,
    z = 2,
    signalsFromThis = {
      [ "main_signal_+1_W" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_-1_E" ] = {
    x = -5,
    computerID = 11,
    z = -2,
    signalsFromThis = {
      [ "main_signal_-1_W" ] = {
        trackSpine = {
          {
            x = -2.5,
            z = -2,
          },
          {
            x = -2.5,
            z = 2,
          },
        },
        redstoneRouting = {
          front = true,
          bottom = false,
        },
      },
      [ "main_signal_+1_E_station" ] = {
        trackSpine = {
          {
            x = 3.5,
            z = -2,
          },
        },
        redstoneRouting = {
          front = false,
          bottom = true,
        },
      },
      [ "main_signal_+1_E" ] = {
        trackSpine = {},
        redstoneRouting = {
          front = false,
          bottom = false,
        },
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-2_S" ] = {
    x = -12,
    computerID = 25,
    z = -8,
    signalsFromThis = {
      [ "bbb_signal_-1_S" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_-3_E" ] = {
    x = -16,
    computerID = 21,
    z = -2,
    signalsFromThis = {
      [ "main_signal_-2_E" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+3_W" ] = {
    x = 15,
    computerID = 30,
    z = 2,
    signalsFromThis = {
      [ "main_signal_+2.5_W" ] = {
        trackSpine = {},
        redstoneRouting = {
        },
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+2.5_W" ] = {
    x = 13.5,
    computerID = 42,
    z = 2,
    signalsFromThis = {
      [ "main_signal_+2_W" ] = {
        trackSpine = {},
        redstoneRouting = {
          front = false,
        },
      },
      [ "main_signal_+2_W_station" ] = {
        trackSpine = {
          {
            x = 11.5,
            z = 2,
          },
        },
        redstoneRouting = {
          front = true,
        },
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+3_E" ] = {
    x = 15,
    computerID = 29,
    z = -2,
    signalsFromThis = {
      [ "main_signal_+3_W" ] = {
        trackSpine = {
          {
            x = 17,
            z = -2,
          },
          {
            x = 17,
            z = 2,
          },
        },
        redstoneRouting = {
          front = true,
        },
      },
      [ "im_signal_+1_S" ] = {
        trackSpine = {
          {
            x = 23,
            z = -2,
          },
        },
        redstoneRouting = {
          front = false,
        },
      },
    },
    destinationOnly = false,
  },
  [ "im_signal_+1_N" ] = {
    x = 21,
    computerID = 34,
    z = 6,
    signalsFromThis = {
      [ "main_signal_+3_W" ] = {
        trackSpine = {
          {
            x = 21,
            z = 2,
          },
        },
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "depo_signal_+1_S" ] = {
    x = -6.5,
    computerID = 5,
    z = 4,
    signalsFromThis = {
      [ "depo_signal_+1_N" ] = {
        trackSpine = {
          {
            x = -6.5,
            z = 6,
          },
          {
            x = -9,
            z = 6,
          },
        },
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "main_signal_+2_W_station" ] = {
    x = 10,
    computerID = 19,
    z = 4,
    signalsFromThis = {
      [ "main_signal_+1_W_station" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
  [ "bbb_signal_-3_N" ] = {
    x = -14.5,
    computerID = 26,
    z = -12,
    signalsFromThis = {
      [ "bbb_signal_-4_N" ] = {
        trackSpine = {},
        redstoneRouting = {},
      },
    },
    destinationOnly = false,
  },
}