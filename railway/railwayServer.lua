local wRequire = require("wRequire")
local tableFile = wRequire("tableFile")
local util = wRequire("util")

local signals = tableFile.load("#railwayServerSignals.lua", {})
local blocks = tableFile.load("#railwayServerBlocks.lua", {})
local trains = tableFile.load("#railwayServerTrains.lua", {})

local networkQueue = {}
rednet.open("back")

local blockMonitor = peripheral.wrap("left")
local signalMonitor = peripheral.wrap("monitor_0")

--[[
	tableFile.save(signals, "#railwayServerSignals.lua")
	tableFile.save(blocks, "#railwayServerBlocks.lua")
	tableFile.save(trains, "#railwayServerTrains.lua")

	Signal (key is signalID):
		x - int
		z - int
		signalsFromThis - List Of:
			signalID (key) {
				["trackSpine"] = {} -- List of x and z coords for display drawing to next signal
				["redstoneRouting"] = {
					["redstone side of peripheral"] = true of false for on or off
				}
			}
		destinationOnly - boolean
		computerID - int
	
	Block (key is blockID):
		trainID - string or nil
		signalsToThis - List Of Signal IDs pointing into this block
	
	Train (key is trainID):
		type - "passenger", "bbQuarry"
		path - List of signal IDs
		looping - boolean
		lastPastSignal - signal ID
]]

local _lockAllSignals = false

local function writeAllBlocksToMonitor()
	if blockMonitor == nil then
		return
	end
	
	blockMonitor.clear()
	blockMonitor.setTextScale(0.5)
	
	local line = 0
	for blockID, block in pairs(blocks) do
		line = line + 1
		
		if line % 2 == 0 then
			blockMonitor.setBackgroundColour(colors.gray)
		else
			blockMonitor.setBackgroundColour(colors.black)
		end
		
		blockMonitor.setCursorPos(1, line)
		blockMonitor.write(string.rep(" ", blockMonitor.getSize()))
		
		blockMonitor.setCursorPos(1, line)
		blockMonitor.write(blockID)
		if block.trainID ~= nil then
			blockMonitor.setCursorPos(40, line)
			blockMonitor.write(block.trainID)
		end
	end
end

local signalDetectorRailStates = {}

local function writeAllSignalsToMonitor()
	if signalMonitor == nil then
		return
	end
	
	signalMonitor.clear()
	signalMonitor.setTextScale(0.5)
	
	local line = 0
	for signalID, detectorRailCount in pairs(signalDetectorRailStates) do
		line = line + 1
		
		if line % 2 == 0 then
			signalMonitor.setBackgroundColour(colors.gray)
		else
			signalMonitor.setBackgroundColour(colors.black)
		end
		
		signalMonitor.setCursorPos(1, line)
		signalMonitor.write(string.rep(" ", signalMonitor.getSize()))
		
		signalMonitor.setCursorPos(1, line)
		signalMonitor.write(signalID)
		signalMonitor.setCursorPos(40, line)
		signalMonitor.write(detectorRailCount)
	end
end

local function getBlockIDSignalPointsInto(signalID)
	for blockID, block in pairs(blocks) do
		if util.tableContainsValue(block.signalsToThis, signalID) then
			return blockID
		end
	end
	
	error("Count not find which block signal " .. signalID .. " points into")
end

local function getBlockIDSignalPointsOutOf(signalID)
	for signalID2, signal2 in pairs(signals) do
		if signal2.signalsFromThis[signalID] ~= nil then
			return getBlockIDSignalPointsInto(signalID2)
		end
	end
	
	error("Count not find which block signal " .. signalID .. " points out of")
end

local function getSignalsWithTrainsInThatArePointingIntoBlock(blockID)
	local otherSignalsWithTrainsInThatArePointingIntoThisBlock = {}
	for signalIndexPointingIntoNextBlock, signalIDPointingIntoNextBlock in pairs(blocks[blockID].signalsToThis) do
		local blockIDBeforeThisSignal = getBlockIDSignalPointsOutOf(signalIDPointingIntoNextBlock)
		if blocks[blockIDBeforeThisSignal].trainID ~= nil then
			local trainsNextSignalID = trains[blocks[blockIDBeforeThisSignal].trainID].path[1]
			if util.tableContainsValue(blocks[blockID].signalsToThis, trainsNextSignalID) then
				--table.insert(otherBlocksWithTrainsInThatArePointingIntoThisBlock, blockIDBeforeThisSignal)
				table.insert(otherSignalsWithTrainsInThatArePointingIntoThisBlock, trainsNextSignalID)
			end
		end
	end
	
	return otherSignalsWithTrainsInThatArePointingIntoThisBlock
end

local signalPriorityScores = {}

local function calculateSignalPriorityScore(signalID)
	local score = 0
	
	for signalID2, signal2 in pairs(signals) do
		score = score + 1
		if signalID == signalID2 then
			break
		end
	end
	
	if string.find(signalID, "station") or signalID == "main_signal_+3_E" then
		score = score - 1000
	end
	
	if string.find(signalID, "main") then
		score = score + 500
	end
	
	return score
end

for signalID, signal in pairs(signals) do
	signalPriorityScores[signalID] = calculateSignalPriorityScore(signalID)
	for signalID2, signal2 in pairs(signals) do
		if signalID ~= signalID2 and signalPriorityScores[signalID] == signalPriorityScores[signalID2] then
			error("Signal " .. signalID .. " has same priority as " .. signalID2 .. " (" .. signalPriorityScores[signalID] .. ")")
		end
	end
end

local function isSignalAtTopOfPriorityList(listOfSignals, signalID)
	for signalIndex2, signalID2 in pairs(listOfSignals) do
		if signalPriorityScores[signalID2] > signalPriorityScores[signalID] then
			return false
		end
	end
	
	return true
end

local function calcualteSignalState(signalID)--Returns signalDisplayState, signalPowerState, signalRoutingRedstoneSignals
	if signals[signalID] == nil then
		error("Could not find signal with ID: " .. signalID)
	end
	
	if _lockAllSignals then
		return false, false, {}
	end
	
	local blockToID = getBlockIDSignalPointsInto(signalID)
	local blockFromID = getBlockIDSignalPointsOutOf(signalID)
	--print(signalID .. " goes into block " .. blockToID)
	--print(blockFromID .. " goes into signal " .. signalID)
	
	if blocks[blockToID].trainID == nil then-- No train in next block
		if blocks[blockFromID].trainID == nil then-- And no train in previous block
			return false, false, {}
		end
		
		local otherSignalsWithTrainsInThatArePointingIntoThisBlock = getSignalsWithTrainsInThatArePointingIntoBlock(blockToID)
		if util.tableSize(otherSignalsWithTrainsInThatArePointingIntoThisBlock) > 1 then
			if isSignalAtTopOfPriorityList(otherSignalsWithTrainsInThatArePointingIntoThisBlock, signalID) == false then
				return false, false, {}--This signal has lower priority then others in this list
			end
		end
		
		
		-- Train in previous block
		
		if trains[blocks[blockFromID].trainID] == nil then
			error("Cannot find train with ID: " .. blocks[blockFromID].trainID)
		end
		
		local trainPathLength = util.tableSize(trains[blocks[blockFromID].trainID].path)
		
		local signalPowerState = (trainPathLength > 1 and trains[blocks[blockFromID].trainID].path[1] == signalID) or (trainPathLength == 1 and trains[blocks[blockFromID].trainID].path[1] ~= signalID)
		local signalRoutingRedstoneSignals = {}
		
		-- Commented out for now so points are only applied when the train is in the same block the points are
		-- if trainPathLength > 0 and signals[signalID].signalsFromThis[trains[blocks[blockFromID].trainID].path[1]] ~= nil then
		-- 	signalRoutingRedstoneSignals = signals[signalID].signalsFromThis[trains[blocks[blockFromID].trainID].path[1]].redstoneRouting
		-- elseif trainPathLength > 1 and signals[signalID].signalsFromThis[trains[blocks[blockFromID].trainID].path[2]] ~= nil then
		-- 	signalRoutingRedstoneSignals = signals[signalID].signalsFromThis[trains[blocks[blockFromID].trainID].path[2]].redstoneRouting
		-- end
		
		return signalPowerState, signalPowerState, signalRoutingRedstoneSignals
	else
		-- Train in next block
		
		local trainPathLength = util.tableSize(trains[blocks[blockToID].trainID].path)
		
		local signalRoutingRedstoneSignals = {}
		
		if trains[blocks[blockToID].trainID].lastPastSignal == signalID and trainPathLength > 0 and signals[signalID].signalsFromThis[trains[blocks[blockToID].trainID].path[1]] ~= nil  then
			signalRoutingRedstoneSignals = signals[signalID].signalsFromThis[trains[blocks[blockToID].trainID].path[1]].redstoneRouting
		end
		
		return false, false, signalRoutingRedstoneSignals
	end
end

local function setSignalState(signalID, signalDisplayState, signalPowerState, signalRoutingRedstoneSignals, dontSave)-- States are either true or false, for green and red respectivly, dontSave is used when bulk updating signals
	if signals[signalID] == nil then
		error("Could not find signal with ID: " .. signalID)
	end
	
	if signalPowerState ~= true and signalPowerState ~= false then
		signalPowerState = signalDisplayState
	end
	
	if signalRoutingRedstoneSignals == nil then
		signalRoutingRedstoneSignals = {}
	end
	
	if dontSave ~= true then
		tableFile.save(signals, "#railwayServerSignals.lua")
	end
	
	--print("Sending signal state:")
	--print("Signal ID:" .. signalID)
	--print("Display State:" .. tostring(signalDisplayState))
	--print("Power State:" .. tostring(signalPowerState))
	
	--print("Sending update to Signal:" .. signalID )
	--print("Display: " .. tostring(signalDisplayState))
	--print("Power: " .. tostring(signalPowerState))
	
	--signals[signalID].signalsFromThis[]
	
	if signals[signalID].computerID > -1 then
		rednet.send(signals[signalID].computerID, {--Broadcast clogged up the network
			["type"] = "railwaySignalState",
			["signalID"] = signalID,
			["signalDisplayState"] = signalDisplayState,
			["signalPowerState"] = signalPowerState,
			["signalRoutingRedstoneSignals"] = signalRoutingRedstoneSignals
		}, "railwaySignal")
	end
	
	rednet.broadcast(
		{
			["type"] = "railwaySignalState",
			["signalID"] = signalID,
			["signalDisplayState"] = signalDisplayState
		},
		"railwayDisplay"
	)
end

local function calculateAndSetSignalState(signalID, dontSave)-- dontSave is used when bulk updating signals
	local signalDisplayState, signalPowerState, signalRoutingRedstoneSignals = calcualteSignalState(signalID)
	setSignalState(signalID, signalDisplayState, signalPowerState, signalRoutingRedstoneSignals, dontSave)
end

local function calculateAndSetSignalStatesPointingIntoBlock(blockID, andOutOf)
	if blocks[blockID] == nil then
		error("Could not find block with ID " .. blockID)
	end
	
	--print("calculateAndSetSignalStatesPointingIntoBlock " .. blockID .. " " .. tostring(andOutOf == true))
	
	for signalsToThisIndex, signalsToThisID in pairs(blocks[blockID].signalsToThis) do
		calculateAndSetSignalState(signalsToThisID, true)
		
		if andOutOf == true then
			for signalFromInSignalID, _ in pairs(signals[signalsToThisID].signalsFromThis) do
				--print("AAAA " .. signalFromInSignalID)
				calculateAndSetSignalState(signalFromInSignalID, true)
			end
		end
	end
	
	tableFile.save(signals, "#railwayServerSignals.lua")
end

local function calculateAndSetAllSignals()
	for signalID, signal in pairs(signals) do
		calculateAndSetSignalState(signalID, true)
	end
	
	tableFile.save(signals, "#railwayServerSignals.lua")
end

local function onTrainPassSignalStart(signalID)-- When the detector rail is activated
	-- Lock all other signals in block train is going into
	
	local blockToID = getBlockIDSignalPointsInto(signalID)
	local blockFromID = getBlockIDSignalPointsOutOf(signalID)
	
	if blocks[blockFromID].trainID == nil then
		error("[" .. signalID .. "] Block (" .. blockFromID ..  ") train came from diddnt have a train in it")
	end
	
	if blocks[blockToID].trainID ~= nil then
		error("[" .. signalID .. "] Block (" .. blockToID ..  ") train is going into already has train " .. blocks[blockToID].trainID .. " in it")
	end
	
	blocks[blockToID].trainID = blocks[blockFromID].trainID
	print("Train " .. blocks[blockFromID].trainID .. " has started to enter block " .. blockToID .. " from " .. blockFromID)
	tableFile.save(blocks, "#railwayServerBlocks.lua")
	
	if util.tableSize(trains[blocks[blockFromID].trainID].path) > 0 then
		if trains[blocks[blockFromID].trainID].path[1] ~= signalID then
			-- Routing error could cause this
			error("Train was expected to pass signal " .. tostring(trains[blocks[blockFromID].trainID].path[1]) .. " but instead passed signal " .. tostring(signalID))
		end
		local removed = table.remove(trains[blocks[blockFromID].trainID].path, 1)
		if trains[blocks[blockFromID].trainID].looping then
			table.insert(trains[blocks[blockFromID].trainID].path, removed)
		end
		
		rednet.broadcast(
			{
				["type"] = "trainPosition",
				["signalIdFrom"] = signalID,
				["signalIdTo"] = trains[blocks[blockFromID].trainID].path[1]
			},
			"railwayDisplay"
		)
	end
	
	--print("Train path: " ..textutils.serialize(trains[blocks[blockFromID].trainID].path))
	
	trains[blocks[blockFromID].trainID].lastPastSignal = signalID
	
	tableFile.save(trains, "#railwayServerTrains.lua")
	
	calculateAndSetSignalStatesPointingIntoBlock(blockToID)
	
	if util.tableSize(trains[blocks[blockFromID].trainID].path) > 0 then
		calculateAndSetSignalStatesPointingIntoBlock(
			getBlockIDSignalPointsInto(trains[blocks[blockFromID].trainID].path[1])
		)
	end
	
	writeAllBlocksToMonitor()
	
	if signalDetectorRailStates[signalID] == nil then
		signalDetectorRailStates[signalID] = 0
	end
	
	signalDetectorRailStates[signalID] = signalDetectorRailStates[signalID] + 1
	writeAllSignalsToMonitor()
end

local function onTrainPassSignalEnd(signalID)-- When the detector rail is deactivated
	local blockFromID = getBlockIDSignalPointsOutOf(signalID)
	local blockToID = getBlockIDSignalPointsInto(signalID)
	
	if blocks[blockFromID].trainID == nil then
		error("Train is leaving block that was already empty")
	end
	
	print("Block " .. blockFromID .. " is empty as " .. blocks[blockFromID].trainID .. " has left")
	blocks[blockFromID].trainID = nil
	tableFile.save(blocks, "#railwayServerBlocks.lua")
	
	calculateAndSetSignalStatesPointingIntoBlock(blockFromID)
	calculateAndSetSignalStatesPointingIntoBlock(blockToID, true)
	
	writeAllBlocksToMonitor()
	
	if signalDetectorRailStates[signalID] == nil then
		signalDetectorRailStates[signalID] = 0
	end
	
	signalDetectorRailStates[signalID] = signalDetectorRailStates[signalID] - 1
	writeAllSignalsToMonitor()
end

local function lockAllSignals()
	print("Locking all signals")
	_lockAllSignals = true
	calculateAndSetAllSignals()
end

local function unlockAllSignals()
	print("Unlocking all signals")
	_lockAllSignals = false
	calculateAndSetAllSignals()
end

local function configureSignal(signalID, x, z, signalsFromThis, destinationOnly)-- Called when configuring signals manually for network setup
	if signalID == nil then
		error("Cannot configure signal as ID is nil")
	end
	
	if signals[signalID] == nil then
		signals[signalID] = {
			["x"] = 0,
			["z"] = 0,
			["signalsFromThis"] = {},
			["destinationOnly"] = false
		}
	end
	
	if x ~= nil then
		signals[signalID].x = x
	end
	
	if z ~= nil then
		signals[signalID].z = z
	end
	
	if signalsFromThis ~= nil then
		signals[signalID].signalsFromThis = signalsFromThis
	end
	
	if destinationOnly ~= nil then
		signals[signalID].destinationOnly = destinationOnly
	end
	
	signals[signalID].destinationOnly = destinationOnly
	
	calculateAndSetSignalState(signalID)-- Will save to file also
end

local function getAllDisplayData()
	local displaySignals = {}
	
	for signalID, signal in pairs(signals) do
		local signalDisplayState, signalPowerState, signalRoutingRedstoneSignals = calcualteSignalState(signalID)
		
		displaySignals[signalID] = {
			x = signal.x,
			z = signal.z,
			connectsTo = {},
			initialState = signalDisplayState
		}
		
		for signalFromThisID, signalFromThis in pairs(signals[signalID].signalsFromThis) do
			displaySignals[signalID].connectsTo[signalFromThisID] = {
				["trackSpine"] = signalFromThis.trackSpine
			}
		end
	end
	
	local displayTrains = {}
	
	--for trainID, train in pairs()
	
	return {
		["type"] = "railwayDisplayAllData",
		["signals"] = displaySignals,
	}
end

local function processNetworkMessage(computerIdFrom, message)
	if type(message) ~= "table" then
		print("Won't process network message as not a table")
		return
	end
	
	if message.type == nil then
		print("Won't process network message as 'type' key not set in table")
		return
	end
	
	if message.type == "railwayRequestSignalState" then
		if message.signalID == nil then
			print("Won't process railwayRequestSignalState message as 'signalID' key not set in table")
			return
		end
		
		if signals[message.signalID].computerID ~= computerIdFrom then
			print("Signal " .. message.signalID .. "'s computer ID has been updated from " .. signals[message.signalID].computerID .. " to " .. computerIdFrom)
			signals[message.signalID].computerID = computerIdFrom
			tableFile.save(signals, "#railwayServerSignals.lua")
		end
		
		calculateAndSetSignalState(message.signalID)
		
		return
	end
	
	if message.type == "railwaySignalDetectorRail" then
		if message.signalID == nil then
			print("Won't process railwaySignalDetectorRail message as 'signalID' key not set in table")
			return
		end
		
		if message.powered == nil then
			print("Won't process railwaySignalDetectorRail message as 'powered' key not set in table")
			return
		end
		
		if message.powered then
			onTrainPassSignalStart(message.signalID)
		else
			onTrainPassSignalEnd(message.signalID)
		end
		
		return
	end
	
	if message.type == "railwayConfigureSignal" then
		if message.signalID == nil then
			print("Won't process railwayConfigureSignal message as 'signalID' key not set in table")
			return
		end
		
		configureSignal(
			message.signalID,
			message.x,
			message.z,
			message.signalsFromThis,
			message.destinationOnly
		)
		
		print("Configured signal " .. message.signalID)
		return
	end
	
	if message.type == "railwayConfigureSignalAddSignalFromThis" then
		if message.signalID == nil then
			print("Won't process railwayConfigureSignalAddSignalFromThis message as 'signalID' key not set in table")
			return
		end
		
		if message.pointsToSignalID == nil then
			print("Won't process railwayConfigureSignalAddSignalFromThis message as 'pointsToSignalID' key not set in table")
			return
		end
		
		if signals[message.signalID] == nil then
			print("Won't process railwayConfigureSignalAddSignalFromThis message as signalID not found in existing signals")
			return
		end
		
		table.insert(signals[message.signalID].signalsFromThis, message.pointsToSignalID)
		
		print("Added " .. message.pointsToSignalID .. " as one of the signals " .. message.signalID .. " points to")
		return
	end
	
	if message.type == "railwayDisplayRequestAllData" then
		print("Sending display computer: " .. computerIdFrom .. " track data")
		rednet.send(computerIdFrom, getAllDisplayData(), "railwayDisplay")
		return
	end
	
	if message.type == "railwayDispatchSetTrainRoute" then
		if message.trainID == nil then
			print("Won't process railwayDispatchSetTrainRoute message as 'trainID' key not set in table")
			return
		end
		
		if trains[message.trainID] == nil then
			print("Can't find train with ID " .. message.trainID .. " when running railwayDispatchSetTrainRoute")
			return
		end
		
		if message.path == nil then
			print("Won't process railwayDispatchSetTrainRoute message as 'path' key not set in table")
			return
		end
		
		if #message.path == 0 then
			print("Won't process railwayDispatchSetTrainRoute message as 'path' is a table instead of a list")
			return
		end
		
		if util.tableSize(message.path) == 0 then
			print("Won't process railwayDispatchSetTrainRoute message as 'path' cannot be an empty list")
			return
		end
		
		if message.looping == nil then
			print("Won't process railwayDispatchSetTrainRoute message as 'looping' key not set in table")
			return
		end
		
		if trains[message.trainID].lastPastSignal ~= nil then
			if signals[trains[message.trainID].lastPastSignal].signalsFromThis[message.path[1]] == nil then
				print("Won't process railwayDispatchSetTrainRoute message as first point of path cannot be reached from current position")
				return
			end
		end
		
		for pathSignalIndex = 2, #message.path do
			if signals[message.path[pathSignalIndex - 1]].signalsFromThis[message.path[pathSignalIndex]] == nil then
				print("Won't process railwayDispatchSetTrainRoute message as " .. message.path[pathSignalIndex] .. " cannot be reached from " .. message.path[pathSignalIndex - 1])
				return
			end
		end
		
		trains[message.trainID].path = message.path
		trains[message.trainID].looping = message.looping
		
		tableFile.save(trains, "#railwayServerTrains.lua")
		
		-- Update routing
		for blockID, block in pairs(blocks) do
			if block.trainID == message.trainID then
				for signalInIndex, signalInID in pairs(block.signalsToThis) do
					calculateAndSetSignalState(signalInID, true)
					for signalFromInSignalID, _ in pairs(signals[signalInID].signalsFromThis) do
						calculateAndSetSignalState(signalFromInSignalID, true)
					end
				end
			end
		end
		tableFile.save(signals, "#railwayServerSignals.lua")
		
		--print("Updated route for train " .. message.trainID)
		--print("Train path: " ..textutils.serialize(trains[message.trainID].path))
		
		return
	end
	
	print("Unknown message type '" .. message.type .. "'")
end

calculateAndSetAllSignals()

local function validateSignals()
	for signalID, signal in pairs(signals) do
		if signal.x == nil or type(signal.x) ~= "number" then
			error("Signal " .. signalID .. " is missing x or is wrong type")
		end
		
		if signal.z == nil or type(signal.z) ~= "number" then
			error("Signal " .. signalID .. " is missing z or is wrong type")
		end
		
		if signal.signalsFromThis == nil or type(signal.signalsFromThis) ~= "table" then
			error("Signal " .. signalID .. " is missing signalsFromThis or is wrong type")
		end
		
		for signalFromThisID, signalFromThis in pairs(signal.signalsFromThis) do
			if signals[signalFromThisID] == nil then
				error("Can't find signal " .. signalFromThisID .. " that signal " .. signalID .. " sais it is pointing to")
			end
			
			if signalFromThis.trackSpine == nil or type(signalFromThis.trackSpine) ~= "table" then
				error("Signal from " .. signalID .. " to " .. signalFromThisID .. " is missing trackSpine or is wrong type")
			end
			
			if signalFromThis.redstoneRouting == nil or type(signalFromThis.redstoneRouting) ~= "table" then
				error("Signal from " .. signalID .. " to " .. signalFromThisID .. " is missing redstoneRouting or is wrong type")
			end
		end
		
		if signal.destinationOnly == nil or type(signal.destinationOnly) ~= "boolean" then
			error("Signal " .. signalID .. " is missing destinationOnly or is wrong type")
		end
		
		if signal.computerID == nil or type(signal.computerID) ~= "number" then
			error("Signal " .. signalID .. " is missing computerID or is wrong type")
		end
	end
end

validateSignals();

-- Validate Blocks
-- TODO

-- Validate trains
-- TODO

local function networkLoop()
	while true do
		local id, message, protocal = rednet.receive()
		if protocal == "railwaySignal" or protocal == "railwayConfigure" or protocal == "railwayDispatch" or protocal == "railwayDisplay" then
			table.insert(networkQueue, {
				["computerID"] = id,
				["message"] = message
			})
		end
	end
end

local function networkLoopSignal()
	while true do
		local id, message = rednet.receive("railwaySignal")
		table.insert(networkQueue, {
			["computerID"] = id,
			["message"] = message
		})
	end
end

local function networkLoopConfigure()
	while true do
		local id, message = rednet.receive("railwayConfigure")
		table.insert(networkQueue, {
			["computerID"] = id,
			["message"] = message
		})
	end
end

local function networkLoopDispatch()
	while true do
		local id, message = rednet.receive("railwayDispatch")
		table.insert(networkQueue, {
			["computerID"] = id,
			["message"] = message
		})
	end
end

local function networkLoopDisplay()
	while true do
		local id, message = rednet.receive("railwayDisplay")
		table.insert(networkQueue, {
			["computerID"] = id,
			["message"] = message
		})
	end
end

local function processNetworkMessagesLoop()
	while true do
		if util.tableSize(networkQueue) > 0 then
			local networkIn = table.remove(networkQueue, 1)
			processNetworkMessage(networkIn.computerID, networkIn.message)
		end
		sleep(0)
	end
end

local function redstoneLockSignalsLoop()
	local lastInput = false
	
	while true do
		local input = redstone.getInput("right")
		
		if lastInput ~= input then
			if input then
				lockAllSignals()
			else
				unlockAllSignals()
			end
		end
		
		lastInput = input
		
		sleep(0.5)
	end
end

print("---")

writeAllBlocksToMonitor()
writeAllSignalsToMonitor()

parallel.waitForAny(
	networkLoop,
	processNetworkMessagesLoop,
	redstoneLockSignalsLoop
)