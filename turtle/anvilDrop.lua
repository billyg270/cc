local wRequire = require("wRequire")
local bbTurtle = wRequire("bbTurtle")

rednet.open("right")

local taskList = {}

local anvilName = "minecraft:anvil"

local function countAnvils()
	local count = 0
	
	for i = 1, 16 do
		local item = turtle.getItemDetail(i)
		if item ~= nil and item.name == anvilName then
			count = count + item.count
		end
	end
	
	return count
end

local function sendStatus()
	rednet.broadcast({
		["anvilCount"] = countAnvils(),
		["position"] = bbTurtle.locate()
	}, "bbAnvilDrop-r2c")
end

local function networkLoop()
	while true do
		local id, message = rednet.receive("bbAnvilDrop-c2r", 10)
		if message ~= nil then
			if message.instruction == "clearQueue" then
				taskList = {}
			elseif message.instruction == "abort" then
				os.reboot()
			else
				table.insert(taskList, message)
			end
		end
	end
end

local function sendStatusLoop()
	while true do
		sendStatus()
		sleep(10)
	end
end

local function drop(redstonePulse)
	bbTurtle.select(anvilName)
	turtle.placeDown()
	if redstonePulse then
		rednet.setOutput("bottom", true)
		sleep(0.1)
		rednet.setOutput("bottom", false)
	end
end

local function actionLoop()
	while true do
		if #taskList > 0 then
			local instruction = table.remove(taskList, 1)
			if instruction.instruction == "goto" then
				shell.execute("refuel", "9999")
				bbTurtle.goto(instruction.x, instruction.y, instruction.z)
			elseif instruction.instruction == "gotoAndDrop" then
				shell.execute("refuel", "9999")
				bbTurtle.goto(instruction.x, instruction.y, instruction.z)
				drop(instruction.redstonePulse)
			elseif instruction.instruction == "gotoRel" then
				shell.execute("refuel", "9999")
				bbTurtle.gotoRel(instruction.x, instruction.y, instruction.z)
			elseif instruction.instruction == "gotoRelAndDrop" then
				shell.execute("refuel", "9999")
				bbTurtle.gotoRel(instruction.x, instruction.y, instruction.z)
				drop(instruction.redstonePulse)
			elseif instruction.instruction == "drop" then
				drop(instruction.redstonePulse)
			end
		else
			sleep(1)
		end
	end
end

parallel.waitForAny(networkLoop, actionLoop, sendStatusLoop)