local wRequire = require("wRequire")
local util = wRequire("util")

local lib = {}

lib.sidesToDirection = {
	["top"] = "u",
	["bottom"] = "d",
	["front"] = "f",
	["back"] = "b",
	["left"] = "l",
	["right"] = "r"
}

local dirs = {
    [1] = "north",
    [2] = "east",
    [3] = "south",
    [4] = "west"
}

local facing = "unknown"

local function dig(dir)
	if dir == "u" then
		return turtle.digUp()
	end
	if dir == "f" then
		return turtle.dig()
	end
	if dir == "d" then
		return turtle.digDown()
	end
	error("Unknown dig direction " .. dir)
end

lib["dig"] = dig

local function detect(dir)
	if dir == "u" then
		return turtle.detectUp()
	end
	if dir == "f" then
		return turtle.detect()
	end
	if dir == "d" then
		return turtle.detectDown()
	end
	error("Unknown detect direction " .. dir)
end

lib["detect"] = detect

local function inspect(dir)
	if dir == "u" then
		return turtle.inspectUp()
	end
	if dir == "f" then
		return turtle.inspect()
	end
	if dir == "d" then
		return turtle.inspectDown()
	end
	error("Unknown inspect direction " .. dir)
end

lib["inspect"] = inspect

lib.drop = function(dir, amt)
	if dir == "u" then
		return turtle.dropUp(amt)
	end
	if dir == "f" then
		return turtle.drop(amt)
	end
	if dir == "d" then
		return turtle.dropDown(amt)
	end
	if dir == "l" then
		turtle.turnLeft()
		local toRet = turtle.drop(amt)
		turtle.turnRight()
		return toRet
	end
	if dir == "r" then
		turtle.turnRight()
		local toRet = turtle.drop(amt)
		turtle.turnLeft()
		return toRet
	end
	if dir == "b" then
		turtle.turnLeft()
		turtle.turnLeft()
		local toRet = turtle.drop(amt)
		turtle.turnRight()
		turtle.turnRight()
		return toRet
	end
	error("Unknown drop direction " .. dir)
end

lib.suck = function(dir, amt)
	if dir == "u" then
		return turtle.suckUp(amt)
	end
	if dir == "f" then
		return turtle.suck(amt)
	end
	if dir == "d" then
		return turtle.suckDown(amt)
	end
	if dir == "l" then
		turtle.turnLeft()
		local toRet = turtle.drop(amt)
		turtle.turnRight()
		return toRet
	end
	if dir == "r" then
		turtle.turnRight()
		local toRet = turtle.suck(amt)
		turtle.turnLeft()
		return toRet
	end
	if dir == "b" then
		turtle.turnLeft()
		turtle.turnLeft()
		local toRet = turtle.suck(amt)
		turtle.turnRight()
		turtle.turnRight()
		return toRet
	end
	error("Unknown suck direction " .. dir)
end

local function rForward(drop)
	while turtle.forward() == false do
		turtle.dig()
		if drop then
			turtle.drop(64)
		end
	end
end

lib["rForward"] = rForward

local function rUp(drop)
	while turtle.up() == false do
		turtle.digUp()
		if drop then
			turtle.drop(64)
		end
	end
end

lib["rUp"] = rUp

local function rDown(drop)
	while turtle.down() == false do
		turtle.digDown()
		if drop then
			turtle.drop(64)
		end
	end
end

lib["rDown"] = rDown

local function rDig(dir)
	while detect(dir) do
		dig(dir)
	end
end

lib["rDig"] = rDig

local function select(itemName)
	for i = 1, 16 do
		local item = turtle.getItemDetail(i)
		if item ~= nil and item.name == itemName then
			turtle.select(i)
			return i
		end
	end
end

lib["select"] = select

lib.locate = function()--will assume is holding modem
    local x, y, z = gps.locate()
    local c = 0
    while x == nil do
        c = c + 1
        print("waiting for GPS " .. tostring(c))
        sleep(3)
        x, y, z = gps.locate()
    end
    
    return x, y, z
end

lib.getFacing = function ()
	if facing ~= "unknown" then
		return facing
	end
	
	local sx, sy, sz = lib.locate()
	lib.rForward()
	local nx, ny, nz = lib.locate()
	turtle.turnLeft()
	turtle.turnLeft()
	lib.rForward()
	turtle.turnLeft()
	turtle.turnLeft()
	
	if nz > sz then
		facing = "south"
		return facing
	end
	if nz < sz then
		facing = "north"
		return facing
	end
	if nx > sx then
		facing = "east"
		return facing
	end
	if nx < sx then
		facing = "west"
		return facing
	end
end

lib.left = function()
	local d = lib.getFacing()
    turtle.turnLeft()
    
    local cd = util.getIndex(d, dirs)
    
    local nd = cd - 1
    if nd == 0 then
        nd = 4
    end
	
    facing = dirs[nd]
end

lib.right = function()
	local d = lib.getFacing()
    turtle.turnRight()
    
    local cd = util.getIndex(d, dirs)
    
    local nd = cd + 1
    if nd == 5 then
        nd = 1
    end
	
	facing = dirs[nd]
end

lib.face = function(nd)
	local d = lib.getFacing()
    local cd = util.getIndex(d, dirs)
    local td = util.getIndex(nd, dirs)
    
    if cd == td then
        return nd
    end
    
    if cd == 1 and td == 4 then
        turtle.turnLeft()
        return nd
    end
    
    if cd == 4 and td == 1 then
        turtle.turnRight()
        return nd
    end
    
    if cd < td then
        for _ = cd, td - 1 do
            turtle.turnRight()
        end
    end
    
    if cd > td then
        for _ = cd, td + 1, -1 do
            turtle.turnLeft()
        end
    end
	
	facing = nd
end

lib.gotoRel = function(rx, ry, rz)
    if ry >= 0 then
        for i = 1, ry do
            lib.rUp()
        end
    else
        for i = -1, ry, -1 do
            lib.rDown()
        end
    end
    
    if rx > 0 then
        lib.face("east")
        for i = 1, rx do
            lib.rForward()
        end
    elseif rx < 0 then
        lib.face("west")
        for i = -1, rx, -1 do
            lib.rForward()
        end
    end
    
    if rz > 0 then
        lib.face("south")
        for i = 1, rz do
            lib.rForward()
        end
    elseif rz < 0 then
        lib.face("north")
        for i = -1, rz, -1 do
            lib.rForward()
        end
    end
end

lib.goto = function(x, y, z)
	local cx, cy, cz = lib.locate()
	lib.gotoRel(x - cx, y - cy, z - cz)
end

return lib