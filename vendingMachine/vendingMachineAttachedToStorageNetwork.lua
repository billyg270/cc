local basalt = require("basalt")--pastebin run ESs1mg7P packed
local wRequire = require("./wRequire")
local tableFile = wRequire("tableFile")
local invClient = wRequire("invClient")
local itemF = wRequire("storageServerItem")

local config = tableFile.load("#vendingMachineAttachedToStorageNetwork.lua", {})

if config.monitor == nil then
	print("Monitor:")
	config.monitor = read()
end
if config.storageIOlocally == nil then
	print("storage IO chest locally (perhaps an ender chest):")
	config.storageIOlocally = read()
end
if config.storageIOremotely == nil then
	print("storage IO chest remotely (perhaps an ender chest):")
	config.storageIOremotely = read()
end
if config.storageServerID == nil then
	print("Storage server ID:")
	config.storageServerID = tonumber(read())
end
if config.userChest == nil then
	print("User Chest (that the user can see):")
	config.userChest = read()
end
if config.holdingChest == nil then
	print("Holding Chest (regular chest in the back):")
	config.holdingChest = read()
end
if config.modemName == nil then
	print("Modem name:")
	config.modemName = read()
end
if config.speakerName == nil then
	print("Speaker name:")
	config.speakerName = read()
end

tableFile.save(config, "#vendingMachineAttachedToStorageNetwork.lua")

local userChest = peripheral.wrap(config.userChest)
local storageIOlocally = peripheral.wrap(config.storageIOlocally)
local holdingChest = peripheral.wrap(config.holdingChest)
local speaker = peripheral.wrap(config.speakerName)

invClient.storageComputerID = config.storageServerID

rednet.open(config.modemName)

local trades = {}

local function checkPayment(cost)
	local gotCount = 0
	
	for slot, item in pairs(userChest.list()) do
		if item ~= nil then
			local itemDetail = userChest.getItemDetail(slot)
			if
				itemDetail.name == cost.id and
				((itemDetail.damage == cost.damage) or (itemDetail.damage == nil and cost.damage == 0) or (cost.damage == nil and itemDetail.damage == 0)) and
				((itemDetail.nbt == cost.nbtHash) or (itemDetail.nbt == nil and cost.nbtHash == "") or (itemDetail.nbt == "" and cost.nbtHash == nil))
			then
				local toGet = math.min(cost.count - gotCount, itemDetail.count)
				userChest.pushItems(peripheral.getName(holdingChest), slot, toGet)
				gotCount = gotCount + toGet
			end
		end
	end
	
	return cost.count == gotCount
end

local function moveAllItems(from, to)
	for slot, item in pairs(from.list()) do
		if item ~= nil then
			from.pushItems(peripheral.getName(to), slot)
		end
	end
end

local function countInChest(chest)
	local total = 0
	
	for slot, item in pairs(chest.list()) do
		if item ~= nil then
			total = total + item.count
		end
	end
	
	return total
end

local function tryTrade(trade)
	moveAllItems(holdingChest, storageIOlocally)
	invClient.storeAllFromIoChest(config.storageIOremotely, storageIOlocally)
	
	if checkPayment(trade.cost) == false then
		moveAllItems(holdingChest, userChest)
		return "cost"
	end
	
	if
		invClient.getToAnySlot(
			itemF.getItemNameAndNbt({
				["name"] = trade.product.id,
				["damage"] = trade.product.damage,
				["nbt"] = trade.product.nbtHash
			}),
			trade.product.count,
			config.storageIOremotely,
			storageIOlocally
		) == false
		or
		countInChest(storageIOlocally) < trade.product.count
	then
		moveAllItems(holdingChest, userChest)
		invClient.storeAllFromIoChest(config.storageIOremotely, storageIOlocally)
		return "product"
	end
	
	moveAllItems(storageIOlocally, userChest)
	
	moveAllItems(holdingChest, storageIOlocally)
	invClient.storeAllFromIoChest(config.storageIOremotely, storageIOlocally)
	
	return "success"
end

local monitor = peripheral.wrap(config.monitor)
monitor.setTextScale(0.5)

local width, height = monitor.getSize()


local mainFrame = basalt.createFrame("mainFrame")
	:setBackground(colors.gray)
	:setMonitor(config.monitor)
	:setSize(monitor.getSize())
	:show()

mainFrame:addLabel("title")
	:setText("Bill's Merch")
	:setSize(width, 1)
	:setForeground(colors.white)
	:show()

mainFrame:addLabel("subtitle")
	:setText("Insert payment and click a trade")
	:setSize(width, 1)
	:setPosition(1, 2)
	:setForeground(colors.lightGray)
	:show()

local messageLabel = mainFrame:addLabel("message")
	:setText("[Message]")
	:setSize(width, 1)
	:setForeground(colors.red)
	:setPosition(width + 1, 3)
	:setZIndex(10)
	:setTextAlign("center")
	:show()

local messageLabelAnimation = mainFrame:addAnimation("messageLabelAnimation")
	--:add(function() messageLabel:setPosition(width, 3) end)
	--:wait(1)
	--:add(function() messageLabel:setPosition(0, 3) end)
	--:wait(1)
	--:add(function() messageLabel:setPosition(0, 3) end)
	--:wait(1)
	--:add(function() messageLabel:setPosition(-width - 2, 3) end)

for i = width, -width, -1 do
	messageLabelAnimation:add(function()
		messageLabel:setPosition(i, 3)
	end)
	
	messageLabelAnimation:wait(0.065)
end

local tradesFrameCount = 0

local function renderTrades()
	tradesFrameCount = tradesFrameCount + 1
	local tradesFrame = mainFrame:addFrame("tradesFrame-" .. tostring(tradesFrameCount))
		:setPosition(1, 4)
		:setSize(width, height - 4)
		:show()

	local activeCatagoryButton = nil
	local activeCatagoryFrame = nil
	local tabX = 0

	local tradesScrollbar = tradesFrame:addScrollbar("tradesScrollbar")
		:setPosition(width, 2)
		:setSize(1, height - 5)
		:onChange(function(self)
			activeCatagoryFrame:setOffset(0, self:getValue() - 1)
		end)
		:show()

	for tradeCatagory, catagoryTrades in pairs(trades) do
		local catagoryTradesFrame = tradesFrame:addFrame("catagoryTradesFrame-" .. tradeCatagory)
			:setPosition(1, 2)
			:setBackground(colors.black)
			:setSize(width - 1, height - 5)
		
		local catagoryTradesButton = tradesFrame:addButton("catagoryTradesButton-" .. tostring(tradeCatagory))
			:setPosition(tabX, 1)
			:setSize(10, 1)
			:setText(tradeCatagory)
			:setBackground(colors.lightGray)
			:setForeground(colors.black)
			:onClick(function(self)
				activeCatagoryButton:setBackground(colors.lightGray)
				self:setBackground(colors.white)
				activeCatagoryButton = self
				
				tradesScrollbar:setMaxValue(#catagoryTrades - 1)
				activeCatagoryFrame:setOffset(0, tradesScrollbar:getValue() - 1)
				
				activeCatagoryFrame:hide()
				catagoryTradesFrame:show()
				activeCatagoryFrame = catagoryTradesFrame
			end)
			:show()
		tabX = tabX + 12
		
		if activeCatagoryButton == nil then
			catagoryTradesButton:setBackground(colors.white)
			activeCatagoryButton = catagoryTradesButton
		end
		if activeCatagoryFrame == nil then
			catagoryTradesFrame:show()
			activeCatagoryFrame = catagoryTradesFrame
		end
		
		for i = 1, #catagoryTrades do
			
			local tradeArrow = "  -->  "
			local widthPerTradeLabel = math.floor(((width - 2) - string.len(tradeArrow)) / 2)
			
			local costStr = tostring(catagoryTrades[i].cost.count) .. "x " .. string.rep(" ", 4 - #tostring(catagoryTrades[i].cost.count)) .. catagoryTrades[i].cost.displayName
			local productStr = tostring(catagoryTrades[i].product.count) .. "x " .. string.rep(" ", 4 - #tostring(catagoryTrades[i].product.count)) .. catagoryTrades[i].product.displayName
			
			if catagoryTrades[i].enabled == true then
				
				catagoryTradesFrame:addButton("tradeButton-" .. tostring(i))
					:setPosition(1, i)
					:setSize(width - 1, 1)
					:setText(
						costStr ..
						string.rep(" ", widthPerTradeLabel - #costStr) ..
						tradeArrow ..
						productStr ..
						string.rep(" ", widthPerTradeLabel - #productStr)
					)
					:setBackground(colors.white)
					:setForeground(colors.black)
					:onClick(function()
						--messageLabel:setForeground(colors.blue)
						--messageLabel:setText("Processing...")
						--messageLabel:setPosition(1, 3)
						--basalt.update()
						monitor.clear()
						monitor.setCursorPos(1, 3)
						monitor.write("Processing...")
						
						local tradeRes = tryTrade(catagoryTrades[i])
						print(tradeRes)
						
						if tradeRes == "cost" then
							print("Insufficient payment")
							messageLabel:setForeground(colors.red)
							messageLabel:setText("Insufficient payment")
							messageLabelAnimation:play()
							speaker.playNote("didgeridoo")
						elseif tradeRes == "product" then
							print("Out of stock")
							messageLabel:setForeground(colors.red)
							messageLabel:setText("Out of stock")
							messageLabelAnimation:play()
							speaker.playNote("snare", 1, 3)
						elseif tradeRes == "success" then
							messageLabel:setForeground(colors.green)
							messageLabel:setText("Purchased")
							messageLabelAnimation:play()
							speaker.playNote("pling", 1, 16)
						else
							messageLabel:setForeground(colors.orange)
							messageLabel:setText(tradeRes)
							messageLabelAnimation:play()
							speaker.playNote("pling", 1, 16)
						end
						
						rednet.broadcast({
							["message"] = "trade",
							["trade"] = catagoryTrades[i],
							["res"] = tradeRes
						}, "bbMerch-c2s")
					end)
					:show()
			else
				catagoryTradesFrame:addButton("tradeButton-" .. tostring(i))
				:setPosition(1, i)
				:setSize(width - 1, 1)
				:setText(
					costStr ..
					string.rep(" ", widthPerTradeLabel - #costStr) ..
					tradeArrow ..
					productStr ..
					string.rep(" ", widthPerTradeLabel - #productStr)
				)
				:setBackground(colors.lightGray)
				:setForeground(colors.black)
				:onClick(function()
					messageLabel:setForeground(colors.blue)
					messageLabel:setText("Processing...")
					messageLabel:setPosition(0, 3)
					
					print("Out of stock (2)")
					messageLabel:setForeground(colors.red)
					messageLabel:setText("Out of stock")
					messageLabelAnimation:play()
					speaker.playNote("snare", 1, 3)
					
					rednet.broadcast({
						["message"] = "trade",
						["trade"] = catagoryTrades[i],
						["res"] = "outOfStock"
					}, "bbMerch-c2s")
				end)
				:show()
			end
		end
	end
end

local function mainLoop()
	rednet.broadcast({
		["message"] = "started"
	}, "bbMerch-c2s")
	
	local tradesFrame = mainFrame:addFrame("tradesFrame-" .. tostring(tradesFrameCount))
		:setPosition(1, 4)
		:setSize(width, height - 4)
		:show()
	
	tradesFrame:addLabel("loadingTitle"):setText("Loading...")
		:setSize(width, 5)
		:setForeground(colors.white)
		:show()
	
	while true do
		print("Waiting for initial message from controller")
		local id, message = rednet.receive("bbMerch-s2c", 10)
		if message ~= nil and type(message) == "table" then
			mainFrame:removeObject("tradesFrame-" .. tostring(tradesFrameCount))
			trades = message["trades"]
			renderTrades()
			print("Initial message recived")
			break
		end
		
		rednet.broadcast({
			["message"] = "started"
		}, "bbMerch-c2s")
	end
		
	while true do
		print("Waiting for message from controller")
        local id, message = rednet.receive("bbMerch-s2c")
        if message ~= nil and type(message) == "table" then
			--mainFrame:removeObject("tradesFrame-" .. tostring(tradesFrameCount))
			--trades = message["trades"]
			--print(textutils.serialise(trades["Exchange"]))
			--renderTrades()
			os.reboot()
        end
		print("Message recived")
	end
end

parallel.waitForAny(basalt.autoUpdate, mainLoop)

--renderTrades()
--basalt.autoUpdate()