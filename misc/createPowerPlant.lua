local wRequire = require("wRequire")

local tableFile = wRequire("tableFile")
local util = wRequire("util")

local config = tableFile.load("#createPowerPlant.lua", nil)

if config == nil then
    config = {
        ["generators"] = {}
    }

    print("Accumulator threashold switch peripherial:")
	config.atsp = read()

    print("Accumulator threashold switch side:")
	config.atss = read()

    print("Will no go through each type of generator in the power plant")
    while true do
        print("------")
        print("Fuel name (or END):")
        local fuel = string.upper(read())
        if fuel == "END" then
            print("Ended generator inputs")
            print("------")
            break
        end

        print("Fuel " .. fuel .. " tank threashold switch peripherial:")
        local tp = read()

        print("Fuel " .. fuel .. " tank threashold switch side:")
        local ts = read()

        print("Motor to power generator for " .. fuel .. " relay peripherial:")
        local rp = read()
        print("Motor to power generator for " .. fuel .. " relay side:")
        local rs = read()

        table.insert(config.generators, {
            ["fuel"] = fuel,
            ["tp"] = tp,
            ["ts"] = ts,
            ["rp"] = rp,
            ["rs"] = rs,
        })
    end
end

tableFile.save(config, "#createPowerPlant.lua")

local accumulatorThreasholdSwitchPeripherial = peripheral.wrap(config.atsp)

function getAccumulatorFull()
    return accumulatorThreasholdSwitchPeripherial.getInput(config.atss)
end

local fuelTankNotEmptyFuncs = {}
local genRelaySetFuncs = {}

for genIndex, gen in pairs(config.generators) do
    local tankPeripherial = peripheral.wrap(gen.tp)
    fuelTankNotEmptyFuncs[genIndex] = function()
        return tankPeripherial.getInput(gen.ts)
    end

    local relayPeripherial = peripheral.wrap(gen.rp)
    genRelaySetFuncs[genIndex] = function(powered)
        return relayPeripherial.setOutput(gen.rs, powered)
    end
end

while true do
    sleep(1)

    local accumulatorFull = getAccumulatorFull()

    for genIndex, gen in pairs(config.generators) do
        genRelaySetFuncs[genIndex](accumulatorFull == false and fuelTankNotEmptyFuncs[genIndex]())
    end
end