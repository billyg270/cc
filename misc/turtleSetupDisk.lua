local args = {...}

local function copyFilesToInstall()
	local filesToCopy = fs.list("/disk/toInstall")
	for _, toCopy in pairs(filesToCopy) do
		fs.copy("/disk/toInstall/" .. toCopy, "/" .. toCopy)
	end
end

local function makeStartupFile(scriptToRun)
	local file = fs.open("/startup.lua", "w")
	file.write("shell.execute(\"" .. scriptToRun .. "\")")
	file.close()
end

copyFilesToInstall()
--makeStartupFile(args[1])--not needed as will be defined in toInstall folder
--os.reboot()

shell.run("/disk/toRunFirst.lua")